#!/bin/bash

# Install Strimzi Kafka Operator
helm install strimzi-kafka-operator strimzi/strimzi-kafka-operator

# Function to check if pods are running
wait_for_pods() {
    local label=$1
    local running=0
    local total_pods
    local running_pods

    echo "Waiting for all pods with label $label to be in RUNNING state..."

    while [ $running -eq 0 ]; do
        # Get the total number of pods with the specified label
        total_pods=$(kubectl get pods -l $label --no-headers | wc -l)
        # Get the number of those pods that are in RUNNING state
        running_pods=$(kubectl get pods -l $label --no-headers | grep "Running" | wc -l)

        if [ $total_pods -eq $running_pods ] && [ $total_pods -ne 0 ]; then
            running=1
            echo "All pods with label $label are now running."
        else
            echo "Waiting for pods to be ready... Running pods: $running_pods/$total_pods"
            sleep 5
        fi
    done
}


# Function to check if the Persistent Volume is available
wait_for_pv() {
    local pv_name=$1
    local available=0
    echo "Waiting for Persistent Volume $pv_name to be in AVAILABLE state..."
    while [ $available -eq 0 ]; do
        if kubectl get pv $pv_name | grep -q "Bound"; then
            available=1
            echo "Persistent Volume $pv_name is now available."
        else
            echo "Waiting for PV to become available..."
            sleep 5
        fi
    done
}


kubectl apply -f PV/mongo-pv.yaml
wait_for_pv "mongo-pv"

kubectl apply -f mongo/mongo-deploy.yaml
kubectl apply -f mongo/mongo-svc.yaml
wait_for_pods "app=mongodb"
kubectl apply -f cluster/cluster-api.yaml
kubectl apply -f expressjs/expressjs-endpoint.yaml
kubectl apply -f expressjs/hpa.yaml
kubectl apply -f STT/stt-api.yaml
kubectl apply -f kafka/kafka.yaml
wait_for_pods "strimzi.io/kind=Kafka"
sleep 5
kubectl apply -f kafka/kafka-bridge.yaml

wait_for_pods "app=node-server"
wait_for_pods "app=flask-app"
wait_for_pods "strimzi.io/kind=KafkaBridge"

kubectl apply -f flask/flask-deploy.yaml
kubectl expose deployment flask-app --type=ClusterIP --name=flask-app --port=80

tmp=`pwd`
cd ingress/envoy
chmod +x ingress-setup.sh
./ingress-setup.sh
cd $tmp
