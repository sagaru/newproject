#!/bin/bash

# Check if an image name and tag are passed
if [ -z "$1" ]; then
    echo "Please provide a Docker image name with an optional tag."
    exit 1
fi

IMAGE_NAME=$1
TAR_FILE="${IMAGE_NAME//[:\/]/_}.tar" # Replace ':' and '/' with '_' in filename

# Save the Docker image to a tar file
docker save -o "$TAR_FILE" "$IMAGE_NAME"
echo "Docker image saved to $TAR_FILE"

# Switch to Minikube's Docker environment
eval $(minikube -p minikube docker-env)

# Load the Docker image into Minikube's Docker daemon
docker load -i "$TAR_FILE"
echo "Docker image loaded into Minikube"

# Clean up the tar file in Minikube
rm -f "$TAR_FILE"
echo "Cleaned up tar file in Minikube"

# Switch back to the original Docker environment
eval $(minikube docker-env --unset)
echo "Switched back to the original Docker context"

# Clean up the tar file in local
rm -f "$TAR_FILE"
echo "Cleaned up tar file in local environment"

echo "Process completed!"
