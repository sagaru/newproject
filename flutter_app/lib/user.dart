import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter_app/main.dart';
import 'package:latlong2/latlong.dart';



class User {
  String phone = '' ;
  LatLng location = LatLng(0.0, 0.0) ;
  String dmTopic = '' ;
  String smsVerificationCode = '' ;
  String consumerName = '' ;
  String consumerGroup = 'general_mobile_group' ;
  Set<String> dmHistory = {} ;
  static const String alertTopic = 'general_mobile_topic' ;
  bool isLoggedIn = false ;
  List<LatLng> waypoints = [] ;



  Map<String, dynamic> toMap() {
    List<double> waypoint = [] ;
    if (waypoints.isNotEmpty) {
      waypoint = [
        waypoints[waypoints.length - 1].latitude,
        waypoints[waypoints.length - 1].longitude
      ];
    }
    return {
      'phone': phone,
      'location': [location.latitude, location.longitude],
      'dmTopic': dmTopic,
      'smsVerificationCode': smsVerificationCode,
      'consumerName': consumerName,
      'dmHistory': dmHistory.toList(), // Convert Set to List for serialization
      'isLoggedIn': isLoggedIn,
      'consumerGroup': consumerGroup,
      'waypoint': waypoint
    };
  }


  void printState() {
    Map<String, String> state = {
      'phone': phone,
      'location': location.toString(),
      'dmTopic': dmTopic,
      'smsVerificationCode': smsVerificationCode,
      'consumerName': consumerName,
      'dmHistory': dmHistory.toString(),
      'isLoggedIn': isLoggedIn.toString(),
      'consumerGroup': consumerGroup
    } ;

    print(state) ;
    return ;
  }

  set setConsumerGroup(String group) {
    consumerGroup = group ;
  }

  get getConsumerGroup {
    return consumerGroup ;
  }

  set setPhone(String phoneNumber) {
    phone = phoneNumber ;
    setDmTopic = "DM_$phone" ;
    consumerName = 'consumer_$phone';
  }

  get getPhone {
    return phone ;
  }

  set setLocation(LatLng location) {
    this.location = location ;
  }

  get getLocation {
    return location ;
  }

  set setDmTopic(String topic) {
    dmTopic = topic ;
  }

  get getDmTopic {
    return dmTopic ;
  }

  get getSmsVerificationCode {
    return smsVerificationCode ;
  }

  set setSmsVerificationCode(String code) {
    smsVerificationCode = code ;
  }

  get getConsumerName {
    return consumerName ;
  }


  get getDmHistory {
    return dmHistory ;
  }

  bool addToDmHistory(String message) {
    return dmHistory.add(message) ;
  }

  get getWaypoints {
    return waypoints ;
  }

  set setWaypoints(List<LatLng> points) {
    waypoints = points ;
  }

  void clearWaypoints() {
    waypoints.clear() ;
  }

  dispose() {
    isLoggedIn = false ;
    phone = '' ;
    location = LatLng(0.0, 0.0) ;
    dmTopic = '' ;
    smsVerificationCode = '' ;
    consumerName = '' ;
    dmHistory = {} ;
  }

  void reset() {
    isLoggedIn = false ;
    phone = '' ;
    location = LatLng(0.0, 0.0) ;
    dmTopic = '' ;
    smsVerificationCode = '' ;
    consumerName = '' ;
    dmHistory = {} ;
    consumerGroup = 'general_mobile_group' ;

  }

}


User user = User();