// import 'package:flutter_app/voiceMessagePage.dart';
// import 'package:flutter_app/network_service.dart';
// import 'package:flutter_test/flutter_test.dart';
// import 'package:mockito/mockito.dart';
// import 'package:http/http.dart' as http;
//
//
// class MockNetworkService extends Mock implements NetworkService {}
//
// void main() {
//   testWidgets('VoiceMessagePage sends a voice message', (WidgetTester tester) async {
//     // Mock the NetworkService
//     final mockNetworkService = MockNetworkService();
//     when(mockNetworkService.sendVoiceMessage(any, filePath: anyNamed('filePath'), fields: anyNamed('fields')))
//         .thenAnswer((_) async => http.Response('{"result": "success"}', 200));
//
//     // Assuming you've modified VoiceMessagePage to accept a NetworkService instance
//     await tester.pumpWidget(MaterialApp(home: VoiceMessagePage(networkService: mockNetworkService)));
//
//     // Trigger the recording and sending logic as needed
//     // For example, if you have a button to start recording, you'd tap it:
//     // await tester.tap(find.byIcon(Icons.mic));
//     // await tester.pumpAndSettle();
//
//     // Verify the network service was called
//     verify(mockNetworkService.sendVoiceMessage(any, filePath: anyNamed('filePath'), fields: anyNamed('fields'))).called(1);
//   });
// }
