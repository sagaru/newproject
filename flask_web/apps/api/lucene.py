import subprocess
from flask import redirect, send_file, request, render_template, Response
from apps.config import Config
from prometheus_client import Counter, Histogram, generate_latest, REGISTRY, Summary
import time



LUCENE_REQUEST_COUNT = Counter('http_lucene_requests_total', 'Total HTTP Requests')
REQUEST_LATENCY = Histogram('http_lucene_request_duration_seconds', 'HTTP request latency')
RESPONSE_TIME = Summary('http_lucene_response_time_seconds', 'HTTP response times')

def init_app(app):
    @app.route('/run-lucene-index')
    def run_lucene_index():

        LUCENE_REQUEST_COUNT.inc()

        start_time = time.time()
        command = "cd /opt/cs7is3-lucene-tutorial-examples/cs7is3-lucene-tutorial-examples/example2-addDocument; \
                    java -jar target/example2-1.2.jar \
                    ../corpus/*"

        process = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
        output, error = process.communicate()
        
        with open('/var/tmp/output_file.txt', 'w') as file:
            file.write(output)

        duration = time.time() - start_time
        RESPONSE_TIME.observe(duration)
        REQUEST_LATENCY.observe(duration)

        return redirect('/display_text_file')


    @app.route('/display_text_file')
    def display_text_file():
        file_path = '/var/tmp/output_file.txt'

        with open(file_path, 'r') as file:
            file_content = file.read()

        return render_template('lucene/lucene-index.html', file_content=file_content)


    @app.route('/search', methods=['POST'])
    def search():
        search_terms = request.form['search_terms']
        command = "cd /opt/cs7is3-lucene-tutorial-examples/cs7is3-lucene-tutorial-examples/example4-interactive; \
                    java -jar target/example4-1.2.jar"

        input_text = "raven\n"

        process = subprocess.Popen(command, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
        output, error = process.communicate(search_terms)

        lines = output.strip().split('\n')

        documents = []
        for line in lines[1:]:
            parts = line.split(' ', 2)
            if len(parts) >= 3:
                doc = {
                    "title": parts[1],
                    "score": float(parts[2])
                }
                documents.append(doc)

        return render_template('lucene/results.html', search_results=documents)

    @app.route('/metrics')
    def metrics():
        return Response(generate_latest(), mimetype='text/plain')
