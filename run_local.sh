#!/bin/bash

container_name="mongodb-container"

# Check for --no-kafka option
kafka=true
for arg in "$@"
do
    if [[ "$arg" == "-k" ]]; then
        kafka=false
        break
    fi
done

# restart stopped container or start it from the image
if [[ "$(docker ps -a -q -f name=${container_name})" ]]; then
  docker start ${container_name}
else
  docker run --name ${container_name} -p 27017:27017 -d mongo
fi

if [[ "$kafka" == true ]]; then
    cd kafka
    ./run_local.sh
    cd ..
fi

# Wait for specific Docker containers to be healthy
echo "Waiting for Docker containers to be healthy..."
while ! docker ps | grep "ksqldb-server" | grep -q "healthy"; do sleep 1; done
while ! docker ps | grep "connect" | grep -q "healthy"; do sleep 1; done
echo "All specified Docker containers are healthy."

cd map-api
gunicorn --timeout 60 -b 0.0.0.0:8080 api:app &
cd ../Database/Endpoint
node node-server.js &
cd ../../flask_web
gunicorn run:app -b 0.0.0.0:8000 &
cd ../
cd STT
bash run_local.sh &
cd ../clustering
bash run_local.sh &

