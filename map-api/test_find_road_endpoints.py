import unittest
import networkx as nx
from astar import find_road_endpoints

class TestFindRoadEndpoints(unittest.TestCase):

    def setUp(self):
        self.graph = nx.Graph()
        self.graph.add_edge((0, 0), (1, 1), weight=1, name="main_road")
        self.graph.add_edge((1, 1), (2, 2), weight=1, name="main_road")
        self.graph.add_edge((2, 2), (3, 3), weight=1, name="main_road")
        self.graph.add_edge((5, 5), (6, 6), weight=1, name="main_road_2")
        self.graph.add_edge((6, 6), (7, 7), weight=1, name="main_road_3")
        # disconnected segment 
        self.graph.add_edge((10, 10), (11, 11), weight=1, name="main_road")
        self.graph.add_edge((11, 11), (12, 12), weight=1, name="main_road")
        
        self.graph.add_edge((0, 0), (-1, -1), weight=1, name="side_road")
        #branching road
        self.graph.add_edge((30, 30), (31, 31), weight=2, name="branch_road")
        self.graph.add_edge((31, 31), (32, 32), weight=2, name="branch_road")
        self.graph.add_edge((31, 31), (33, 33), weight=3, name="branch_road")

    def test_find_road_endpoints_basic(self):
        # basic case
        longest_path, max_length = find_road_endpoints(self.graph, "main_road")
        self.assertEqual(longest_path, ((0, 0), (3, 3)), "Longest path endpoints do not match expected result.")
        self.assertEqual(max_length, 3, "Max length does not match expected result.")

    def test_find_road_endpoints_disconnected_segments(self):
        # main_road path should not include the disconnected segment
        longest_path, max_length = find_road_endpoints(self.graph, "main_road")
        self.assertNotIn((10, 10), longest_path, "Longest path includes disconnected segment.")
        self.assertNotIn((11, 11), longest_path, "Longest path includes disconnected segment.")
        self.assertEqual(max_length, 3, "Max length is 4")

    def test_find_road_endpoints_nonexistent_road(self):
        # non existent road check
        longest_path, max_length = find_road_endpoints(self.graph, "nonexistent_road")
        self.assertIsNone(longest_path, "Longest path should be None for a nonexistent road.")
        self.assertEqual(max_length, 0, "Max length should be 0 for a nonexistent road.")

    def test_find_road_endpoints_single_segment_road(self):
        # testing for road with length of 1
        longest_path, max_length = find_road_endpoints(self.graph, "side_road")
        self.assertEqual(longest_path, ((0, 0), (-1, -1)), "Longest path endpoints do not match for a single-segment road.")
        self.assertEqual(max_length,1, "Max length does not match expected result for a branch road.")

    def test_find_road_endpoints_branching_road(self):

        # checking length of branching road, along with weights
        longest_path, max_length = find_road_endpoints(self.graph, "branch_road")
        
        self.assertEqual(longest_path, ((30, 30), (33, 33)), "Longest path endpoints do not match for a branch road.")
        
        # max length should be sum of 2 and 3
        self.assertEqual(max_length, 5, "Max length does not match expected result for a branch road.")


if __name__ == '__main__':
    unittest.main()
