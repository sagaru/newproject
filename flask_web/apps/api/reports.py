from math import log
import re
from flask import jsonify, request, render_template
from pymongo import MongoClient
import requests
import json
from apps.config import Config
from bson.objectid import ObjectId
from flask_login import login_required


def init_app(app):
    @login_required
    @app.route("/get-reports-table", methods=["GET"])
    def get_reports_table():
        html = render_template("reports/report_table.html")
        return jsonify({"html": html})

    @app.route("/get-report", methods=["GET"])
    # @login_required
    def get_report_data():
        client = MongoClient(Config.MONGO_URI)
        db = client.Dashboard

        cursor = db.Reports.find({})
        reports = list(cursor)
        for report in reports:
            report["_id"] = str(report["_id"])

        return jsonify(reports)

    @app.route("/report/<report_id>")
    @login_required
    def report(report_id):
        client = MongoClient(Config.MONGO_URI)
        db = client.Dashboard
        app.logger.info(report_id)
        report = db.Reports.find_one({"_id": ObjectId(report_id)})

        client.close()

        if report is None:
            return render_template("other/page-404.html"), 404

        amenities = {
            "hospital": {"name": "", "coordinates": "", "distance": ""},
            "fire station": {"name": "", "coordinates": "", "distance": ""},
            "police station": {"name": "", "coordinates": "", "distance": ""},
            "coast guard": {"name": "", "coordinates": "", "distance": ""},
        }

        for amenity in amenities.keys():
            response = requests.post(
                Config.MAP_API + "/get_nearest_amenity",
                json={
                    "lat": str(report["location"]["lat"]),
                    "lng": str(report["location"]["lng"]),
                    "amenity_name": amenity,
                },
            ).json()
            if response.get("error", None) is None:
                amenities[amenity] = response

        report["_id"] = str(report["_id"])
        html = render_template(
            "reports/report_details.html", report=report, amenities=amenities
        )

        return jsonify({"html": html})

    @app.route("/delete-report", methods=["POST"])
    @login_required
    def delete_report():
        report_id = request.get_json()["report_id"]
        app.logger.warning(report_id)
        client = MongoClient(Config.MONGO_URI)
        db = client.Dashboard
        report = db.Reports.find_one({"_id": ObjectId(report_id)})
        if not report:
            return jsonify({"message": "Report not found"}), 404
        db.ReportsArchive.insert_one(
            {
                "location": report["location"],
                "description": report["description"],
                "timestamp": report["timestamp"],
            }
        )

        # delete this report from Reports
        db.Reports.delete_one({"_id": ObjectId(report_id)})
        return jsonify({"message": "Report deleted successfully"})

    @app.route("/get-blocked-routes", methods=["GET"])
    @login_required
    def get_blocked_routes():
        client = MongoClient(Config.MONGO_URI)
        db = client.Dashboard
        cursor = db.BlockedRoutes.find({})
        routes = list(cursor)
        r = []
        for route in routes:
            ret = {}
            ret["_id"] = str(route["_id"])
            ret["start"] = route["path"][0]
            ret["end"] = route["path"][-1]
            ret["path"] = route["path"]
            ret["road_name"] = route["road_name"]
            r.append(ret)

        rets = jsonify(r)
        return rets, 200


# example for thin slice
# def process_reports(reports, app=None):
#     processed_reports = []
#     route_fetched = False


#     for report in reports:
#         processed_report = report.copy()
#         # app.logger.info(report)
#         if 'M50' in report.get('description', '') and not route_fetched:
#             try:
#                 # app.logger.info('trying get_route_coordinates')
#                 coords = get_route_coordinates('M50', app=app)
#                 # app.logger.info('got route coordinates')
#                 if coords:
#                     processed_report['blocked_route'] = coords
#                 route_fetched = True
#             except:
#                 processed_report['blocked_route'] = []
#         else:
#             processed_report['blocked_route'] = []

#         processed_reports.append(processed_report)


#     return processed_reports


# def fetch_route_coordinates(route):  ### for demo purposes

#     if route == 'M50':
#         #ASSETS_ROOT
#         filename = 'export.geojson'
#         with open(filename, 'r') as f:
#             geojson = json.load(f)
#             return geojson


# def get_route_coordinates(route, app=None):
#     # app.logger.info('get_route_coordinates called')
#     client = MongoClient(Config.MONGO_URI)
#     db = client.Dashboard
#     cursor = db.CachedRoutes.find_one({route: {'$exists': True}})
#     if cursor:
#         route_geojson = cursor[route]
#     else:
#         route_geojson = fetch_route_coordinates(route)
#         if route_geojson:
#             db.CachedRoutes.insert_one({route: route_geojson})
#         else:
#             app.logger.info('route_geojson not found')
#             return None


#     route_coordinates = []

#     tmp = []
#     for feature in route_geojson['features']:
#         if feature['geometry']['type'] == 'LineString':
#             for coords in feature['geometry']['coordinates']:
#                 tmp.append([coords[1], coords[0]])
#                 # app.logger.info(tmp)
#             route_coordinates.append(tmp)
#             tmp = []

#     return route_coordinates
