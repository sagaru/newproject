import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:flutter_app/user.dart';
import 'main.dart' ;
import 'mapPage.dart';
import 'package:flutter_app/utils/user_preferences.dart';
import 'package:connectivity_plus/connectivity_plus.dart';


class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final TextEditingController _phoneController = TextEditingController();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  Future<void> _login() async {

    // pass context and phone number to login function
    if (await login(context, _phoneController.text)) {
      Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => BaseWidget(body: MapPage())));
    }
    else {
      ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(
            content: Text('Error logging in'),
          ));
    }
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(title: const Text('Login')),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            TextFormField(
              controller: _phoneController,
              decoration: const InputDecoration(
                labelText: 'Phone Number, e.g. 0871234567',
                border: OutlineInputBorder(),
              ),
              keyboardType: TextInputType.phone,
            ),
            const SizedBox(height: 20),
            ElevatedButton(
              onPressed: _login,
              child: const Text('Login'),
            ),
          ],
        ),
      ),
    );
  }
}



Future<bool> createKafkaConsumer() async {
  ConnectivityResult connectivityResult = await (Connectivity().checkConnectivity());
  if (connectivityResult != ConnectivityResult.mobile && connectivityResult != ConnectivityResult.wifi) {
    print('No internet connection');
    return false;
  }
  try {
    // Create Consumer Group
    var response = await http.post(
      // Uri.parse('$serverAddr:$kafkaPort/consumers/${user.getConsumerGroup}'),
      Uri.parse('$serverAddr/consumers/${user.getConsumerGroup}'),
      headers: {'Content-Type': 'application/vnd.kafka.json.v2+json'},
      body: jsonEncode({
        'name': user.getConsumerName,
        'format': 'json',
        'auto.offset.reset': 'earliest'
      }),
    );

    if (response.statusCode >= 200 && response.statusCode < 300) {
      print('Consumer group created successfully');
      return true ;
    }
    else if (response.statusCode == 409) {
      print('Consumer group already exists');
      return true ;
    }
    else {
      print('Error creating consumer group: ${response.statusCode} - ${response.body}');
      return false;
    }
  } catch (e) {
    print('Error creating consumer group: $e');
    return false;
  }
}

Future<bool> subscribeTopics(List<String> topics) async {
  ConnectivityResult connectivityResult = await (Connectivity().checkConnectivity());
  if (connectivityResult != ConnectivityResult.mobile && connectivityResult != ConnectivityResult.wifi) {
    print('No internet connection');
    return false;
  }
  try {
    print('$serverAddr/consumers/${user.getConsumerGroup}/instances/${user.getConsumerName}/subscription');
    //print topics
    for (String topic in topics) {
      print('Topic: $topic');
    }
    var response = await http.post(
      // Uri.parse('$serverAddr:$kafkaPort/consumers/${user.getConsumerGroup}/instances/${user.getConsumerName}/subscription'),
      Uri.parse('$serverAddr/consumers/${user.getConsumerGroup}/instances/${user.getConsumerName}/subscription'),
      body: jsonEncode({'topics': topics}),
      headers: {'Content-Type': 'application/vnd.kafka.json.v2+json'},
    );

    if (response.statusCode >= 200 && response.statusCode < 300) {
      print('Consumer subscribed successfully to topics');
      return true;
    } else {
      print('Error subscribing consumer to topics: ${response.statusCode} - ${response.body}');
      return false;
    }
  } catch (e) {
    print('Error subscribing ${user.getConsumerName} to topics: $e');
    return false;
  }
}


Future<bool> login(BuildContext context, String phoneNumber) async {
  try {
////  for development purposes only
    bool ret = false ;
    String phone = '353${phoneNumber.substring(1)}';
    user.setPhone = phone;
    ret = (await createKafkaConsumer());
    user.isLoggedIn = ret && (await subscribeTopics([user.getDmTopic, User.alertTopic]));
    if (user.isLoggedIn) {
      UserPreferences.setUser(user);
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
        content: Text('login successful'),
      ));
      return true ;
    }
    else {
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
        content: Text('Error logging in - Please check your internet connection'),
      ));
      // user.reset() ;
      return false ;
    }

  }
  catch (e) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text('Error logging in: $e'),
    ));
    return false ;
  }
}

/////////////////////////////////

    // if (phone.isEmpty) {
    //   ScaffoldMessenger.of(context).showSnackBar(
    //       const SnackBar(
    //         content: Text('Please enter a phone number'),
    //       ));
    // }
    // else if (phone.startsWith('+') || phone.startsWith('0')) {
    //   phone = '353${phone.substring(1)}';
    // }
    // else if (phone.length > 9) {
    //   phone = phone.substring(phone.length - 9);
    //   // phone = '353${phone.substring(-9)}';
    //   // 353 + {last 9 digits}
    // }
    // print('Processed phone number: $phone') ;
    //

    // var response = await http.post(
    //   Uri.parse('http://10.0.2.2:3000/flutter-login'),
    //   body: jsonEncode({'phone': phone}),
    //   headers: {'Content-Type': 'application/json'},
    // );
    //
    // if (response.statusCode == 200) {
    //   ScaffoldMessenger.of(context).showSnackBar(SnackBar(
    //     content: Text('login successful: ${response.body}'),
    //   ));
    //   user.isLoggedIn = true;
    //   user.setPhone = phone;
    //   createKafkaConsumer();
    //   //createTopic();
    //   subscribeTopic(user.getDmTopic);
    //   subscribeTopic(User.alertTopic);
    // }
    // else {
    //   ScaffoldMessenger.of(context).showSnackBar(SnackBar(
    //     content: Text('Error logging in: ${response.statusCode} - ${response.body}'),
    //   ));
    // }
// }
//   catch (e) {
//     ScaffoldMessenger.of(context).showSnackBar(SnackBar(
//       content: Text('Error logging in: $e'),
//     ));
//     return false ;
//   }
// }


