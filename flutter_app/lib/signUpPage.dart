import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_app/loginPage.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_app/user.dart';
import 'main.dart';
import 'package:flutter_app/smsVerification.dart';


class SignUpPage extends StatefulWidget {
  const SignUpPage({super.key});

  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  final TextEditingController _phoneController = TextEditingController();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();



  Future<void> _postSignUp() async {
    try {
      if (_phoneController.text.isEmpty) {
        ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(
              content: Text('Please enter a phone number'),
            ));
        return;
      }

      bool valid = false ;
      String processedPhoneNumber = "" ;
      // phone number must:
      // start with 0, 353, +353 OR be 9 digits long
      // all numbers will be stored as 353 plus the 9 digits

      if (_phoneController.text.startsWith('0')) {
        valid = true ;
        // remove all leading zeros
        processedPhoneNumber = _phoneController.text.replaceFirst(RegExp(r'^0+'), '353') ;
      }
      else if (_phoneController.text.startsWith('353')) {
        valid = true ;
        processedPhoneNumber = _phoneController.text ;
      }
      else if (_phoneController.text.startsWith('+353')) {
        valid = true ;
        processedPhoneNumber = _phoneController.text.replaceFirst('+', '') ;
      }
      if (processedPhoneNumber != "" && processedPhoneNumber.length == 12) {
        valid = valid && true ;
      }

      print('$processedPhoneNumber') ;
      print('length: ${processedPhoneNumber.length}') ;
      print('valid: $valid') ;


      // Basic validation for phone number length
      if (!valid) {
        ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(
              content: Text('Invalid phone number length'),
            ));
        return;
      }


      print('Processed phone number: $processedPhoneNumber');
      int verificationCode = await twilioSendSMS(processedPhoneNumber) ;
      if (verificationCode == 0) {
        ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(
              content: Text('Error sending SMS'),
            ));
        return;
      }
      else if (verificationCode == 1) {
        print('SMS sent successfully');
      }
      else if (verificationCode == 2){
        ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(
              content: Text('Phone number already exists'),
            ));


        Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => BaseWidget(body: const LoginPage())));
        return;
      }

      ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(
            content: Text('SMS sent'),
          ));

      user.setPhone = processedPhoneNumber;

      // Navigate to SMS code verification page
      Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => BaseWidget(body: const SMSCodeVerificationPage())));

    } catch (e) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text('Error signing up: $e'),
      ));
    }
  }





  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(title: const Text('Sign Up')),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            TextFormField(
              controller: _phoneController,
              decoration: const InputDecoration(
                labelText: 'Phone Number, e.g. 0871234567',
                border: OutlineInputBorder(),
              ),
              keyboardType: TextInputType.phone,
            ),
            const SizedBox(height: 20),
            ElevatedButton(
              onPressed: _postSignUp,
              child: const Text('Sign Up'),
            ),
          ],
        ),
      ),
    );
  }
}


Future<int> twilioSendSMS(String phoneNumber) async {
  try {


    // final response = await http.get(
    //   Uri.parse('$serverAddr:$expressJsPort/ping'),
    //   headers: {'Content-Type': 'application/json'},
    // );
    // print("pinged server") ;
    // return true ;

    print('twilioSending') ;
    final response = await http.post(
      // Uri.parse('$serverAddr:$expressJsPort/flutter-twilio-send-sms'),
      Uri.parse('$serverAddr/flutter-twilio-send-sms'),
      headers: {'Content-Type': 'application/json'},
      body: json.encode({'phone': phoneNumber}),
    );
    print('twilioSent') ;

    if (response.statusCode == 200) {
      print('SMS sent successfully\n\n\n\n\n\n');
      // nodejs; res.status(200).send({code: toString(code)});
      user.setSmsVerificationCode = json.decode(response.body)['code'];
      return 1 ;
    } else {
      if (response.statusCode == 409) {
        return 2; //  phone number already exists
      }
      print('Error sending SMS: ${response.statusCode} - ${response.body}\n\n\n\n\n\n');
      return 0 ;
    }
  } catch (e) {
    print('Error sending SMS: $e\n\n\n\n\n\n\n\n');
    return 0 ;
  }
}

