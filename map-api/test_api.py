import unittest
from unittest.mock import MagicMock
import networkx as nx
from unittest.mock import patch
import os
import sys
import random
project_root_path = os.path.abspath(os.path.join(os.getcwd(), '../'))
sys.path.append(project_root_path)
from random import randrange as rand
from flask import Flask, request, jsonify
from astar import nearest_node, reverse_lat_lon, a_star_search, create_graph, apply_constraints, restore_constraints, find_road_endpoints, manhattan_distance, haversine_heuristic
from api import app, G


class TestMapAPI(unittest.TestCase):
    def setUp(self):
        self.graph = G
        self.client = app.test_client()
        self.client.testing = True
        # self.client.post = MagicMock()
        # self.client.get = MagicMock()
        
        
    @patch('api.G', G)
    def test_find_path(self):
        for _ in range(10):
             with self.subTest():  # Each iteration a separate test
                random_nodes = [
                    list(self.graph.nodes)[random.randint(0, len(self.graph.nodes) - 1)],
                    list(self.graph.nodes)[random.randint(0, len(self.graph.nodes) - 1)]
                ]


                try:
                    response = self.client.post('/find_path', json={
                        'start_lat': random_nodes[0][0],
                        'start_lon': random_nodes[0][1],
                        'end_lat': random_nodes[1][0],
                        'end_lon': random_nodes[1][1]
                    })
                except Exception as e:
                    self.fail(f"An exception occurred: {e}")

                self.assertEqual(response.status_code, 200, "Response status code is not 200.")
                self.assertTrue(response.is_json, "Response is not JSON.")
                data = response.get_json()
                self.assertTrue('path' in data.keys(), "Response does not contain a path.")

        
        


if __name__ == "__main__":
    unittest.main()
