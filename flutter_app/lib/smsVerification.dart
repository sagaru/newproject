import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:flutter_app/user.dart';
import 'package:flutter_app/utils/user_preferences.dart';
import 'main.dart';
import 'package:flutter_app/mapPage.dart';
import 'package:flutter_app/loginPage.dart';
import 'package:flutter_app/background_services.dart';


class SMSCodeVerificationPage extends StatefulWidget {
  const SMSCodeVerificationPage({super.key});

  @override
  _SMSCodeVerificationPageState createState() => _SMSCodeVerificationPageState();
}

class _SMSCodeVerificationPageState extends State<SMSCodeVerificationPage> {
  final TextEditingController _codeController = TextEditingController();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  Future<void> _postVerificationCode() async {
    try {
      user.isLoggedIn = false;
      if (_codeController.text.isEmpty) {
        ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(
              content: Text('Please enter a verification code'),
            ));
        return;
      }
      else if (_codeController.text.length != 6) {
          ScaffoldMessenger.of(context).showSnackBar(
              const SnackBar(
                content: Text('Please enter a 6-digit verification code'),
              ));
          return;
      }
      else if (_codeController.text == user.getSmsVerificationCode) {
        ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(
              content: Text('Verification code accepted'),
            ));
        // setPasskey(_codeController.text);

        var response = await http.post(
          Uri.parse('$serverAddr/flutter-login'),
          body: jsonEncode({'phone': user.getPhone}),
          headers: {'Content-Type': 'application/json'},
        );

        if (response.statusCode == 200) {
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text('login successful: ${response.body}'),
          ));


          user.isLoggedIn = true;
          user.isLoggedIn = user.isLoggedIn & (await createKafkaConsumer());
          user.isLoggedIn = user.isLoggedIn & (await subscribeTopics([user.getDmTopic, User.alertTopic]));
          UserPreferences.setUser(user);
        }

        Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => BaseWidget(body: MapPage())));
      }
      else {
        user.setPhone = "";
        ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(
              content: Text('Verification code incorrect'),
            ));
        return;
      }
    }
    catch (e) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text('Error verifying code: $e'),
      ));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(title: const Text('Sign Up')),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Text('Enter verification code'),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: TextField(
                controller: _codeController,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Verification Code',
                ),
              ),
            ),
            ElevatedButton(
              onPressed: _postVerificationCode,
              child: const Text('Submit'),
            ),
          ],
        ),
      ),
    );
  }
}
