from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField
from wtforms.validators import Email, DataRequired, Optional, Regexp

def is_email(form, field):
    if '@' in field.data:
        Email()(form, field)

class LoginForm(FlaskForm):
    email_or_phone = StringField('Email or Phone',
                                 id='email_or_phone_login',
                                 validators=[DataRequired(), is_email])
    password = PasswordField('Password',
                             id='pwd_login',
                             validators=[DataRequired()])

class CreateAccountForm(FlaskForm):
    email = StringField('Email',
                        id='email_create',
                        validators=[DataRequired(), Email()])
    
    # +353 for Ireland
    # phone_regex = Regexp(regex=r'^\353\d{9}$', message='Phone number must be in the format 353XXXXXXXXX')
    phone = StringField('Phone',
                        id='phone_create',
                        validators=[DataRequired()])
    
    password = PasswordField('Password',
                             id='pwd_create',
                             validators=[DataRequired()])
