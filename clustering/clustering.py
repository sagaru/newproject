import haversine
from datetime import datetime
import math
from openai import OpenAI
import os
from pymongo import MongoClient
from bson import ObjectId
import re
import json

# Set the API key from an environment variable for better security practices
client = OpenAI(api_key=os.environ.get("OPENAI_API_KEY"))
MONGO_URI = os.environ.get('MONGO_URI', 'mongodb://localhost:27017')
threshold = 0.5

def llm_similarity(statement1, statement2):
    response = client.chat.completions.create(
        model="gpt-3.5-turbo",
        messages=[{
            "role": "user" , 
            "content":f"Analyze the semantic similarity between the following two emergency\
                    reports:\n\nReport 1: '{statement1}'\nReport 2: '{statement2}'\n\nProvide a similarity score between 0 and 1, where 0 means \
                        the reports are completely dissimilar and 1 means they are identical. The score should be calculated \
                        considering the type of emergency, location, and other relevant details. A score above 0.5 means the two reports \
                        likely correspond to the same event. Note that the two events may be unrelated. Respond with ONLY the score, using 2 decimal places.\
                        ",
        }],
        max_tokens=5,
        temperature=0,
    )
    print(str(json.dumps(json.loads(response.model_dump_json())['choices'][0]['message']['content'])))
    similarity = float(json.dumps(json.loads(response.model_dump_json())['choices'][0]['message']['content']).replace('"','').replace("'",''))
    return similarity



def update_cluster_description(cluster_description, new_description):
    response = client.chat.completions.create(
        model="gpt-3.5-turbo",
        messages=[{
            "role": "user" , 
            "content": f"Update the event description message '{cluster_description}' to contain any new information conveyed \
                in the new message '{new_description}'. You must respond with 'None' if no new information is conveyed. \
                Respond with the updated description message.\
                It is absolutely paramount that you respond with only the updated description message, or 'None' if no new information is conveyed.\
                The penalties for providing additional information, or any preamble such as 'Here is the updated message' or anything similar\
                are extremely severe.",
        }],
        max_tokens=200,
        temperature=0,
    )
    print("\n\n\n"+json.dumps(json.loads(response.model_dump_json())['choices'][0]['message']['content'])+"\n\n\n\n")
    return json.dumps(json.loads(response.model_dump_json())['choices'][0]['message']['content'])


def summarize_description(description):
    response = client.chat.completions.create(
        model="gpt-3.5-turbo",
        messages=[{
            "role": "user" , 
            "content":f"Summarize the emergency description message '{description}'.\
                It is absolutely paramount that you respond with only the description message.\
                The penalties for providing additional information, or any preamble such as 'Here is the summary message' or anything similar\
                are extremely severe.", }],
        max_tokens=200,
        temperature=0,
    )
    print(json.dumps(json.loads(response.model_dump_json())['choices'][0]['message']['content']))
    return json.dumps(json.loads(response.model_dump_json())['choices'][0]['message']['content'])

    
    


def calculate_score(entry1, entry2):
    print("............")
    print(str(entry1))
    print(str(entry2))
    print("............")
    similarity = llm_similarity(str(entry1), str(entry2))
    print("similarity: " + str(similarity))
    return similarity


def cluster_reports(new_report_id):
    try:
        db_client = MongoClient(MONGO_URI)
        reports_collection = db_client['Dashboard']['Reports']
        clusters_collection = db_client['Dashboard']['Clusters']
        # Retrieve existing clusters from the database
        clusters = list(clusters_collection.find())
        
        print("\n\n\n\n\n\n\nnew_report_id: " + str(new_report_id))
        # Compare the new report with the representative reports of existing clusters
        # if new_report_id is a json key value apr, take the value
        if isinstance(new_report_id, dict):
            new_report_id = new_report_id['new_report_id']
        # if it is a json key value pair as a string, convert to json and then take the value
        if isinstance(new_report_id, str):
            if new_report_id.startswith("{"):
                new_report_id = json.loads(new_report_id)
                new_report_id = new_report_id['new_report_id']
                
                        
        max_score = -1
        max_cluster = None
        new_report = reports_collection.find_one({'_id': ObjectId(new_report_id)})
        for cluster in clusters:
            representative = cluster['representative']
            score = calculate_score(new_report, representative)
            if score > max_score:
                max_score = score
                max_cluster = cluster

        if max_score > 0.5:
            # Add the new report into the existing cluster
            cluster_name = max_cluster['name']
            reports_collection.update_one({'_id': new_report['_id']}, {'$set': {'cluster': cluster_name}})
            
            # Update the cluster representative if needed
            if new_report['timestamp'] < max_cluster['representative']['timestamp']:
                # update the timestamp of representative
                max_cluster['representative']['timestamp'] = new_report['timestamp']
            
            response = update_cluster_description(max_cluster['representative']['description'], new_report['description'])
            if response != "None":
                max_cluster['representative']['description'] = response
            
            # add member
            max_cluster['members'].append(new_report['_id'])
            # update it in the database
            
            clusters_collection.update_one({'name': cluster_name}, {'$set': {'representative': max_cluster['representative'], 'members': max_cluster['members']}}, upsert=True)
        else:
            # Create a new cluster for the new report
            cluster_name = f"Cluster_{clusters_collection.count_documents({}) + 1}"
            reports_collection.update_one({'_id': new_report['_id']}, {'$set': {'cluster': cluster_name}})
            
            cluster = {
                'name': cluster_name,
                'representative': new_report,
                'members': [new_report['_id']]
            }
            clusters_collection.insert_one(cluster)
            
        db_client.close()
        
        return [0, cluster_name]

    except Exception as e:
        return [-1, str(e)]
        
        

def cleanup_clusters():
    try:
        db_client = MongoClient(MONGO_URI)
        reports_collection = db_client['Dashboard']['Reports']
        clusters_collection = db_client['Dashboard']['Clusters']

        # Retrieve all clusters from the database
        clusters = list(clusters_collection.find())

        for cluster in clusters:
            cluster_name = cluster['name']
            members = cluster['members']

            # Check each member of the cluster
            valid_members = []
            for member_id in members:
                # Check if the member exists in the Reports collection
                member = reports_collection.find_one({'_id': member_id})
                if member:
                    valid_members.append(member_id)

            # Update the cluster with valid members
            clusters_collection.update_one(
                {'name': cluster_name},
                {'$set': {'members': valid_members}}
            )

            # Delete the cluster if it has no members
            if len(valid_members) == 0:
                clusters_collection.delete_one({'name': cluster_name})

        db_client.close()

        return [0, "Cleanup completed successfully"]
    except Exception as e:
        return [-1, str(e)]
    
    
    
# function to intialize the clustering. finds all reports with no cluster and clusters them
def initialize_clustering():
    try:
        db_client = MongoClient(MONGO_URI)
        reports_collection = db_client['Dashboard']['Reports']
        clusters_collection = db_client['Dashboard']['Clusters']
        # Retrieve existing clusters from the database
        clusters = list(clusters_collection.find())
        reports = list(reports_collection.find({'cluster': {'$exists': False}}))
        for report in reports:
            max_score = -1
            max_cluster = None
            for cluster in clusters:
                representative = cluster['representative']
                score = calculate_score(report, representative)
                print("score: " + str(score))
                if score > max_score:
                    max_score = score
                    max_cluster = cluster
            if max_score > threshold:
                # Add the new report into the existing cluster
                cluster_name = max_cluster['name']
                reports_collection.update_one({'_id': report['_id']}, {'$set': {'cluster': cluster_name}})
                
                # Update the cluster representative if needed
                if report['timestamp'] < max_cluster['representative']['timestamp']:
                    # update the timestamp of representative
                    max_cluster['representative']['timestamp'] = report['timestamp']
                
                if len(report['description']) > 120:
                    report['description'] = summarize_description(report['description'])

                response = update_cluster_description(max_cluster['representative']['description'], report['description'])
                if response != "None":
                    max_cluster['representative']['description'] = response
                
                # add member
                max_cluster['members'].append(report['_id'])
                
                clusters_collection.update_one({'name': cluster_name}, {'$set': {'representative': max_cluster['representative']}})            

            else:
                # Create a new cluster for the new report
                if len(report['description']) > 120:
                    report['description'] = summarize_description(report['description'])
                cluster_name = f"Cluster_{clusters_collection.count_documents({}) + 1}"
                reports_collection.update_one({'_id': report['_id']}, {'$set': {'cluster': cluster_name}})
                cluster = {
                    'name': cluster_name,
                    'representative': report,
                    'members': [report['_id']]
                }
                clusters_collection.insert_one(cluster)
        db_client.close()
        return [0, "Initialization completed successfully"]
    except Exception as e:
        return [-1, str(e)]
    
    
    
if __name__ == "__main__":
    initialize_clustering() ; 
    
    
    
    
