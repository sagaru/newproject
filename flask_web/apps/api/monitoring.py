from flask_login import current_user, login_required
from flask import jsonify, request
from apps.models import DashboardUser, MongoDBClient
from apps.app_init import logger
from apps.config import Config
import requests
import math
from bson import ObjectId
import aiohttp
import asyncio

def init_app(app):
    @app.route('/get_amenity', methods=['POST'])
    def get_amenity():
        amenity_name = request.get_json().get('amenity')
        response = requests.get(Config.MAP_API + '/get_amenity_coordinates', params={'amenity_name': amenity_name})

        if response.status_code != 200:
            app.logger.error(f"Error from external API: {response.status_code} {response.text}")
            return jsonify({'error': 'External API error'}), response.status_code

        try:
            data = response.json()
            ret = [{'name': amenity['properties']['name'], 'location': amenity['geometry']['coordinates']} for amenity in data]
            return jsonify(ret)
        except ValueError:
            app.logger.error(f"Invalid JSON response: {response.text}")
            return jsonify({'error': 'Invalid JSON response from external API'}), 500
        
    
    @app.route('/locate-users', methods=['GET'])
    def locate_users():
        if current_user.is_authenticated:
            client = MongoDBClient.get_client()
            db = client.MobileApp
            users = db.Users.find({})
            ret = [{'phone': user['phone'], 'location': [user['location']['coordinates'][1], user['location']['coordinates'][0]]} for user in users]
            for user in users:
                app.logger.warning([user['location']['coordinates'][1], user['location']['coordinates'][0]])

            # this expects the collection to have the schema: {'username': str, 'location': {'coordinates': [float, float], 'timestamp': datetime}}
            app.logger.warning("ret:" + str(ret))
            return jsonify(ret)
        else:
            return jsonify({'error': 'User not authenticated'}), 401
        
    @app.route('/send-waypoint', methods=['POST'])
    def send_waypoint():
        if current_user.is_authenticated:
            client = MongoDBClient.get_client()
            db = client.MobileApp
            req = request.get_json()
            req = dict(req)
            app.logger.warning("send-waypint:" + str(req))
            coords = req['coordinates']
            coords = [round(coord, 7) for coord in coords]
            timestamp = req['timestamp']
            radius = float(req['radius'])
            ret = db.Waypoints.insert_one({'waypoint': {'coordinates': coords, 'timestamp': timestamp, 'radius': radius}})
            
            # response = requests.get(Config.NODEJS_API + '/update-waypoints')
            # app.logger.warning("send-waypoint: " + str(response.status_code))
            
            # for all users iun MobileApp.Users, calculate the distance to the waypoint
            # if user location is within the radius of the waypoint, update user db entry to have 'waypoint' field with coordinates and timestamp, unless it already has a waypoint field
            
            users = db.Users.find({})
            for user in users:
                user_coords = user['location']['coordinates']
                distance = math.sqrt((coords[0] - user_coords[0])**2 + (coords[1] - user_coords[1])**2)
                if distance <= radius:
                    # update user db entry
                    ret = db.Users.update_one({'_id': ObjectId(user['_id'])}, {'$set': {'waypoint': {'coordinates': coords, 'timestamp': timestamp}}})
                    if ret.acknowledged == 200:
                        app.logger.warning(f"Updated user {user['phone']} with waypoint")
                    else:
                        app.logger.error(f"Failed to update user {user['phone']} with waypoint")
            
            
            
            
            if ret.acknowledged == 200:
                return jsonify({'success': True}), 200
            else:
                return jsonify({'error': 'Failed to insert waypoint'}), 500
            
            
    @app.route('/get-waypoints', methods=['GET'])
    def get_waypoints():
        if current_user.is_authenticated:
            client = MongoDBClient.get_client()
            db = client.MobileApp
            waypoints = db.Waypoints.find({})
            ret = [{'waypoint': waypoint['waypoint']} for waypoint in waypoints]
            return jsonify(ret)
        else:
            return jsonify({'error': 'User not authenticated'}), 401
        
        
    @app.route('/delete-waypoint', methods=['POST'])
    def delete_waypoint():
        # request gives index (0=indexed) of waypoint to delete, in order of insertion.
        # delete the ith waypoint from the collection
        if current_user.is_authenticated:
            client = MongoDBClient.get_client()
            db = client.MobileApp
            index = request.get_json().get('index')
            waypoints = db.Waypoints.find({})
            waypoints = [waypoint for waypoint in waypoints]
            if index < len(waypoints):
                ret = db.Waypoints.delete_one({'_id': waypoints[index]['_id']})
                if ret.acknowledged:
                    return jsonify({'success': True}), 200
                else:
                    return jsonify({'error': 'Failed to delete waypoint'}), 500
            else:
                return jsonify({'error': 'Index out of range'}), 400
        else:
            return jsonify({'error': 'User not authenticated'}), 401
        
        
        
    @app.route('/fetch-vehicle-positions', methods=['GET'])
    async def fetch_vehicle_positions():
        url = "https://api.nationaltransport.ie/gtfsr/v2/Vehicles?format=json"
        headers = {
            "x-api-key": "c099921ae82a464d9cd553bdb0d5af5b"
        }

        async with aiohttp.ClientSession() as session:
            try:
                async with session.get(url, headers=headers) as response:
                    response.raise_for_status()  # Raises HTTPError for bad responses
                    data = await response.json() # Asynchronously get json response
                    
                    

                    if 'entity' in data:
                        vehicles_with_positions = [entity for entity in data['entity']
                                                if 'vehicle' in entity and 'position' in entity['vehicle']]

                        if not vehicles_with_positions:
                            print("No vehicles with position data available.")
                            return

                        results = []
                        for entity in vehicles_with_positions:
                            vehicle_id = entity['vehicle']['vehicle']['id']
                            position = entity['vehicle']['position']
                            latitude = position['latitude']
                            longitude = position['longitude']
                            results.append(f"Bus ID: {vehicle_id} is at Latitude: {latitude}, Longitude: {longitude}")
                        
                        return jsonify(results), 200
                    else:
                        return jsonify({'error': 'No entity key in response'}), 500

            except aiohttp.ClientError as e:
                print(f"Error fetching data from API: {e}")
                return str(e), 500

