#!/bin/bash

# Set the name of the consumer group to delete
GROUP_NAME="general_mobile_group"

# Set the Kafka broker address
BROKER_ADDRESS="localhost:29092"

# Set the name or ID of your Kafka Docker container
KAFKA_CONTAINER_NAME="broker"

# List all consumer groups
echo "Listing all consumer groups..."
docker exec $KAFKA_CONTAINER_NAME kafka-consumer-groups --bootstrap-server $BROKER_ADDRESS --list


read -p "Do you want to delete the consumer group '$GROUP_NAME'? (y/N) " confirm
if [[ $confirm == [yY] || $confirm == [yY][eE][sS] ]]
then
    # Delete the specified consumer group
    echo "Deleting consumer group '$GROUP_NAME'..."
    docker exec $KAFKA_CONTAINER_NAME kafka-consumer-groups --bootstrap-server $BROKER_ADDRESS --delete --group $GROUP_NAME
else
    echo "Operation cancelled."
fi
