from flask_login import current_user, login_required
from flask import jsonify, request
from apps.models import DashboardUser, MongoDBClient
from apps.app_init import logger
import json
from apps.authentication.util import format_phone

def init_app(app):
    @app.route('/kafka-consumer/get-messages', methods=['POST'])
    @login_required
    def get_messages_kafka():
        recipient = format_phone(request.get_json()['recipient'])
        user = DashboardUser.find_by_email(current_user.email)
        user.subscribe_consumer("DM_"+recipient)
        messagesKafka = user.get_messages(decode=False, forUser=recipient)
        app.logger.info("Found {} messages".format(len(messagesKafka)))
        messages = []
        for message in messagesKafka:
            if message.topic().split('_')[1] == recipient:
                value = message.value().decode('utf-8').split('::')
                app.logger.warning("message value list: {}".format(str(value)))
                direction = 'inbound' if 'user' in value[0] else 'outbound'
                if 'alert' in value[0]:
                    direction = 'alert'
                messages.append({'value': value[-1], 'time': message.timestamp(), 'direction': direction})
        
        # message.value, message.direction, message.time
        return jsonify(messages), 200
    
        
        
    
    @app.route('/kafka-prod/alert-users-in-area', methods=['POST'])
    def alert_users_in_area():
        data = request.get_json()
        while len(data) == 1:       # remove nested list(s)
            data = data[0]
        user = DashboardUser.find_by_email(current_user.email)
        #  from javascript POST:           body: JSON.stringify({data: msg, time: time, date: date, mapArea: mapArea})
        msg = dict()
        # msg['time'] = data['time']
        # msg['date'] = data['date']
        # msg['msg'] =  data['data']
        # msg['type'] = 'alert'
        msg['coords'] = data['mapArea']
        msg['text'] = data['text']
        # dict as string
        msg = "alert::" + json.dumps(msg)
        user.send_message(msg, 'general_mobile_topic')      # (all) mobile users will compute if they are in the area and perform other actions
        
        # save to db
        client = MongoDBClient.get_client()
        db = client.Dashboard
        alerts_collection = db.Alerts
        alerts_collection.insert_one(data)
        app.logger.info(data)
        
        return jsonify({'success': True}), 200
    
    @app.route('/save-alert-to-db', methods=['POST'])
    def save_alert_to_db():
        client = MongoDBClient.get_client()
        db = client.Dashboard
        alerts_collection = db.Alerts
        data = request.get_json()
        alerts_collection.insert_one(data)
        app.logger.info(data)
        return jsonify({'success': True}), 200
    
    @app.route('/get-prev-alerts', methods=['GET'])
    def get_prev_alerts():
        client = MongoDBClient.get_client()
        db = client.Dashboard
        alerts_collection = db.Alerts
        cursor = alerts_collection.find()
        cursor = list(cursor)
        alerts = []
        for c in cursor:
            del c['_id']
            alerts.append(c)
        if cursor:
            app.logger.info('got alerts')
            return jsonify({'alerts': alerts}), 200
        else:
            return jsonify({'alerts': []}), 200
        
    
    @app.route('/kafka-prod/DM', methods=['POST'])
    @login_required
    def DM():
        msg = request.get_json()['msg']
        recipient = request.get_json()['recipient']
        user = DashboardUser.find_by_email(current_user.email)
        user.send_message(msg, "DM_"+format_phone(recipient))
        return jsonify({'success': True}), 200
    
    
    @app.route('/get-users', methods=['GET'])
    @login_required
    def get_users():
        client = MongoDBClient.get_client()
        db = client.MobileApp
        users_collection = db.Users
        cursor = users_collection.find()
        cursor = list(cursor)
        users = []
        for c in cursor:
            users.append(c['phone'])
            
        return jsonify({'users': users}), 200
    

    
