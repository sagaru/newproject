from apps.config import Config
from flask import render_template, jsonify, request
import requests
from pymongo import MongoClient
from flask_login import current_user, login_required
from apps.models import MongoDBClient
from bson import ObjectId


def init_app(app):
    @app.route('/get-clusters', methods=['GET'])
    def get_clusters():
        try:
            client = MongoClient(Config.MONGO_URI)
            db = client['Dashboard']
            collection = db['Clusters']
            clusters = list(collection.find())
            # ObjectID --> str
            for cluster in clusters:
                cluster['_id'] = str(cluster['_id'])
                cluster['representative']['_id'] = str(cluster['representative']['_id'])
                for i in range(len(cluster['members'])):
                    cluster['members'][i] = str(cluster['members'][i])
                app.logger.info(cluster)
            return jsonify({'clusters': clusters}), 200
        except Exception as e:
            return jsonify({'error': str(e)}), 500
