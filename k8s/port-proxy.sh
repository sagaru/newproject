#!/bin/bash

package_name=socat

if dpkg -s $package_name &> /dev/null; then
	echo "$package_name is already installed"
else
	sudo apt update
	sudo apt install -y $package_name
fi



sudo socat TCP-LISTEN:80,fork TCP:$(minikube ip):80
