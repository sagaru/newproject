import os
from flask_minify import Minify  
from sys import exit
from apps.config import config_dict
from apps import create_app
from apps.models import MongoDBClient
from dotenv import load_dotenv
load_dotenv()
from datetime import timedelta

# WARNING: Don't run with debug turned on in production!
DEBUG = bool(os.getenv('DEBUG', False))

# The configuration
# get_config_mode = 'Debug' if DEBUG else 'Production'

try:
    # Load the configuration using the default values
    app_config = config_dict['Production']
except KeyError:
    exit('Error: Invalid <config_mode>. Expected values [Debug, Production]')

app = create_app(app_config)
app.debug = DEBUG


app.config['PERMANENT_SESSION_LIFETIME'] = timedelta(days=1) 


if not DEBUG:
    Minify(app=app, html=True, js=False, cssless=False)

if DEBUG:
    app.logger.info('DEBUG            = ' + str(DEBUG))
    app.logger.info('Page Compression = ' + ('FALSE' if DEBUG else 'TRUE'))
    app.logger.info('DBMS             = ' + app_config.MONGO_URI)
    app.logger.info('ASSETS_ROOT      = ' + app_config.ASSETS_ROOT)




@app.teardown_appcontext
def shutdown_session(exception=None):
    MongoDBClient.close_client()


if __name__ == "__main__":
    app.run()
