#download and install helm, a the package manager for kubernetes add-ons
wget https://get.helm.sh/helm-v3.13.2-linux-amd64.tar.gz
tar -zxvf helm-v3.13.2-linux-amd64.tar.gz
sudo mv linux-amd64/helm /usr/local/bin/helm
helm repo add stable https://charts.helm.sh/stable
helm repo update

# add strimzi
helm repo add strimzi http://strimzi.io/charts/	
helm install strimzi-kafka-operator strimzi/strimzi-kafka-operator
