import 'dart:async';
import 'package:flutter/material.dart';

class TrafficImageMonitoringPage extends StatefulWidget {
  const TrafficImageMonitoringPage({Key? key}) : super(key: key); // Added const constructor

  @override
  _TrafficImageMonitoringPageState createState() => _TrafficImageMonitoringPageState();
}

class _TrafficImageMonitoringPageState extends State<TrafficImageMonitoringPage> {
  late String imageUrl; // Use 'late' for lazy initialization
  Timer? _timer; // Make '_timer' nullable

  @override
  void initState() {
    super.initState();
    imageUrl = _getImageUrl(); // Initialize 'imageUrl' here
    _timer = Timer.periodic(const Duration(minutes: 1), (Timer t) => _updateImage());
  }

  @override
  void dispose() {
    _timer?.cancel(); // Cancel the timer if it's initialized
    super.dispose();
  }

  void _updateImage() {
    if (mounted) { // Check if the widget is still in the tree
      setState(() {
        imageUrl = _getImageUrl(); // Update 'imageUrl' with a new value
      });
    }
  }

  String _getImageUrl() {
    final timestamp = DateTime.now().millisecondsSinceEpoch;
    return 'https://tiitrafficdata.azurewebsites.net/api/Camera/?name=4207_cam1.jpeg&type=WeatherCameraImages&v=$timestamp';
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Traffic Camera Image'),
      ),
      body: Center(
        child: Image.network(imageUrl, fit: BoxFit.cover), // Added BoxFit.cover for better layout
      ),
    );
  }
}
