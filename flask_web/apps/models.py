from flask_login import UserMixin, current_user, login_required
from pymongo import MongoClient
from bson.objectid import ObjectId
from apps.authentication.util import hash_pass, format_phone
from confluent_kafka import Consumer, KafkaError, Producer
from confluent_kafka.admin import AdminClient, NewTopic
from apps.config import Config as cfg
from apps.app_init import logger
import json
import time
import uuid


class MongoDBClient:
    _client = None

    @classmethod
    def get_client(cls):
        if cls._client is None:
            cls._client = MongoClient(cfg.MONGO_URI)
        return cls._client

    @classmethod
    def close_client(cls):
        if cls._client is not None:
            cls._client.close()
            cls._client = None


class DashboardUser(UserMixin):

    def __init__(self, user_data):
        self.email = user_data.get("email")
        self.phone = format_phone(user_data.get("phone"))
        self.password = user_data.get("password")
        self.id = str(user_data.get("_id"))
        self.kafka_topics = list(set(user_data.get("kafka_topics", [])))
        self.kafka_groups = list(set(user_data.get("kafka_groups", [])))
        self.kafka_consumer = None
        self.kafka_producer = None
        self.kafka_admin = None
        self.initialize_kafka()


    def initialize_kafka(self):
        if self.kafka_topics and self.kafka_groups:
            # Initialize Kafka consumer, producer, and admin clients
            self.kafka_consumer = Consumer({
                "bootstrap.servers": cfg.KAFKA_URI,
                "group.id": self.kafka_groups[0],  # Assuming each user uses their first group ID
                "auto.offset.reset": "earliest",
                "enable.auto.commit": False,
            })

            self.kafka_producer = Producer({"bootstrap.servers": cfg.KAFKA_URI})
            self.kafka_admin = AdminClient({"bootstrap.servers": cfg.KAFKA_URI})

            # Check and create topics if they do not exist
            topic_metadata = self.kafka_admin.list_topics(timeout=10)
            existing_topics = topic_metadata.topics
            logger.info("existing:" + str(list(existing_topics.keys())))
            
            topics_to_create = [topic for topic in self.kafka_topics if topic not in existing_topics]
            
            new_topics = [NewTopic(topic, num_partitions=1, replication_factor=1) for topic in topics_to_create]
            if new_topics:
                self.kafka_admin.create_topics(new_topics)

                # Wait for each new topic to be created
                for topic in topics_to_create:
                    while True:
                        detail = self.kafka_admin.list_topics(topic).topics
                        if topic in detail:
                            break
                        time.sleep(0.5)  # Sleep briefly to avoid a tight loop
                        
            for topic in list(existing_topics.keys()):
                if "DM_" in str(topic) and str(topic):
                    self.kafka_topics.append(topic)

            # Subscribe the consumer to all topics
            self.kafka_topics = list(set(self.kafka_topics))
            self.kafka_consumer.subscribe(self.kafka_topics)

                
        

    @classmethod
    def create_user(cls, email, phone, password, **kwargs):
        db = MongoDBClient.get_client().Dashboard
        users_collection = db.Users

        if users_collection.find_one({"email": email}) or users_collection.find_one(
            {"phone": phone}
        ):
            raise ValueError("A user with the given email or phone already exists.")

        password_hash = hash_pass(password)
        phone_formatted = format_phone(phone)
        user_data = {
            "email": email,
            "password": password_hash,
            "phone": phone_formatted,
            "kafka_topics": kwargs.get("kafka_topics", ["general_dashboard_topic"]),
            "kafka_groups": kwargs.get("kafka_groups", ["general_dashboard_group"]),
        }

        user_id = cls.save(user_data)
        user_data["_id"] = ObjectId(user_id)
        user_instance = cls(user_data)
        user_instance.initialize_kafka()  # we must re-initialise the kafka consumer every time the user is loaded up
        return user_instance

    @classmethod
    def save(cls, user_data):
        db = MongoDBClient.get_client().Dashboard
        users_collection = db.Users
        result = users_collection.insert_one(user_data)
        return str(result.inserted_id)

    def subscribe_consumer(self, topic):
        if self.kafka_consumer:
            try:
                topic_metadata = self.kafka_admin.list_topics(timeout=8)
                if topic not in topic_metadata.topics:
                    bootstrap_server_response = self.kafka_admin.create_topics(
                        [NewTopic(topic, num_partitions=1, replication_factor=1)]
                    )

                    while not bootstrap_server_response[topic].done():
                        time.sleep(0.2)  # Sleep to avoid busy-waiting

                    if bootstrap_server_response[topic].exception():
                        raise bootstrap_server_response[topic].exception()

                    self.kafka_consumer.subscribe([topic])
                    self.kafka_topics.append(topic)
                    self.kafka_topics = list(set(self.kafka_topics))

                    db = MongoDBClient.get_client().Dashboard
                    users_collection = db.Users
                    users_collection.update_one(
                        {"_id": ObjectId(self.id)},
                        {"$set": {"kafka_topics": self.kafka_topics}},
                    )

                    return True

            except KafkaError as e:
                logger.error(f"Kafka error: {e}")
                return False
            except Exception as e:
                logger.error(f"General error: {e}")
                return False

    def unsubscribe_consumer(self, topic):
        if self.kafka_consumer:
            self.kafka_consumer.unsubscribe([topic])
            self.kafka_topics.remove(topic)
            db = MongoDBClient.get_client().Dashboard
            users_collection = db.Users
            users_collection.update_one(
                {"_id": ObjectId(self.id)},
                {"$set": {"kafka_topics": self.kafka_topics}},
            )
            return True

    def get_messages(self, forUser=None, timeout=1.0, decode=True):
        if not self.kafka_consumer:
            return None

        # Create a new consumer instance for the specific topic
        consumer = Consumer(
            {
                "bootstrap.servers": cfg.KAFKA_URI,
                "group.id": str(uuid.uuid4()),  # Generates a unique group ID for each run
                "auto.offset.reset": "earliest",  # Start reading from the earliest available offset
                "enable.auto.commit": False,
            }
        )

        # Subscribe the consumer to the specified topic
        topic = "DM_" + forUser
        consumer.subscribe([topic])

        messages = []
        while True:
            msg = consumer.poll(timeout)
            if msg is None:
                break
            if not msg.error():
                if decode:
                    messages.append(msg.value().decode("utf-8"))
                else:
                    messages.append(msg)
            elif msg.error().code() != KafkaError._PARTITION_EOF:
                logger.error(msg.error())
                break

        # Close the consumer after retrieving the messages
        consumer.close()
        del consumer

        return messages

    def send_message(self, message, topic):
        if self.kafka_producer:

            logger.info(f"\n\n\nSending message to topic {topic}\n\n")

            # Convert the dictionary to a JSON string and then encode it to bytes
            message_bytes = json.dumps(message).encode("utf-8")

            self.kafka_producer.produce(
                topic, message_bytes, callback=callback_send_message
            )
            self.kafka_producer.flush()
        else:
            logger.info("\n\n\n** No producer available **\n\n\n\n")

    def __repr__(self):
        return f"<User {self.email}>"

    def get_id(self):
        return self.id

    @classmethod
    def find_by_id(cls, user_id):
        try:
            db = MongoDBClient.get_client().Dashboard
            users_collection = db.Users
            user_data = users_collection.find_one({"_id": ObjectId(user_id)})
            if user_data:
                user_instance = cls(user_data)
                if not user_instance.kafka_consumer:
                    user_instance.initialize_kafka()  # we must re-initialise the kafka consumer every time the user is loaded up
                return user_instance
        except Exception as e:
            logger.error(e)
            pass
        return None

    @classmethod
    def find_by_email(cls, email):
        db = MongoDBClient.get_client().Dashboard
        user_data = db.Users.find_one({"email": email})
        if user_data:
            user_instance = cls(user_data)
            if not user_instance.kafka_consumer:
                user_instance.initialize_kafka()  # we must re-initialise the kafka consumer every time the user is loaded up
            return user_instance
        return None

    @classmethod
    def find_by_phone(cls, phone):
        db = MongoDBClient.get_client().Dashboard
        user_data = db.Users.find_one({"phone": phone})
        if user_data:
            user_instance = cls(user_data)
            # if not user_instance.kafka_consumer:
            user_instance.initialize_kafka()  # we must re-initialise the kafka consumer every time the user is loaded up
            return user_instance
        return None


class Report:

    def __init__(
        self,
        user=None,
        report_type=None,
        report_date=None,
        report_time=None,
        report_location=None,
        report_description=None,
        report_status=None,
        report_image=None,
    ):
        self.user = user
        self.report_type = report_type
        self.report_date = report_date
        self.report_time = report_time
        self.report_location = report_location
        self.report_description = report_description
        self.report_status = report_status
        self.report_image = report_image

    def save(self):
        db = MongoDBClient.get_client().Dashboard
        reports_collection = db.Reports
        report_data = {
            "user": self.user,
            "report_type": self.report_type,
            "report_date": self.report_date,
            "report_time": self.report_time,
            "report_location": self.report_location,
            "report_description": self.report_description,
            "report_status": self.report_status,
            "report_image": self.report_image,
        }
        ret = reports_collection.insert_one(report_data)
        self.id = str(ret.inserted_id)

    def __repr__(self):
        return f"<Report {self.id}>"

    @staticmethod
    def find_by_id(report_id):
        db = MongoDBClient.get_client().Dashboard
        reports_collection = db.Reports
        ret = reports_collection.find_one({"_id": ObjectId(report_id)})
        return ret

    @staticmethod
    def find_by_user(user):
        db = MongoDBClient.get_client().Dashboard
        reports_collection = db.Reports
        ret = reports_collection.find({"user": user})
        return ret

    @staticmethod
    def find_by_status(report_status):
        db = MongoDBClient.get_client().Dashboard
        reports_collection = db.Reports
        ret = reports_collection.find({"report_status": report_status})
        return ret


def callback_send_message(err, msg):
    if err:
        logger.error(err)
    else:
        logger.info("Message sent: {}".format(msg))
