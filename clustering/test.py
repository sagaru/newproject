import unittest
from unittest.mock import patch, MagicMock
import clustering
from clustering import gpt4_similarity, weight_decay, norm_distance, norm_time, sigmoid10, calculate_score, DBentry
import json 

class TestClustering(unittest.TestCase):

    @patch('clustering.client.chat.completions.create')
    def test_gpt4_similarity(self, mock_completion):
        mock_completion.return_value = MagicMock(choices=[MagicMock(message=MagicMock(content="0.75"))])
        result = gpt4_similarity("word1", "word2")
        print(result)
        self.assertEqual(result, "0.75")

    def test_weight_decay(self):
        result = weight_decay(10, 5, 2)
        self.assertIsInstance(result, float)
     
    def test_norm_distance(self):
        with patch('clustering.haversine.haversine', return_value=10):
            result = norm_distance(1, 2, 3, 4)
            self.assertIsInstance(result, float)

    def test_norm_time(self):
        result = norm_time("2023-01-01 00:00:00.000Z", "2023-01-02 00:00:00.000Z")
        self.assertIsInstance(result, float)

    def test_sigmoid10(self):
        result = sigmoid10(0.5)
        self.assertAlmostEqual(result, 0.5)


    def test_calculate_score(self):
        print("""
            similar reports - same referent
            ~250 m distance as crow flies
            2 hrs time dif
        """)
        report1 = "There is a fire outbreak in the downtown area, and the firefighters are on the scene."
        report2 = "Emergency services are responding to a fire in the city center, and firefighters are actively working to control it."
        entry1 = DBentry(json.dumps({"description": report1, "location": {"lat": 53.34367, "lng": -6.256354}, "timestamp": "2023-01-02 13:00:00.000Z", "phone": True}))
        entry2 = DBentry(json.dumps({"description": report2, "location": {"lat": 53.345464, "lng": -6.246313}, "timestamp": "2023-01-02 15:00:00.000Z", "phone": False}))
        result = calculate_score(entry1, entry2)
        print("1. MY SCORE:", result)
        self.assertTrue(0 <= result <= 1)


    def test_calculate_score_2(self):
        print("""
            dissimilar reports - different referents
            2km distance as crow flies
            30 min time dif
        """)
        report1 = "There has been a chemical spill near the industrial zone, and hazmat teams are responding swiftly to contain the situation. The spill has resulted in the evacuation of nearby residents, and emergency services are on high alert to mitigate the potential health risks. Authorities are urging the public to avoid the affected area and follow safety instructions broadcasted through local media channels."
        report2 = "Emergency services are responding to a fire in the city center, and firefighters are actively working to control it."
        entry1 = DBentry(json.dumps({"description": report1, "location": {"lat": 53.34367, "lng": -6.256354}, "timestamp": "2023-01-02 13:00:00.000Z", "phone": True}))
        entry2 = DBentry(json.dumps({"description": report2, "location": {"lat": 53.342430, "lng": -6.276846}, "timestamp": "2023-01-02 13:30:00.000Z", "phone": False}))
        result = calculate_score(entry1, entry2)
        print("2. MY SCORE:", result)
        self.assertTrue(0 <= result <= 1)

    def test_calculate_score_3(self):
        print("""
            similar reports - same referent
            ~9km distance as crow flies
            9 hrs time dif
        """)
        report1 = "There is a fire outbreak in the downtown area, and the firefighters are on the scene."
        report2 = "Emergency services are responding to a fire in the city center, and firefighters are actively working to control it."
        entry1 = DBentry(json.dumps({"description": report1, "location": {"lat": 53.298726, "lng": -6.177690}, "timestamp": "2023-01-02 10:00:00.000Z", "phone": True}))
        entry2 = DBentry(json.dumps({"description": report2, "location": {"lat": 53.368387, "lng": -6.255900}, "timestamp": "2023-01-02 19:00:00.000Z", "phone": False}))
        result = calculate_score(entry1, entry2)
        print("3. MY SCORE:", result)
        self.assertTrue(0 <= result <= 1)


    def test_calculate_score_4(self):
        print("""
            dissimilar reports - different referents
            ~250 distance as crow flies
            0 hrs time dif
        """)
        report1 = "A traffic accident occurred during rush hour in the city center, leading to significant delays. Emergency services are on the scene to assist the injured, and commuters are advised to seek alternate routes."
        report2 = "Public health officials have issued a warning about a potential flu outbreak in the region. Citizens are urged to take preventive measures, such as getting vaccinated and practicing good hygiene, to avoid the spread of the virus."
        entry1 = DBentry(json.dumps({"description": report1, "location": {"lat": 53.34367, "lng": -6.256354}, "timestamp": "2023-01-02 13:00:00.000Z", "phone": True}))
        entry2 = DBentry(json.dumps({"description": report2, "location": {"lat": 53.342430, "lng": -6.276846}, "timestamp": "2023-01-02 13:00:00.000Z", "phone": False}))
        result = calculate_score(entry1, entry2)
        print("4. MY SCORE:", result)
        self.assertTrue(0 <= result <= 1)


    def test_calculate_score_5(self):
        print("""
            similar reports - different referents
            ~250 distance as crow flies
            5 mins time dif
        """)
        report1 = "There is smoke coming from the Hamilton building at college green."
        report2 = "I think there is a fire in one of the lecture halls at TCD."
        entry1 = DBentry(json.dumps({"description": report1, "location": {"lat": 53.34367, "lng": -6.256354}, "timestamp": "2023-01-02 13:00:00.000Z", "phone": True}))
        entry2 = DBentry(json.dumps({"description": report2, "location": {"lat": 53.342430, "lng": -6.276846}, "timestamp": "2023-01-02 13:05:00.000Z", "phone": False}))
        result = calculate_score(entry1, entry2)
        print("5. MY SCORE:", result)
        self.assertTrue(0 <= result <= 1)

    def test_calculate_score_6(self):
        print("""
            dissimilar reports - different referents
            ~250 distance as crow flies
            5 mins time dif
        """)
        report1 = "Stately Buck Muligan came from the stearhead bearing a bowl of lather on which a mirror and a razor lay crossed"
        report2 = "There is a bomb at trinity. Please help"
        entry1 = DBentry(json.dumps({"description": report1, "location": {"lat": 53.34367, "lng": -6.256354}, "timestamp": "2023-01-02 13:00:00.000Z", "phone": True}))
        entry2 = DBentry(json.dumps({"description": report2, "location": {"lat": 53.342430, "lng": -6.276846}, "timestamp": "2023-01-02 13:05:00.000Z", "phone": False}))
        result = calculate_score(entry1, entry2)
        print("6. MY SCORE:", result)
        self.assertTrue(0 <= result <= 1)

    def test_calculate_score_7(self):
        print("""    
            similar reports - different referents
            ~250 distance as crow flies
            12 hrs time dif
        """)
        report1 = """Hello Emergency Services Team,
                I need urgent assistance! There's a fire in my apartment on Oak Street. The flames are getting out of control, and I'm really scared. Please send firefighters immediately to contain the fire. I've evacuated, and I'm waiting for help.

                Thank you,
                Olivia Owlglass"""
        report2 = "I want to report a fire incident in our neighborhood. It's on Elm Street. I saw smoke coming out of a building, and it seems like a serious situation. Please dispatch firefighters to handle it. I hope everyone is safe. - Paul Slothrop"
        entry1 = DBentry(json.dumps({"description": report1, "location": {"lat": 53.34367, "lng": -6.256354}, "timestamp": "2023-01-02 10:00:00.000Z", "phone": True}))
        entry2 = DBentry(json.dumps({"description": report2, "location": {"lat": 53.342430, "lng": -6.276846}, "timestamp": "2023-01-02 22:0:00.000Z", "phone": False}))
        result = calculate_score(entry1, entry2)
        print("7. MY SCORE:", result)
        self.assertTrue(0 <= result <= 1)

    def test_calculate_score_8(self):
        print(    """
            similar reports - different referents
            ~9km distance as crow flies
            9 hrs time dif
        """)
        report1 = """Hello Emergency Services Team,
                I need urgent assistance! There's a fire in my apartment on Oak Street. The flames are getting out of control, and I'm really scared. Please send firefighters immediately to contain the fire. I've evacuated, and I'm waiting for help.

                Thank you,
                Olivia Owlglass"""
        report2 = "I want to report a fire incident in our neighborhood. It's on Elm Street. I saw smoke coming out of a building, and it seems like a serious situation. Please dispatch firefighters to handle it. I hope everyone is safe. - Paul Slothrop"
        entry1 = DBentry(json.dumps({"description": report1, "location": {"lat": 53.298726, "lng": -6.177690}, "timestamp": "2023-01-02 10:00:00.000Z", "phone": True}))
        entry2 = DBentry(json.dumps({"description": report2, "location": {"lat": 53.368387, "lng": -6.255900}, "timestamp": "2023-01-02 19:00:00.000Z", "phone": False}))
        result = calculate_score(entry1, entry2)
        print("8. MY SCORE:", result)
        self.assertTrue(0 <= result <= 1)


    # def test_DBentry(self):
    #     entry = DBentry(json.dumps({"timestamp": "2024-01-01 00:00:00.000Z", "description": "test", "location": {"lat": 1, "lng": 2}, "phone": True}))
    #     self.assertEqual(entry.timestamp, "2024-01-01 00:00:00.000Z")
    #     self.assertEqual(entry.description, "test")
    #     self.assertEqual(entry.location, (1, 2))
    #     self.assertTrue(entry.phone)

if __name__ == '__main__':
    unittest.main()
