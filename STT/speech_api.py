from flask import Flask, jsonify, request
import os
import bson
from pydub import AudioSegment
from pymongo import MongoClient
from io import BytesIO
from google.cloud import speech
from google.cloud.speech import RecognitionConfig, RecognitionAudio
from google.oauth2 import service_account
from base64 import b64decode
import subprocess
from tempfile import NamedTemporaryFile

app = Flask(__name__)

# Environment variables
MONGO_URI = os.getenv('MONGO_URI', 'mongodb://localhost:27017/')
#GOOGLE_CREDENTIALS = os.getenv('GOOGLE_APPLICATION_CREDENTIALS', '')
GOOGLE_CREDENTIALS = os.getenv('GOOGLE_APPLICATION_CREDENTIALS', 'sa-key.json')

if not MONGO_URI or not GOOGLE_CREDENTIALS:
    app.logger.error('Environment variables not set')
    exit(1)

# Set up Google Speech-to-Text client
credentials = service_account.Credentials.from_service_account_file(GOOGLE_CREDENTIALS)
speech_client = speech.SpeechClient(credentials=credentials)

# MongoDB setup
client = MongoClient(MONGO_URI)
db = client['MobileApp']  
collection = db['AudioMessages']  


@app.route('/stt-transcribe', methods=['POST'])
def transcribe_voice_message():
    try:
        app.logger.warning('Transcribing voice message')
        # Check if there is data in the request
        if request.data:
            audio_data = request.data
        else:
            return jsonify({'message': 'No audio file provided'}), 400

        # Save the audio to a temporary FLAC file
        with NamedTemporaryFile(delete=False, suffix='.flac') as temp_flac:
            temp_flac.write(audio_data)
            temp_flac_path = temp_flac.name
        
        # Convert FLAC to WAV
        temp_wav_path = temp_flac_path.replace('.flac', '.wav')
        command = ['ffmpeg', '-i', temp_flac_path, temp_wav_path]
        subprocess.run(command, check=True)
        
        # Load the WAV file for transcription
        audio = AudioSegment.from_file(temp_wav_path, format='wav')
        sample_rate = audio.frame_rate

        # Prepare audio for recognition
        with open(temp_wav_path, 'rb') as audio_file:
            content = audio_file.read()
        
        audio_for_recognition = RecognitionAudio(content=content)
        config = RecognitionConfig(
            encoding=RecognitionConfig.AudioEncoding.LINEAR16,
            sample_rate_hertz=sample_rate,
            language_code="en-US"
        )

        # Transcribe the audio
        response = speech_client.recognize(config=config, audio=audio_for_recognition)
        transcription_results = response.results
        
        app.logger.info('Transcription results: %s', transcription_results)

        # Cleanup temporary files
        os.remove(temp_flac_path)
        os.remove(temp_wav_path)

        # Extract and return the transcription text
        if transcription_results:
            transcription_text = transcription_results[0].alternatives[0].transcript
            return jsonify({'transcription': transcription_text}), 200
        else:
            return jsonify({'transcription': 'No transcription found'}), 404

    except Exception as e:
        app.logger.error('Error during transcription: %s', str(e))
        return jsonify({'error': str(e)}), 500

if __name__ == '__main__':
    app.run(debug=True)








