import 'dart:async'; // Import the async library for Timer
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:flutter_app/user.dart';
import 'package:flutter_app/loginPage.dart';
import 'main.dart';

class KafkaChatPage extends StatefulWidget {
  const KafkaChatPage({Key? key}) : super(key: key);

  @override
  _KafkaChatPageState createState() => _KafkaChatPageState();
}

class ChatMessage {
  String text;
  bool isSender;

  ChatMessage({required this.text, this.isSender = false});

  bool isUserMessage() {
    return text.startsWith('user::');
  }

  String get displayText {
    return isUserMessage() ? text.replaceFirst('user::', '') : text;
  }
}

class _KafkaChatPageState extends State<KafkaChatPage> {
  final List<ChatMessage> _messages = [];
  final TextEditingController _controller = TextEditingController();
  Timer? _chatRefreshTimer;

  @override
  void initState() {
    super.initState();
    _startChatRefresh();
  }

  @override
  void dispose() {
    _chatRefreshTimer?.cancel(); // Cancel the timer when the widget is disposed
    super.dispose();
  }

  void _startChatRefresh() {
    // Refresh chat every 5 seconds
    _chatRefreshTimer = Timer.periodic(const Duration(seconds: 5), (timer) {
      _refreshChatMessages();
    });
  }

  void _refreshChatMessages() {
    List<ChatMessage> newMessages = [];
    user.getDmHistory.forEach((message) {
      print('message: $message') ;
      newMessages.add(
          ChatMessage(text: message, isSender: message.startsWith('user::')));
    });

    setState(() {
      _messages.clear();
      _messages.addAll(newMessages);
    });
  }

  Future<void> _sendMessage() async {
    String messageText = "user::${_controller.text}";
    if (messageText.isNotEmpty) {
      // final uri = Uri.parse('$serverAddr:$kafkaPort/topics/${user.getDmTopic}');
      final uri = Uri.parse('$serverAddr/topics/${user.getDmTopic}');
      print(uri);
      final headers = {'content-type': 'application/vnd.kafka.json.v2+json'};
      final body = jsonEncode({'records': [{'value': messageText}]});


      final response = await http.post(uri, headers: headers, body: body);

      if (response.statusCode == 200) {
        // show message to user
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(
            content: Text('Data submitted successfully'),
          ),
        );
      }
      else {
        // show error message to user
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            // include response code for debugging purposes
              content: Text('Error submitting data : ${response.reasonPhrase}')
          ),
        );
      }

      bool isAdded = user.addToDmHistory(messageText);
      if (isAdded) {
        setState(() {
          _messages.add(ChatMessage(text: messageText));
          _controller.clear();
        });
        _refreshChatMessages();
      }
    }
  }

  @override
  Widget build(BuildContext context) {


    if (user.isLoggedIn) {
      return _buildChatInterface();
    }
    else {
      return _buildLoginPrompt(context);
    }
  }

  @override
  Widget _buildChatInterface() {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Instant Message Support', style: TextStyle(fontSize: 18)),
        automaticallyImplyLeading: false,
      ),
      body: Column(
        children: [
          Expanded(
            child: ListView.builder(
              itemCount: _messages.length,
              itemBuilder: (context, index) {
                var message = _messages[index];
                bool isSender = message.isUserMessage();
                return Align(
                  alignment: isSender ? Alignment.centerRight : Alignment.centerLeft,
                  child: Container(
                    margin: const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                    padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 15),
                    decoration: BoxDecoration(
                      color: isSender ? Colors.grey[300] : Colors.blue[300],
                      borderRadius: BorderRadius.circular(15),
                    ),
                    child: Text(
                      message.displayText,
                      style: TextStyle(color: isSender ? Colors.black : Colors.white),
                    ),
                  ),
                );
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: [
                Expanded(
                  child: TextField(
                    controller: _controller,
                    decoration: InputDecoration(
                      hintText: 'Enter a message',
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(15),
                      ),
                    ),
                  ),
                ),
                IconButton(
                  onPressed: () => _sendMessage(),
                  icon: const Icon(Icons.send),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }



  // Builds the login prompt
  Widget _buildLoginPrompt(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const Text('Instant messaging requires login'),
          const SizedBox(height: 20),
          ElevatedButton(
            onPressed: () {
              Navigator.of(context).push(MaterialPageRoute(builder: (context) => const LoginPage()));
            },
            child: const Text('Go to Login'),
          ),
        ],
      ),
    );
  }

}

