
import os, random, string

class Config(object):

    basedir = os.path.abspath(os.path.dirname(__file__))

    ASSETS_ROOT = os.getenv('ASSETS_ROOT', '/static/assets')

    DB_ENGINE = 'mongodb'
    DB_NAME   = 'Dashboard' 

    MONGO_URI = os.getenv('MONGO_URI', f"mongodb://localhost:27017")

    KAFKA_HOST = str(os.getenv('KAFKA_HOST', 'localhost'))   ## No http:: confluent kafka does not use http protocol
    KAFKA_PORT = os.getenv('KAFKA_PORT', '9092')    ##
    KAFKA_URI = f"{KAFKA_HOST}:{KAFKA_PORT}"
    
    MAP_API = "http://" + str(os.getenv('MAP_API', 'localhost'))
    MAP_API_PORT = os.getenv('MAP_API_PORT', '8080')    ######## change back to 80 for deploy
    MAP_API = f"{MAP_API}:{MAP_API_PORT}"          ######## change back to 80 for deploy
    
    
    NODEJS_API = "http://" + str(os.getenv('NODEJS_API', 'localhost'))
    NODEJS_API_PORT = os.getenv('NODEJS_API_PORT', '3000')    ######## change back to 80 for deploy
    NODEJS_API = f"{NODEJS_API}:{NODEJS_API_PORT}"          ######## change back to 80 for deploy
    
    CLUSTERING_API = "http://" + str(os.getenv('CLUSTERING_API', 'localhost'))
    CLUSTERING_PORT = os.getenv('CLUSTERING_PORT', '3500')
    CLUSTERING_API = f"{CLUSTERING_API}:{CLUSTERING_PORT}"
    
    DEBUG = bool(os.environ.get('DEBUG', False))
    
    
    
    

class ProductionConfig(Config):
    DEBUG = False

    # Security
    SESSION_COOKIE_HTTPONLY = True
    REMEMBER_COOKIE_HTTPONLY = True
    REMEMBER_COOKIE_DURATION = 3600

class DebugConfig(Config):
    DEBUG = False


config_dict = {
    'Production': ProductionConfig,
    'Debug'     : DebugConfig,
}