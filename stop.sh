#!/bin/bash

# Attempt to kill the MongoDB container and handle error
docker kill mongodb-container || echo "Error: Failed to kill mongodb-container. It may not be running."

# Attempt to kill gunicorn and handle potential failure
sudo pkill -9 gunicorn || echo "Warning: Failed to kill gunicorn. It may not be running."

# Attempt to kill node and handle potential failure
sudo pkill -9 node || echo "Warning: Failed to kill node. It may not be running."

# Navigate to kafka directory and run its stop script
# if -k option is present, skip this step
if [[ "$1" == "-k" ]]; then
    exit 0
fi

cd kafka
./stop.sh || echo "Warning: Kafka stop script failed."
cd ..
