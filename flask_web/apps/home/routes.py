from crypt import methods
from math import log
from flask_login import login_required, current_user
from jinja2 import TemplateNotFound
from apps.config import Config
from flask import redirect, render_template, jsonify
from apps.authentication.forms import LoginForm, CreateAccountForm


def init_app(app):
    @app.route("/")
    def home():
        if current_user.is_authenticated:
            return render_template("dashboard/dashboard.html")
        else:
            return render_template("accounts/login.html", form=LoginForm())

    @app.route("/index", methods=["GET"])
    @login_required
    def index():
        return redirect("/")

    @app.route("/live-images")
    @login_required
    def live_images():
        return render_template("monitoring/live-images.html")

    @app.route("/lucene-index")
    @login_required
    def index_lucene():
        return render_template("lucene/lucene-index.html")

    @app.route("/lucene-search")
    @login_required
    def search_lucene():
        return render_template("lucene/results.html")

    @app.route("/SMS")
    @login_required
    def SMS():
        return render_template("messaging/SMS.html")

    @app.route("/messaging", methods=["GET", "POST"])
    @login_required
    def messaging():
        return render_template("messaging/DirectMessaging.html")

