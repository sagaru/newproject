const express = require('express');
const { MongoClient, ReadPreferenceMode, Code } = require('mongodb');
const axios = require('axios');
const multer = require('multer');
const path = require('path');
const fs = require('fs');
const { exec } = require('child_process');


let debug = process.env.DEBUG || true ; 
// if DEBUG is a string "0" convert it to an int
if (typeof debug === 'string') {
  debug = parseInt(debug) ;
}



const app = express();
app.use(express.json());

// MongoDB connection
const mongoUrl = process.env.MONGO_URI || 'mongodb://localhost:27017/';
const client = new MongoClient(mongoUrl);

const sttURI = 'http://'+ (process.env.STT_URI || 'localhost:5000');
const mapURI = 'http://'+ (process.env.MAP_URI || 'localhost:8080');
const clusteringURI = 'http://'+(process.env.CLUSTERING_URI || 'localhost:3500');


const twilio = require('twilio');

const accountSid = 'ACa7e7fa06a55df517657cf7946f8c730a';
const authToken = '70a41f4d05e2cdbc95cfd597f1bc3ed0';
const twilioClient = twilio(accountSid, authToken);


let waypoints = [] ; 


// Setup multer for file handling
const storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, process.cwd() + '/uploads');
  },
  filename: function(req, file, cb) {
    const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9);
    cb(null, file.fieldname + '-' + uniqueSuffix + path.extname(file.originalname));
  }
});


const upload = multer({ storage: storage });



async function initializeApp() {
  try {
    // Connect to MongoDB
    await client.connect();
    console.log("Connected to MongoDB");

      // Get all waypoints from the database
      client.db('MobileApp').collection('Waypoints').find({}).toArray().then(wps => {
        console.log(wps);
        waypoints = wps;
      }
      ).catch(err => console.error(err));
  

    app.listen(3000, () => console.log('Server listening on port 3000'));
  } catch (error) {
    console.error("Failed to initialize app:", error);
    process.exit(1);
  }
}


initializeApp();


app.post('/flutter-voice-message', upload.single('voice_message'), async (req, res) => {
  if (!req.file) {
    return res.status(400).send('No file uploaded');
  } else {
    res.status(200).send('File received and processed');
  }

  const filePath = req.file.path;
  console.log('Received file:', filePath);

  try {
    const compressedFilePath = filePath + '.flac';
    const flacCommand = `flac -6 ${filePath} -o ${compressedFilePath}`;

    await new Promise((resolve, reject) => {
      exec(flacCommand, (error, stdout, stderr) => {
        if (error) {
          console.error('Error compressing file:', error);
          reject(error);
        }
        console.log('stdout:', stdout);
        console.error('stderr:', stderr);
        resolve();
      });
    });

    let audioContent = await fs.promises.readFile(compressedFilePath);

    // Prepare the HTTP request options
    const sttResponse = await axios.post(sttURI + '/stt-transcribe', audioContent, {
      headers: {
        'Content-Type': 'audio/flac'
      }
    });

    console.log('STT response:', sttResponse.data);

    // Create a new report in the database
    const dbName = 'Dashboard';
    const db = client.db(dbName);
    const collection = db.collection('Reports');
    // req.body.location is in the form '[123.12, 123.34]', we want to onvert from string to array of floating points
    req.body.location = req.body.location.replace('[', '').replace(']', '').split(',').map(x => parseFloat(x));
    // location shoyuld have the form {lat: float, long: float}
    req.body.location = {lat: req.body.location[0], long: req.body.location[1]};
    let date = new Date() ; 
    // ISODate("2024-04-16T15:36:08.667Z") --> 2024-04-16T15:36:08.667Z
    date = date.toISOString().split('T')[0] + ' ' + date.toISOString().split('T')[1].split('.')[0] + 'Z' ;
    const report = {
      timestamp: date,
      description: sttResponse.data.transcription,
      location: req.body.location,
      phone: req.body.phone,   // the user's phone number
    };

    await collection.insertOne(report);

    // Cleanup: Delete the original, compressed files
    await fs.promises.unlink(compressedFilePath);
    await fs.promises.unlink(filePath);

    // Cleanup: Delete any files remaining in the uploads directory
    const directory = process.cwd() + '/uploads';
    fs.readdir(directory, (err, files) => {
      if (err) throw err;
      for (const file of files) {
        fs.promises.unlink(path.join(directory, file)).catch(err => {
          console.error('Error deleting file:', err);
        });
      }
    });

  } catch (error) {
    console.error('Error during processing:', error);
  }
});






app.post('/flutter-report', async (req, res) => {
  try {
    const dbName = 'Dashboard';
    const db = client.db(dbName);
    const collection = db.collection('Reports');
    const report = {
      timestamp: req.body.timestamp,
      description: req.body.description,
      location: req.body.location,
      phone: req.body.phone,   // the user's phone number
    };

    const id = await collection.insertOne(report);
    res.status(200).send('Data received and added to MongoDB');

    try {
      // axios make post to /cluster at clustering server. send id of the report in filed new_report_id
      // to convert string to json, use JSON.parse(req.body.location)
      const clusterResponse = await axios.post(clusteringURI + '/cluster', {
        new_report_id: id.insertedId,
      });
      console.log('Cluster response:', clusterResponse.status);
    }
    catch (error) {
      console.error('Error during clustering:', error);
    }

  } catch (error) {
    console.error(error);
    res.status(500).send(`Error processing request: ${error.message}`);
  }
});




app.post('/flutter-signup', async (req, res) => {

	// convert phone number to a standardised format (e.g 353 XX XXX XXXX)
	// confirm via twilio before storing the user's phone number

  try {
    const dbName = 'MobileApp';
    const db = client.db(dbName);
    const collection = db.collection('Users');
    const user = {
      phone: req.body.phone,
    };

    // if user need to sign up again, we may want to let them replace their old account when they download the app again
    const foundUser = await collection.findOne(user);
    if (foundUser !== null) {
      res.status(409).send('User already exists');
      return;
    }

    await collection.insertOne(user);
    res.status(200).send('User registered and added to MongoDB');
  } catch (error) {
    console.error(error);
    res.status(500).send(`Error processing request: ${error.message}`);
  }
});


app.post('/flutter-login', async (req, res) => {

  try {
    const dbName = 'MobileApp';
    const db = client.db(dbName);
    const collection = db.collection('Users');
    const user = {
      phone: req.body.phone,
    };

    const foundUser = await collection.findOne(user);

    if (foundUser === null) {
      res.status(404).send('User not found');
    }else {
      res.status(200).send('User found');
    }
  } catch (error) {
    console.error(error);
    res.status(500).send(`Error processing request: ${error.message}`);
  }
});





function randCode() {
  let v = (Math.floor(Math.random() * 1000000) % 1000000).toString() ; 
  while (v.length < 6) {
    v = '0' + v ;
  }
  return v ;
}


app.post('/flutter-twilio-send-sms', async (req, res) => {
  const code = randCode();
  console.log("received request to send sms to " + req.body.phone + " with code " + code);
  try {
    if (0) {
      console.log("Code: " + code) ; 
      res.status(200).send({ code: code });
      // return;
    }
    console.log("Code: " + code) ; 
    const message = await twilioClient.messages.create({
      body: "Please enter the following code to verify your account: " + code,
      from: '+16185963009',
      to: "+" + req.body.phone
    });
    console.log("Message sent: ", message) ; 
    res.status(200).send({ code: code.toString() });
    if (message) {
      console.log("Message sent: ", message.sid);
    }
    else {
      console.log("Message not sent");
    }
  } catch (error) {
    console.error(error);
    res.status(500).send(`Error processing request: ${error.message}`);
  }
});




app.post('/update-location', async (req, res) => {
    try {
      const dbName = 'MobileApp';
      const db = client.db(dbName);
      const collection = db.collection('Users');
      const user = {
        phone: req.body.phone,
      };

      let exists = 1 ;
      // check if user exists
      if (await collection.findOne(user) === null) {
        exists = 0 ; 
      }

      //'location': {'coordinates': [float, float], 'timestamp': datetime}}
      if (exists) {
        const ret = await collection.updateOne(user, {$set: {'location': req.body.location}});
        res.status(200).send('User location updated');
        // console.log(ret);
      }
      else {
        const ret = await collection.insertOne({'phone': req.body.phone, 'location': req.body.location});
        res.status(200).send('User location added');
        // console.log(ret);
      }
    } catch (error) {
      console.error(error);
      res.status(500).send(`Error processing request: ${error.message}`);
    }
  });



// app.get('/update-waypoints', async (req, res) => {
//   console.log("Updating waypoints") ; 
//   try {
//     const dbName = 'MobileApp';
//     const db = client.db(dbName);
//     const usersCollection = db.collection('Users');
//     const users = await usersCollection.find({}).toArray();
//     console.log("users: ", users) ; 

//     const waypointsCollection = db.collection('Waypoints');
//     const waypoints = await waypointsCollection.find({}).toArray();
//     console.log("waypoints: ", waypoints) ;

//     res.status(200).send("");

//     // for all users iun MobileApp.Users, calculate the distance to the nearest waypoint
//     // if user location is within the radius of a waypoint, update user db entry to have 'waypoint' field with coordinates and timestamp

//     for (let user of users) {
//       for (let waypoint of waypoints) {
//         let dist = Math.sqrt((user.location.coordinates[0] - waypoint.coordinates[0])**2 + (user.location.coordinates[1] - waypoint.coordinates[1])**2)
//         console.loh("distance: ", dist) ;
//         if (dist < waypoint.radius) {
//           await usersCollection.updateOne({
//             'phone': user.phone}, {$set: {'waypoint': waypoint}
//           });
//         }
//       }
//     }

//   } catch (error) {
//     console.error(error);
//     res.status(500).send(`Error processing request: ${error.message}`);
//   }
// });




app.post('/mobile-directions', async (req, res) => {

  const db = client.db('MobileApp');
  const usersCollection = db.collection('Users');
  const user = req.body.phone;
  const userDoc = await usersCollection.findOne({phone: user});
  let waypoint = req.body.waypoint; // waypoint has the format [lat, long]
  // round each value in waypint to 7 decimal places
  waypoint = waypoint.map(x => Math.round(x * 10000000) / 10000000);
  console.log("waypoint: ", waypoint) ;

  // if waypoint == [0.0, 0.0] return 404
  if (waypoint[0] == 0.0 && waypoint[1] == 0.0) {
    res.status(404).send('No waypoint sent by user');
    return;
  }

  if (waypoint.length != 0) {
    // check if waypoint exists in the database
    // if not, remove waypoint from MobileApp.Users entry
    // also, if another (existing) waypoint is within the radius of the user, return the path to that waypoint

    const waypointsCollection = db.collection('Waypoints');
    const foundWaypoint = await waypointsCollection.findOne({'waypoint.coordinates': waypoint});
    //console.log("waypoints: ", foundWaypoint) ;

    // if found waypoint is not in the database, remove it from the user entry
    if (foundWaypoint == null) {
      // remove waypoint from user entry
      await usersCollection.updateOne({phone: user}, {$unset: {waypoint: ""}});
    }
    // check if waypoint sent by user matches the one in the database. if it is return 200 OK
    else {
      res.status(201).send('Waypoint still valid');
      return;
    }


    // if another waypoint is within the radius of the user, return the path to that waypoint
    let min_dist = 1000000000;
    let min_waypoint = null ;
    const waypoints = await waypointsCollection.find({}).toArray();
    if (waypoints == null) {
      res.status(404).send('No waypoints found');
      return;
    }
    for (let waypoint of waypoints) {
      waypoint = waypoint.waypoint ;
      let dist = Math.sqrt((userDoc.location.coordinates[0] - waypoint.coordinates[0])**2 + (userDoc.location.coordinates[1] - waypoint.coordinates[1])**2)
      console.log("distance: ", dist) ;
      if (dist < waypoint.radius) {
        if (dist < min_dist) {
          min_dist = dist ;
          min_waypoint = waypoint ;
        }
      }
    }
    if (min_waypoint != null) {
      const path = await axios.post(mapURI+'/start-end-path', {
        start: userDoc.location.coordinates,
        end: min_waypoint.coordinates
      });
      path.data.path.push(min_waypoint.coordinates) ;  
      res.status(200).send(path.data);
        // update user in db to have waypoint
        await usersCollection.updateOne({phone: user}, {$set: {waypoint: min_waypoint}});
        return ;
      }
      else {
        res.status(404).send('No waypoints found');
        return;
      }
    }
    
    else {
      console.log("No waypoint sent by user") ;
    }


    
  // if user has a waypoint, make a call to map-api and return the path to the end point
  console.log("User: ", user)
  console.log("User doc: ", userDoc) ;
  if (userDoc == null) {
    res.status(404).send('User not found');
    return;
  }

  //console.log("waypoint: ", userDoc.waypoint) ; 
  if (userDoc.location && userDoc.waypoint) {
    const path = await axios.post(mapURI+'/start-end-path', {
      start: userDoc.location.coordinates,
      end: userDoc.waypoint.coordinates
    });
    res.status(200).send(path.data);
    console.log(path.data);
  } else {
    res.status(404).send('No waypoint found');
  }

}); 
  


app.get('/ping', (req, res) => {
  console.log('Ping received');
  res.status(200).send('Pong!');
});



