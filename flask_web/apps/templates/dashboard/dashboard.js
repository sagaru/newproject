  
  
  let map;

  function initMap() {
      map = L.map('map').setView([53.35, -6.26], 12);

      L.tileLayer(getTileLayerUrl(), {
        attribution: 'Map tiles by CARTO, under CC BY 3.0. Data by OpenStreetMap, under ODbL.',
        subdomains: 'abcd',
        maxZoom: 25
      }).addTo(map); 

  }

  initMap();


  async function updateMapTheme() {
    if (map) {
        map.eachLayer(function(layer) {
            map.removeLayer(layer);
        });
        L.tileLayer(getTileLayerUrl(), {
            attribution: 'Map tiles by CARTO, under CC BY 3.0. Data by OpenStreetMap, under ODbL.',
            subdomains: 'abcd',
            maxZoom: 16
        }).addTo(map);
    }

    loadReportsAndMarkers();
    loadPolylines();
    updateDivider();
    await getWaypoints();
    mapMarkersWaypoints();
    removeLayers();
    if (groupShowing) {
        map.addLayer(group);
    }
    if (clusterShowing) {
        map.addLayer(clusterGroup);
    }

    try {
      showIcons(keepLast=true);
  } catch (error) {
      ;
  }


    console.log("curent theme: " + currentTheme);
  }

  function loadReportsAndMarkers() {
    if (clusterShowing) {
      return ; 
    }
    fetch('/get-report')
        .then(response => response.json())
        .then(reports => {
            console.log('Reports:', reports);
            const tableBody = document.querySelector('#reports-table tbody');
            if (tableBody) {
              tableBody.innerHTML = '';
            }
            else {
              return;
            }

            reports.forEach(report => {
              if (tableBody) {
                const tr = document.createElement('tr');
                tr.setAttribute('data-report-id', report._id); 
                tr.classList.add('report-row'); 
                tr.innerHTML = `
                    <td><span>${report.description}</span></td>
                    <td><span>${report.location.lat}, ${report.location.lng}</span></td>
                    <td><span>${report.timestamp}</span></td>
                    <td><span>${report.phone}</span></td>
                `;
                tableBody.appendChild(tr);
                document.querySelectorAll('.report-row.active').forEach(r => r.classList.remove('active'));


                const deleteIcon = document.createElement('td');
                deleteIcon.style.cursor = 'pointer';
                deleteIcon.innerHTML = '<span class="delete-icon"><strong>X</strong></span>';
                deleteIcon.addEventListener('click', function(event) {
                    event.stopPropagation(); // Stop the event from bubbling up
                    deleteReport(report._id, tr);
                });
                tr.appendChild(deleteIcon);
              }

              // if there is already a marker being shwon at the exact same location, do not add another marker
              let markerExists = false;
              map.eachLayer(function(layer) {
                if (layer instanceof L.Marker) {
                  if (layer.getLatLng().lat == report.location.lat && layer.getLatLng().lng == report.location.lng) {
                    markerExists = true;
                  }
                }
              });
                
                if (markerExists == false) {
                  L.marker([report.location.lat, report.location.lng])
                  .addTo(map)
                  .bindPopup(`${report.description}`);
                }

            });

            addReportClickEventListeners();
        })
        .catch(error => console.error('Error loading reports:', error));

        // fetchBlockedRoutes();
  }


  function fetchReportDetails(reportId) {
  fetch(`/report/${reportId}`)
    .then(response => response.json())
    .then(details => {
        document.getElementById('tab1').innerHTML = details.html;
    })
    .catch(error => console.error('Error fetching report details:', error));
  }

  let highlightedMarker = null;
  let selectedReportId = null;

  function highlightReportOnMap(reportId) {
      // Clear the previously highlighted marker
      if (highlightedMarker) {
          map.removeLayer(highlightedMarker);
          highlightedMarker = null;
      }

      document.querySelectorAll('.report-row.active').forEach(activeRow => {
        activeRow.classList.remove('active');
      });


      const report = document.querySelector(`[data-report-id="${reportId}"]`);
      report.classList.add('active');

      // Parse the latitude and longitude
      const lat = parseFloat(report.children[1].innerText.split(',')[0].trim());
      const lng = parseFloat(report.children[1].innerText.split(',')[1].trim());

      // Create the red marker and add it to the map
      var redIcon = new L.Icon({
          iconUrl: 'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-red.png',
          shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
          iconSize: [25, 41],
          iconAnchor: [12, 41],
          popupAnchor: [1, -34]
      });

      highlightedMarker = L.marker([lat, lng], {icon: redIcon})
          .addTo(map)
          .bindPopup(report.children[0].innerText);

      // Open the popup for the highlighted marker
      highlightedMarker.openPopup();
  }

  function addReportClickEventListeners() {
  document.querySelectorAll('.report-row').forEach(row => {
    row.addEventListener('click', function() {
        // Remove 'active' class from all rows
        document.querySelectorAll('.report-row').forEach(r => r.classList.remove('active'));
        // Add 'active' class to the clicked row
        this.classList.add('active');
        
        const reportId = this.getAttribute('data-report-id');
        selectedReportId = reportId;
        fetchReportDetails(reportId);
        // highlightReportOnMap(reportId);
        
    });
  });
  }


  function fetchBlockedRoutes() {
    fetch('/get-blocked-routes')
    .then(response => response.json())
    .then(routes => {
        console.log('Routes:', routes);
        const tableBody = document.querySelector('#blocked-routes-table tbody');
        if (tableBody) tableBody.innerHTML = '';

        routes.forEach(route => {
          if (tableBody) {
            const tr = document.createElement('tr');
            tr.setAttribute('blocked-route-id', route._id); 
            tr.classList.add('report-row'); 
            // make get-blcoked-routes in python backend and return the blocked routes
            // add click fucntionality to select the route on the map and set map view to the route //////////////////////////////////////////////////////////////
            tr.innerHTML = `
                <td><span>${route.road_name}</span></td>
                <td><span>${route.start.lat}, ${route.start.lng}</span></td>
                <td><span>${route.end.lat}, ${route.end.lng}</span></td>
            `;
            // tableBody.appendChild(tr);
            document.querySelectorAll('.report-row.active').forEach(r => r.classList.remove('active'));


            const deleteIcon = document.createElement('td');
            deleteIcon.style.cursor = 'pointer';
            deleteIcon.innerHTML = '<td><button id="delete-blocked-route"  class="btn btn-danger" >DELETE</button></td>';
            deleteIcon.addEventListener('click', function(event) {
                event.stopPropagation(); // Stop the event from bubbling up
                deleteBlockedRoute(route._id);
            });
            tr.appendChild(deleteIcon);
            tableBody.appendChild(tr);
          }

          // addPolylineBlock(path,id, options)
          const options = {
            color: 'red',
            weight: 5,
            opacity: 0.5,
            smoothFactor: 1
          }
          addPolylineBlock(route.path, route._id, options);
        }
        );
      })
  }


  function fetchActions() {
    fetch('/get-actions')
        .then(response => response.json())
        .then(data => {
            document.getElementById('tab2').innerHTML = data.html;
            initializeActionsTab(); 
        })
        .catch(error => console.error('Error fetching actions:', error));
  }


  $(document).ready(function() {
    $('.selectpicker').selectpicker();
  });



  function deleteBlockedRoute(id) {
    if (confirm("Are you sure you want to unblock this route?")) {
      removePolyline(id);
      // fetch post to '/unblock-route' with 'path' = path
      fetch('/unblock-route', {
          method: 'POST',
          headers: {
              'Content-Type': 'application/json',
          },
          body: JSON.stringify({ id: id }),
      })

      // reload blocked routes
      fetchBlockedRoutes();
    }
  }



