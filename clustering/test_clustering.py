import unittest
from unittest.mock import patch, MagicMock
from pymongo import MongoClient
from bson import ObjectId
from clustering import cluster_reports, cleanup_clusters, llm_similarity, update_cluster_description

class TestClustering(unittest.TestCase):
    @patch('clustering.MongoClient')
    def test_cluster_reports_success(self, mock_mongo_client):
        # Mock the MongoDB client and collections
        mock_db = MagicMock()
        mock_mongo_client.return_value.__getitem__.return_value = mock_db
        mock_reports_collection = MagicMock()
        mock_clusters_collection = MagicMock()
        mock_db.__getitem__.side_effect = [mock_reports_collection, mock_clusters_collection]

        # Mock the data returned by the collections
        mock_reports_collection.find_one.return_value = {
            '_id': ObjectId('60a5c5d5f1d1b6b1e8e8e8e8'),
            'description': 'Sample report',
            'location': {'lat': 40.7128, 'lng': -74.0060},
            'timestamp': '2023-05-20 10:00:00.000Z',
            'phone': '1234567890'
        }
        mock_clusters_collection.find.return_value = []
        mock_clusters_collection.count_documents.return_value = 0

        # Call the function being tested
        new_report_id = '60a5c5d5f1d1b6b1e8e8e8e8'
        result = cluster_reports(new_report_id)

        # Assert the expected behavior
        self.assertEqual(result, [0, 'Cluster_1'])
        mock_reports_collection.update_one.assert_called_once()
        mock_clusters_collection.insert_one.assert_called_once()


    @patch('clustering.MongoClient')
    def test_cleanup_clusters_success(self, mock_mongo_client):
        # Mock the MongoDB client and collections
        mock_db = MagicMock()
        mock_mongo_client.return_value.__getitem__.return_value = mock_db
        mock_reports_collection = MagicMock()
        mock_clusters_collection = MagicMock()
        mock_db.__getitem__.side_effect = [mock_reports_collection, mock_clusters_collection]

        # Mock the data returned by the collections
        mock_clusters_collection.find.return_value = [
            {'name': 'Cluster_1', 'members': [ObjectId('60a5c5d5f1d1b6b1e8e8e8e8')]},
            {'name': 'Cluster_2', 'members': [ObjectId('60a5c5d5f1d1b6b1e8e8e8e9')]}
        ]
        mock_reports_collection.find_one.side_effect = [
            {'_id': ObjectId('60a5c5d5f1d1b6b1e8e8e8e8')},
            None
        ]

        # Call the function being tested
        result = cleanup_clusters()

        # Assert the expected behavior
        self.assertEqual(result, [0, 'Cleanup completed successfully'])
        self.assertEqual(mock_clusters_collection.update_one.call_count, 2)
        mock_clusters_collection.delete_one.assert_called_once()
        
        
    def test_llm_similarity(self):
        statement1 = "The quick brown fox jumps over the lazy dog"
        statement2 = "The lazy dog was jumped over by the quick brown fox"
        similarity = llm_similarity(statement1, statement2)
        print(similarity)
        similarity = float(similarity)
        self.assertTrue(similarity > 0.5)
        
        
    def test_update_cluster_description_success(self):
        description1 = "The quick brown fox jumps over the lazy dog"
        description2 = "The lazy dog was jumped over by the quick brown fox"
        response = update_cluster_description(description1, description2)
        # self.assertEqual(response, "The quick brown fox jumps over the lazy dog was jumped over by the quick brown fox")
        print(response)
        self.assertIsNotNone(response)
        
    
        

if __name__ == '__main__':
    unittest.main()