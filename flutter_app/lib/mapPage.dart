import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong2/latlong.dart';
import 'package:location/location.dart';
import 'package:flutter_app/user.dart' ;
import 'package:sensors_plus/sensors_plus.dart';
import 'dart:async';
import 'dart:math' as math;

final GlobalKey<_MapPageState> mapPageKey = GlobalKey<_MapPageState>();

class MapPage extends StatefulWidget {
  const MapPage({super.key});

  @override
  _MapPageState createState() => _MapPageState();
}

class _MapPageState extends State<MapPage> {
  final Location _location = Location();
  late LatLng _center = user.getLocation;
  List<Marker> _markers = [];
  List<Polyline> _polylines = []; // Added to store polylines
  final MapController _mapController = MapController();
  double _direction = 0.0;
  Timer? _locationUpdateTimer;


  @override
  void initState() {
    super.initState();

    magnetometerEventStream().listen((MagnetometerEvent event) {
      // Calculate the azimuth (in radians) and update direction
      double azimuth = math.atan2(event.y, event.x);
      double azimuthDegrees = azimuth * (180 / math.pi);
      if (azimuthDegrees < 0) {
        azimuthDegrees += 360;
      }

      // Update the state only if there's a significant change to reduce redraws
      if ((azimuthDegrees - _direction).abs() > 5) { // Example threshold, adjust as needed
        setState(() {
          _direction = azimuthDegrees;
          _updateDirectionMarker(); // Update marker with new direction
        });
      }
    });


    _locationUpdateTimer = Timer.periodic(const Duration(seconds: 10), (Timer t) => _updateLocation());

    _updateLocation();
    _updatePolylines(); // Call this to initialize polylines
  }


  @override
  void dispose() {
    // Cancel the timer when the widget is disposed
    _locationUpdateTimer?.cancel();
    super.dispose();
  }


  Future<void> _updateLocation() async {
    try {
      LatLng newLocation = await _getLocation();
      if (!mounted) return;
      setState(() {
        _center = newLocation;
        _updatePolylines(); // Update polylines when location updates
      });
      _mapController.move(newLocation, _mapController.zoom);
      user.setLocation = newLocation;
    } catch (e) {
      throw Exception("Error getting location: $e");
    }

    try {
      _updateDirectionMarker();
    }
    catch (e) {
      throw Exception("Error updating direction marker: $e");
    }
  }


  // Function to update polylines based on user.Waypoints
  void _updatePolylines() {
    setState(() {
      _polylines.clear(); // Clear existing polylines
      if (user.getWaypoints.isNotEmpty) {
        _polylines.add(
          Polyline(
            points: user.getWaypoints,
            strokeWidth: 4.0,
            color: Colors.blue,
          ),
        );
      }
    });
  }

  void updatePolylines() {
    _updatePolylines();
  }



  Future<LatLng> _getLocation() async {
    bool _serviceEnabled;
    PermissionStatus _permissionGranted;

    _serviceEnabled = await _location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await _location.requestService();
      if (!_serviceEnabled) {
        throw Exception("Location services are disabled.");
      }
    }

    _permissionGranted = await _location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await _location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        throw Exception("Location permission denied.");
      }
    }

    final LocationData _locationData = await _location.getLocation();
    return LatLng(_locationData.latitude ?? 0.0, _locationData.longitude ?? 0.0);
  }



  void _updateDirectionMarker() {
    LatLng currentLocation = _center; // Assuming _center holds your current location
    setState(() {
      _markers = [
        Marker(
          width: 80.0,
          height: 80.0,
          point: currentLocation,
          builder: (ctx) => Container(
            alignment: Alignment.center,
            child: Transform.rotate(
              angle: _direction * (math.pi / 180), // Convert degrees to radians
              child: const Icon(Icons.navigation, color: Colors.red, size: 32),
            ),
          ),
        ),
      ];
    });
  }





  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FlutterMap(
        mapController: _mapController,
        options: MapOptions(
          center: _center,
          zoom: 13.0,
          onTap: (tapPosition, latLng) {
            setState(() {
              _updatePolylines();            // Redraw the polyline
            });
          },
        ),
        children: [
          TileLayer(
            urlTemplate: 'https://{s}.basemaps.cartocdn.com/rastertiles/voyager/{z}/{x}/{y}{r}.png',
            subdomains: const ['a', 'b', 'c'],
          ),
          MarkerLayer(markers: _markers),
          PolylineLayer(polylines: _polylines), // Display polylines on the map
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _updateLocation,
        tooltip: 'Recenter',
        child: const Icon(Icons.my_location),
      ),
    );
  }
}



