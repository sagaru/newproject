import 'dart:io';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:permission_handler/permission_handler.dart';
import 'package:path_provider/path_provider.dart';
import 'package:flutter_sound/flutter_sound.dart';
import 'package:flutter_app/user.dart';
import 'package:flutter_app/main.dart';
import 'package:http_parser/http_parser.dart';



class VoiceMessagePage extends StatefulWidget {
  @override
  _VoiceMessagePageState createState() => _VoiceMessagePageState();
}

class _VoiceMessagePageState extends State<VoiceMessagePage> {
  final FlutterSoundRecorder _soundRecorder = FlutterSoundRecorder();
  bool _isRecording = false;
  String _tempPath = '';

  @override
  void initState() {
    super.initState();
    _initRecorder();
  }

  @override
  void dispose() {
    _soundRecorder.closeRecorder();
    super.dispose();
  }

  Future<void> _initRecorder() async {
    Directory appDocDir = await getApplicationDocumentsDirectory();
    String appDocPath = appDocDir.path;

    final status = await Permission.microphone.request();
    if (status != PermissionStatus.granted) {
      throw RecordingPermissionException('Microphone permission not granted');
    }
    await _soundRecorder.openRecorder();
    _tempPath = '$appDocPath/temp_voice_message.wav';
  }

  Future<void> _startRecording() async {
    await _soundRecorder.startRecorder(toFile: _tempPath, codec: Codec.pcm16WAV);
    print('Temporary voice message path: $_tempPath') ;

    setState(() {
      _isRecording = true;
    });
  }

  Future<void> _stopRecording() async {
    await _soundRecorder.stopRecorder();
    setState(() {
      _isRecording = false;
    });
    _sendVoiceMessage();
  }

  Future<void> _sendVoiceMessage() async {
    try {
      // var request = http.MultipartRequest('POST', Uri.parse('$serverAddr:$expressJsPort/flutter-voice-message'));
      var request = http.MultipartRequest('POST', Uri.parse('$serverAddr/flutter-voice-message'));
      request.files.add(await http.MultipartFile.fromPath('voice_message', _tempPath, contentType: MediaType('audio', 'wav')));
      request.fields['phone'] = user.getPhone;
      request.fields['location'] = user.toMap()['location'].toString();
      request.fields['timestamp'] = DateTime.now().toUtc().toString();

      var response = await request.send();

      if (response.statusCode == 200) {
        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text('Voice message sent successfully')));
        // Delete temporary file after successful upload
        File(_tempPath).delete();
      } else {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text('Error sending voice message: ${response.statusCode} - ${response.reasonPhrase}')));
      }
    } catch (e) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text('Error sending voice message: $e')));
    }
  }


// FOR TESTING PURPOSES ONLY /////////////////
  Future<void> sendWavFile(String filePath) async {
    try {
      // var uri = Uri.parse('$serverAddr:$expressJsPort/flutter-voice-message');
      var uri = Uri.parse('$serverAddr/flutter-voice-message');
      var request = http.MultipartRequest('POST', uri)
        ..files.add(await http.MultipartFile.fromPath(
          'voice_message',
          filePath,
          contentType: MediaType('audio', 'wav'),
        ));
        request.fields['phone'] = user.getPhone;
        request.fields['location'] = user.toMap()['location'].toString();
        request.fields['timestamp'] = DateTime.now().toUtc().toString();


      var response = await request.send();

      if (response.statusCode == 200) {
        print('File sent successfully');
        // Optionally, delete the file after sending
        File(filePath).delete();
      } else {
        print('Failed to send file: ${response.statusCode} - ${response.reasonPhrase}');
      }
    } catch (e) {
      print('Error sending file: $e');
    }
  }
//////////////////////////////////////////////





  void _onPressStart(LongPressStartDetails details) {
    _startRecording();

    // sendWavFile("/sdcard/Download/gettysburg.wav");
  }

  void _onPressEnd(LongPressEndDetails details) {
    _stopRecording();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Send Voice Message')),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            GestureDetector(
              onLongPressStart: _onPressStart,
              onLongPressEnd: _onPressEnd,
              child: Icon(
                _isRecording ? Icons.stop : Icons.mic,
                size: 64,
              ),
            ),
            Text(_isRecording ? 'Recording...' : 'Tap and hold to record'),
          ],
        ),
      ),
    );
  }
}
