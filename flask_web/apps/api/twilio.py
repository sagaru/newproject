from twilio.rest import Client
from flask import jsonify, request
import requests
from pymongo import MongoClient
from apps.config import Config
from bson.binary import Binary
from flask_login import login_required

def init_app(app):
    @login_required
    @app.route('/get-SMS', methods=['POST'])
    def get_SMS():
        if 0:
            messages = []
            messages.append({'value': "Hello, this is a message for testing and development purposes", 'direction': 'inbound'})
            messages.append({'value': 'test 2', 'direction': 'inbound'})
            messages.append({'value': 'test response', 'direction': 'outbound'})
            
            return jsonify(messages), 200
            
        else:
            app.logger.info('Getting SMS')
            account_sid = 'ACa7e7fa06a55df517657cf7946f8c730a'  # 
            auth_token = '70a41f4d05e2cdbc95cfd597f1bc3ed0'    #

            client = Client(account_sid, auth_token)

            response = client.messages.list(limit=50)
            messages = []
            for i in range(len(response)):
                if "Sent from your Twilio trial account" in response[i].body:
                    continue
                messages.append({'value': response[i].body, 'direction': 'inbound', 'time': response[i].date_sent})
                    
            app.logger.info(messages)
            return jsonify(messages), 200

        messages_sids = []
        for record in messages:
            if "Sent from your Twilio trial account" in record.body:
                continue
            messages_sids.append(record.sid)
            
        messages_json = []
        for message_sid in messages_sids:
            message = client.messages(message_sid).fetch()
            if message.direction == 'inbound':
                messages_json.append(message.body)
                
            if message.num_media != '0':
                # Fetch the media
                app.logger.info('Found media')
                media = client.messages(message_sid).media.list()
                for m in media:
                    app.logger.info(m.uri)
                    media_url = m.uri #'https://api.twilio.com/' + m.uri
                    app.logger.info(media_url)
                    response = requests.get(media_url, auth=(account_sid, auth_token))
                    app.logger.info(response.headers['Content-Type'])
                    # if image
                    if 'image' in response.headers['Content-Type']:
                        store_image(response.content)
            
        return jsonify(messages_json), 200
    

    @app.route('/send-SMS', methods=['POST'])
    def send_SMS():
        msg = request.get_json()['msg']
        recipient = request.get_json()['recipient']
        account_sid = 'ACa7e7fa06a55df517657cf7946f8c730a' 
        auth_token = '70a41f4d05e2cdbc95cfd597f1bc3ed0' 
        client = Client(account_sid, auth_token)
        # from my number : +1 618 596 3009
        if '+' not in recipient:
            recipient = '+' + recipient
        res = client.messages.create(
            body=msg,
            to=recipient,
            from_='+16185963009'
        )
        return jsonify(res.sid), 200     


def store_image(image):
    client = MongoClient(Config.MONGO_URI)
    db = client['Dashboard']
    collection = db['ReportImages']

    # convert image from json to binary
    image_binary = Binary(image)
    
    

    # Store in MongoDB
    collection.insert_one({'image': image_binary})