import unittest
from unittest.mock import MagicMock
import networkx as nx
from astar import *
from unittest.mock import patch
import os
import random

class TestNearestNode(unittest.TestCase):

    def setUp(self):
        # Create a sample graph
        self.graph = nx.Graph()
        self.graph.add_edge((0, 0), (1, 1), available=True)
        self.graph.add_edge((1, 1), (2, 2), available=True)
        self.graph.add_edge((2, 2), (3, 3), available=False)
    
    @patch('astar.haversine')
    def test_nearest_node(self, mock_haversine):
        mock_haversine.side_effect = lambda coord, node: abs(coord[0]-node[0]) + abs(coord[1]-node[1])
        
        # Test coordinates
        test_coords = []
        expected_nearest_nodes = []
        
        # Test coordinates
        test_coords.append((0.4, 0.4))
        test_coords.append((1.4, 1.4))
        test_coords.append((2.4, 2.4))
        
        expected_nearest_nodes.append((0, 0))
        expected_nearest_nodes.append((1, 1))
        expected_nearest_nodes.append((2, 2))
        
        
        for i in range(len(test_coords)):
            nearest = nearest_node(self.graph, test_coords[i])
            self.assertEqual(nearest, expected_nearest_nodes[i], "Nearest node is not as expected.")
            
    
    def test_graph_with_data(self):
        G = nx.Graph()
        current_directory = os.path.dirname(os.path.abspath(__file__))

        if not os.path.exists(current_directory+'/routes'):
            os.system("cd " + current_directory)
            os.system("tar -xjf routes.tar.bz2 -C .")

        G = create_graph(current_directory+'/routes', G, app=None)
        
        # make 1000 random coords for testing
        # the result should not be None, as there should always be a nearest node, regardless of where the coordinates are
        for i in range(10):
            lat = random.uniform(-90, 90)
            lon = random.uniform(-180, 180)
            nearest = nearest_node(G, (lat, lon))
            print(nearest)
            self.assertIsNotNone(nearest, "Nearest node is None.")
        

if __name__ == '__main__':
    unittest.main()
