import unittest
import networkx as nx
from haversine import haversine
from astar import create_distance_matrix
import numpy as np 

class TestCreateDistanceMatrix(unittest.TestCase):

    def test_empty_graph(self):
        print("\nRunning test for empty graph...")
        G = nx.Graph()
        distance_matrix, nodes = create_distance_matrix(G)
        print("Result - Distance Matrix:", distance_matrix)
        print("Result - Nodes:", nodes)
        self.assertEqual(len(nodes), 0, "For an empty graph, the list of nodes should be empty.")
        self.assertEqual(distance_matrix.shape, (0, 0), "For an empty graph, the distance matrix should be a 0x0 matrix.")

    def test_single_node_graph(self):
        print("\nRunning test for single node graph...")
        G = nx.Graph()
        G.add_node((0, 0))
        distance_matrix, nodes = create_distance_matrix(G)
        print("Result - Distance Matrix:", distance_matrix)
        print("Result - Nodes:", nodes)
        self.assertEqual(len(nodes), 1, "For a graph with a single node, the list of nodes should contain one element.")
        self.assertEqual(distance_matrix.shape, (1, 1), "For a graph with a single node, the distance matrix should be a 1x1 matrix.")
        self.assertEqual(distance_matrix[0][0], 0, "The distance from a node to itself should be 0.")

    def test_multiple_node_graph(self):
        print("\nRunning test for multiple node graph...")
        G = nx.Graph()
        node1 = (0, 0)   
        node2 = (1, 1)   
        G.add_node(node1)
        G.add_node(node2)
        distance_matrix, nodes = create_distance_matrix(G)
        print("Result - Distance Matrix:", distance_matrix)
        print("Result - Nodes:", nodes)
        expected_distance = haversine(node1, node2)
        self.assertEqual(len(nodes), 2, "For a graph with two nodes, the list of nodes should contain two elements.")
        self.assertEqual(distance_matrix.shape, (2, 2), "For a graph with two nodes, the distance matrix should be a 2x2 matrix.")
        self.assertAlmostEqual(distance_matrix[0][1], expected_distance, places=5, msg="The calculated distance between two distinct nodes should closely match the expected haversine distance.")
        self.assertAlmostEqual(distance_matrix[1][0], expected_distance, places=5, msg="The distance matrix should be symmetric.")
    
    # def test_nodes_far_apart(self):
    #     print("\nStarting test: Nodes Far Apart")
        
    #     # Define coordinates for New York and London
    #     new_york_coords = (40.7128, -74.0060)  # Latitude, Longitude
    #     london_coords = (51.5074, -0.1278)
    #     print(f"Coordinates: New York {new_york_coords}, London {london_coords}")
        
    #     # Known approximate distance between New York and London in kilometers
    #     known_distance_km = 5567
    #     print(f"Known approximate distance between New York and London: {known_distance_km} km")
        
    #     # Create a graph and add the two nodes
    #     G = nx.Graph()
    #     G.add_node(new_york_coords)
    #     G.add_node(london_coords)
    #     print("Graph created and nodes added.")

    #     # Generate the distance matrix
    #     distance_matrix, nodes = create_distance_matrix(G)
    #     print("Distance matrix generated.")

    #     # Extract the calculated distance from the matrix
    #     calculated_distance = distance_matrix[0][1]  # Distance from New York to London
    #     print(f"Calculated distance between New York and London: {calculated_distance:.2f} km")
        
    #     # Assert that the calculated distance is within a reasonable margin of the known distance
    #     self.assertAlmostEqual(calculated_distance, known_distance_km, delta=100, 
    #                            msg=f"The calculated distance between New York and London should be approximately {known_distance_km} km ±100 km tolerance")
    #     print("The calculated distance is within the expected range.")
    
   

    def test_graph_with_invalid_node_data(self):
        print("\nStarting test: Graph with Invalid Node Data")
    
    # Creating a graph with some invalid node data
        G = nx.Graph()
        G.add_node((89.0, 90.0))  # Invalid latitude and longitude
        G.add_node(('invalid', 'data'))  # Non-numeric data
        G.add_node((0, 0))  # A valid node for comparison
    
        try:
            distance_matrix, nodes = create_distance_matrix(G)
            print("Distance matrix generated.")

        # If the function does not raise an exception, perform basic integrity checks
            self.assertEqual(len(nodes), 3, "Number of nodes in the graph should include invalid nodes.")
            self.assertEqual(distance_matrix.shape, (3, 3), "Distance matrix dimensions should match the number of nodes, even with invalid data.")

        # Additional checks can include verifying that the distance_matrix contains specific values (e.g., zeros) for invalid data points
        # This depends on how you expect the function to handle invalid data
        except Exception as e:
            print("An exception was raised during distance matrix generation with invalid node data.")
            self.fail(f"Function should handle invalid data gracefully. Exception: {e}")

    

    



    

if __name__ == '__main__':
    unittest.main()
