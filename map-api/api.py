from flask import Flask, request, jsonify
import networkx as nx
from astar import nearest_node, reverse_lat_lon, a_star_search, create_graph, apply_constraints, restore_constraints, find_road_endpoints, manhattan_distance, haversine_heuristic
import os
from amenities import get_amenity_data, amenities_list
from haversine import haversine
from pymongo import MongoClient
from bson import ObjectId
import logging
import sys


MONGO_URI = os.environ.get('MONGO_URI', 'mongodb://localhost:27017/')
client = MongoClient(MONGO_URI)

DEBUG = os.environ.get('DEBUG', True)

app = Flask(__name__)


# logging setup
logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(levelname)s - %(message)s')
for handler in app.logger.handlers[:]:
    app.logger.removeHandler(handler)
    
app.logger.setLevel(logging.DEBUG)



G = nx.Graph()

if not os.path.exists('./routes'):
    os.system("tar -xjf routes.tar.bz2 -C .")

while not os.path.exists('./routes'):
    pass


if not os.path.exists('./amenities'):
    os.system("tar -xjf amenities.tar.bz2 -C .")
    
while not os.path.exists('./amenities'):
    pass

G= create_graph('./routes', G, app=app)

@app.route('/find_path', methods=['POST'])
def find_path():
    app.logger.info('find_path request: {}'.format(request.get_json()))
    global G
    end_lat = request.get_json()['end_lat']
    end_lon = request.get_json()['end_lon']
    end = nearest_node(G, (end_lon, end_lat))
    
    start_lon = request.get_json()['start_lat']
    start_lat = request.get_json()['start_lon']
    start = nearest_node(G, (start_lon, start_lat))
    
    g = G.edge_subgraph((u, v) for u, v, d in G.edges(data=True) if d['available'])
    path = None
    while 1:
        path = a_star_search(g, start, end)
        if not path:
            app.logger.warning('Path not found')
            return jsonify({'error': 'Path not found'}), 404
        else:
            break
        
    
    # reverse order of coordinates
    for i in range(len(path)):
        path[i] = (path[i][1], path[i][0])

    return jsonify({'path': path})



@app.route('/add_constraint', methods=['GET'])
def add_constraint():

    _ = request.get_json() 
    
    constraints = client.Dashboard.BlockedRoutes.find()  

    for constraint in constraints:
        global G
        G = apply_constraints(G, constraint['path'])   

    return jsonify({'success': True})




@app.route('/remove_constraint', methods=['POST'])
def remove_constraint():
    constraint_id = request.get_json()['id']
    global G
    constraints = client.Dashboard.BlockedRoutes.find({'_id': ObjectId(constraint_id)})
    for constraint in constraints:
        app.logger.info(f'constraint: {constraint}')
        restore_constraints(G, constraint['path'])

    return jsonify({'success': True}), 200



@app.route('/find-road', methods=['POST'])   # find road endpoints and path given road name
def get_road():
    app.logger.info('find_road request: {}'.format(request.get_json()))
    global G
    road_name = request.get_json()['road_name'].lower()
    path, dist = find_road_endpoints(G, road_name, app=app)
    app.logger.info('path: {}'.format(type(path)))
    if dist == 0:
        return jsonify({'error': 'Road not found'}), 404
    path = reverse_lat_lon(a_star_search(G, path[0], path[1]))

    for i in range(len(path)):
        path[i] = {'lat': path[i][0], 'lng': path[i][1]}

    return jsonify({'path': path, 'dist': dist, 'start': path[0], 'end': path[1]}),  200
    
    
    
@app.route('/get_amenities_list', methods=['GET'])
def get_amenities_list_():
    return jsonify(amenities_list())
    

# returns the coordinates of all amenities of the given type
@app.route('/get_amenity_coordinates', methods=['GET'])
def get_amenity_coordinates():
    amenity_name = request.args.get('amenity_name', '')
    
    # get list of amenities
    list_amenities = amenities_list()
    
    if amenity_name in list_amenities:
        return jsonify(get_amenity_data(amenity_name))
    else:
        return jsonify({'error': 'Amenity not found'}), 404

    

# returns the closest amenity to the given coordinates
@app.route('/get_nearest_amenity', methods=['POST'])
def get_nearest_amenity():
    amenity_name = request.get_json()['amenity_name']
    lng, lat = request.get_json()['lng'], request.get_json()['lat']
    lng = float(lng)
    lat = float(lat)
    
    if len(amenity_name) == 0:
        app.logger.warning("'error': 'Amenity name not provided'")
        return jsonify({'error': 'Amenity name not provided'}), 400
    elif not lat or not lng:
        app.logger.warning("'error': 'Coordinates not provided'")
        return jsonify({'error': 'Coordinates not provided'}), 400
    
    list_amenities = amenities_list()    
    
    nearest_amenity = None
    name = None
    min_distance = float('inf')
    
    
    if amenity_name in list_amenities:        
        amenity_geojson = get_amenity_data(amenity_name)
        for feature in amenity_geojson:
            coordinates = feature['geometry']['coordinates']
            distance = round(haversine((lng, lat), coordinates), 4)
            if distance < min_distance:
                nearest_amenity = coordinates
                min_distance = distance
                name = feature['properties']['name']
    else:
        app.logger.info('Amenity not found in list - 404')
                
    for i in range(len(nearest_amenity)):
        nearest_amenity[i] = round(float(nearest_amenity[i]), 4)
    
    if nearest_amenity:
        return jsonify({'coordinates': nearest_amenity, 'name': name, 'distance': min_distance})
    else:
        app.logger.info('Amenity not found - 404')
        return jsonify({'error': 'Amenity not found'}), 404




@app.route('/start-end-path', methods=['POST'])
def start_end_path():
    request_data = request.get_json()
    app.logger.info(f'start-end-path request: {request_data}')
    global G
    subgraph_view = G.edge_subgraph((u, v) for u, v, d in G.edges(data=True) if d['available'])
    g = nx.Graph(subgraph_view)

    start = nearest_node(g, reverse_lat_lon(request_data['start']))
    end = nearest_node(g, reverse_lat_lon(request_data['end']))

    try:
        path = nx.astar_path(g, start, end, heuristic=haversine_heuristic)
        return jsonify({'path': reverse_lat_lon(path)})
    except nx.NetworkXNoPath:
        return jsonify({'error': 'Path not found', 'path': []}), 404


    

@app.route('/road-name-by-path', methods=['POST'])
def road_name_by_path():
    data = request.get_json()
    if 'path' not in data:
        return jsonify({'error': 'Path not provided'}), 400

    # path = [tuple(point) for point in data['path']]
    path = reverse_lat_lon(data['path'])
    app.logger.info('Getting road name for path')

    road_name = []
    try:
        for start, end in zip(path, path[1:]):
            road_name.append(G[tuple(start)][tuple(end)]['name'])
    except KeyError as e:
        app.logger.error(f"Error processing path: {e}")
        return jsonify({'error': 'Invalid path or edge not found'}), 400
    except TypeError as e:
        app.logger.error(f"TypeError: {e}")
        return jsonify({'error': 'Invalid node type in path'}), 400

    unique_road_names = list(set(road_name))
    app.logger.info(f"Road names: {unique_road_names}")
    return jsonify({'road_name': unique_road_names})


@app.route('/ping', methods=['GET'])
def ping():
    return jsonify("Pong!")

    



class Constraint:
    def __init__(self, path):
        self.path = path
        self.id = constraint_id_counter
        constraint_id_counter += 1
    
    def __del__(self):
        constraint_id_counter -= 1
    
    def __eq__(self, other):
        return self.path == other.path



if __name__ == '__main__':
    app.run(debug=True)


# test  a*

#   $ gunicorn api:app

#   $ curl -X GET http://localhost:8000/find_path \
#      -H "Content-Type: application/json" \
#      -d '{"start_lat": 53.271411, "start_lon": -6.205388, "end_lat": 53.248885, "end_lon": -6.172056}'
