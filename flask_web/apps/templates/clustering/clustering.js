var group = L.layerGroup();
var clusterGroup = L.layerGroup();
let groupShowing = false;
let clusterShowing = false;

function initializeClusteringTab() {

    map.eachLayer(function(layer) {
    if (layer instanceof L.Marker || layer instanceof L.Polyline || layer instanceof L.Icon ||  layer instanceof L.Circle || layer instanceof L.marker) {
        group.addLayer(layer);
    }
    });

    if (map.hasLayer(clusterGroup)) {
        clusterShowing = true;
    } else {
        groupShowing = true;
    }
    // Add the group to the map
    map.addLayer(group);
}


function removeLayers() {
    map.removeLayer(group);
    // remove each layer from the map
    map.eachLayer(function(layer) {
        if (layer instanceof L.TileLayer) {
            return;
        }
        map.removeLayer(layer);
    });
}

function toggleLayers() {
    if (map.hasLayer(group)) {
    removeLayers();
    groupShowing = false;
    clusterShowing = true;
    map.addLayer(clusterGroup);
    } else {
    removeLayers();
    map.addLayer(group);
    groupShowing = true;
    clusterShowing = false;
    }
}




function getColorFromString(str) {
    const colors = [
      '#FF69B4', // Pink
      '#0000FF', // Blue
      '#008000', // Green
      '#FFA500', // Orange
      '#800080', // Purple
      '#FFFF00', // Yellow
      '#00FFFF', // Cyan
      '#FF0000', // Red
      '#A52A2A', // Brown
      '#808080', // Gray
      '#000000', // Black
      '#FFFFFF'  // White
    ];
  
    const lastChar = str[str.length - 1];
    const index = parseInt(lastChar) % 12;
  
    return colors[index];
  }

function createCustomMarker(color) {
    return L.divIcon({
      className: 'custom-marker',
      iconSize: [25, 41],
      iconAnchor: [12, 41],
      popupAnchor: [1, -34],
      html: `
        <div style="background-color: ${color}; width: 25px; height: 25px; border-radius: 50%; border: 2px solid #fff;"></div>
        <div style="position: absolute; left: 50%; top: 25px; transform: translateX(-50%); width: 0; height: 0; border-left: 8px solid transparent; border-right: 8px solid transparent; border-top: 16px solid ${color};"></div>
      `
    });
  }


  function getClusters() {
    // if clusterGroup is empty, fetch clusters from the server
    if (clusterGroup.getLayers().length === 0) {
      fetch('/get-clusters')
        .then(response => response.json())
        .then(data => {
          console.log(data);
          // for each cluster, create a marker and add it to the clusterGroup
          data.clusters.forEach((cluster) => {
            let marker = L.marker(cluster.representative.location);
            // color
            let color = getColorFromString(cluster.name);
            marker.setIcon(createCustomMarker(color));
            marker.bindPopup(`<b>${cluster.name}</b>`);
            clusterGroup.addLayer(marker);
          });
  
          // Populate the table with cluster data
          const tableBody = document.getElementById('clusterTableBody');
          data.clusters.forEach((cluster) => {
            const row = document.createElement('tr');
  
            const clusterCell = document.createElement('td');
            clusterCell.textContent = cluster.name;
            clusterCell.style.color = getColorFromString(cluster.name);
            row.appendChild(clusterCell);
  
            const reportsCell = document.createElement('td');
            reportsCell.textContent = cluster.members.length;
            row.appendChild(reportsCell);
  
            const timestampCell = document.createElement('td');
            timestampCell.textContent = cluster.representative.timestamp;
            row.appendChild(timestampCell);
  
            const centroidCell = document.createElement('td');
            centroidCell.textContent = `(${cluster.representative.location.lat}, ${cluster.representative.location.lng})`;
            row.appendChild(centroidCell);

            const summaryCell = document.createElement('td');
            summaryCell.textContent = cluster.representative.description.slice(0, 120) ; 
            row.appendChild(summaryCell);
  
            tableBody.appendChild(row);
          });
        })
        .catch((error) => {
          console.error('Error:', error);
        });
    }
  }

// when docuemtn is ready, initialize the clustering tab
document.addEventListener('DOMContentLoaded', function() {
    setInterval(getClusters, 5000);
});

