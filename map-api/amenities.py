import json
from flask import jsonify


# return coordinates of amenity
def get_amenity_data(amenity_name):
    with open('./amenities/' + amenity_name + '.geojson', 'r') as f:
        amenity_geojson = json.load(f)['features']
        
    return amenity_geojson
    


def amenities_list():
    amenities_list = []
    with open('./amenities/list_amenities.json', 'r') as f:
        amenities_list = json.load(f)['amenities']
        
    return amenities_list