#!/bin/bash

# Name of the Kafka broker container
BROKER_CONTAINER_NAME="broker"

# Get the list of topics
topics=$(docker exec $BROKER_CONTAINER_NAME kafka-topics --bootstrap-server localhost:29092 --list)

# Iterate over each topic and print messages
for topic in $topics; do
    echo "--------------------------------"
    echo "Messages in topic: $topic"
    echo "--------------------------------"
    # Consume messages from the topic
    docker exec -t $BROKER_CONTAINER_NAME kafka-console-consumer --bootstrap-server localhost:29092 --topic $topic --from-beginning --timeout-ms 5000
done
