#!/bin/bash

# Kafka broker Docker container name
KAFKA_CONTAINER="broker"

# Kafka broker list/address
BROKER_LIST="localhost:9092" # Adjust if your broker listens on different host/port

echo "Listing all consumer groups..."
docker exec $KAFKA_CONTAINER kafka-consumer-groups --bootstrap-server $BROKER_LIST --list

# Prompt for consumer group name
read -p "Enter the name of the consumer group you wish to describe: " GROUP_NAME

# Check if the user entered a consumer group name
if [ -z "$GROUP_NAME" ]; then
    echo "No consumer group name entered. Exiting."
    exit 1
fi

echo "Describing consumer group '$GROUP_NAME'..."
docker exec $KAFKA_CONTAINER kafka-consumer-groups --bootstrap-server $BROKER_LIST --describe --group $GROUP_NAME

