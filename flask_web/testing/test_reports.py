import unittest
from mongomock import MongoClient
import os
import sys
import json
project_root_path = os.path.abspath(os.path.join(os.getcwd(), '../'))
sys.path.append(project_root_path)
from apps.api.reports import init_app as reports_api_init
from bson import ObjectId
from unittest.mock import patch, MagicMock
from flask import Flask, jsonify, template_rendered
from flask_login import login_user, LoginManager, current_user
from contextlib import contextmanager
from apps.models import DashboardUser


app = Flask(__name__, template_folder='../apps/templates')
reports_api_init(app)
app.config["SECRET_KEY"] = "your_secret_key_for_testing"

login_manager = LoginManager()  
login_manager.init_app(app)

@login_manager.user_loader
def load_user(user_id):
    return DashboardUser({"_id": user_id, "email": "test@example.com", "phone": "+123456789", "password": "password"})



@contextmanager
def captured_templates(app):
    recorded = []

    def record(sender, template, context, **extra):
        recorded.append((template, context))
    template_rendered.connect(record, app)
    try:
        yield recorded
    finally:
        template_rendered.disconnect(record, app)
        


class TestReportAPI(unittest.TestCase):
    def setUp(self):
        self.app = app
        app.config["TESTING"] = True
        self.client = app.test_client()
        self.app_context = self.app.app_context()
        self.app_context.push()
        self.client = self.app.test_client()
            
    def tearDown(self):
        self.app_context.pop()

    @patch("apps.api.reports.MongoClient")
    @patch("apps.api.reports.requests.post")
    def test_report_details(self, mock_post, mock_client):
        mock_response = MagicMock()
        mock_response.json.return_value = {
            "name": "Hospital",
            "coordinates": "0,0",
            "distance": 100,
        }
        mock_post.return_value = mock_response

        test_user = load_user('mock_user_id')
        with self.client:
            login_manager.login_view = "login"
            with self.client.session_transaction() as session:
                session['_user_id'] = test_user.get_id()
            current_user = test_user
                        
            # Mock the MongoDB call
            mock_db = mock_client.return_value
            mock_collection = mock_db.Dashboard.Reports
            mock_collection.find_one.return_value = {
                "_id": ObjectId("5f5e9c6c2b6e6b4d7b3f3b9e"),
                "name": "Report 1",
                "location": {"lat": 0, "lng": 0},
            }

        with captured_templates(app) as templates:
            mock_db = mock_client.return_value
            mock_collection = mock_db.Dashboard.Reports
            mock_collection.find_one.return_value = {
                "_id": ObjectId("5f5e9c6c2b6e6b4d7b3f3b9e"),
                "name": "Report 1",
                "location": {"lat": 0, "lng": 0},
            }
            
            response = self.client.get("/report/5f5e9c6c2b6e6b4d7b3f3b9e")
            
            self.assertEqual(response.status_code, 200)
            
            self.assertEqual(len(templates), 1)
            
            template, context = templates[0]
            self.assertEqual(template.name, "reports/report_details.html")
            self.assertEqual(context['report']['_id'], "5f5e9c6c2b6e6b4d7b3f3b9e")
            
            
            
    @patch("apps.api.reports.MongoClient")
    def test_get_report_data(self, mock_client):
        # Mock the MongoDB call
        mock_db = mock_client.return_value
        mock_collection = mock_db.Dashboard.Reports
        mock_collection.find.return_value = [
            {"_id": "1", "name": "Report 1"},
            {"_id": "2", "name": "Report 2"},
        ]

        # Make a GET request to the endpoint
        response = self.client.get("/get-report")

        # Verify the response
        self.assertEqual(response.status_code, 200)
        expected_response = jsonify(
            [{"_id": "1", "name": "Report 1"}, {"_id": "2", "name": "Report 2"}]
        )
        self.assertEqual(response.data, expected_response.data)
        
        
    
    def test_get_report_table(self):        
        with captured_templates(app) as templates:
            response = self.client.get("/get-reports-table")
            self.assertEqual(len(templates), 1)
            template, context = templates[0]
            self.assertEqual(template.name, "reports/report_table.html")
            
            
    @patch("apps.api.reports.MongoClient")
    @patch("apps.api.reports.requests.post")
    def test_delete_report(self, mock_post, mock_client):
        mock_response = MagicMock()
        mock_response.json.return_value = {
            "name": "Hospital",
            "coordinates": "0,0",
            "distance": 100,
        }
        mock_post.return_value = mock_response
        
        test_user = load_user('mock_user_id')
        
        with self.client:
            login_manager.login_view = "login"
            with self.client.session_transaction() as session:
                session['_user_id'] = test_user.get_id()
            current_user = test_user
            
            # Mock the MongoDB call
            mock_db = mock_client.return_value
            mock_collection = mock_db.Dashboard.Reports
            mock_collection.delete_one.return_value = {"_id": "5f5e9c6c2b6e6b4d7b3f3b9e"}
            
            response = self.client.post("/delete-report", json={"report_id": "5f5e9c6c2b6e6b4d7b3f3b9e"})
            print(response.data)
            self.assertEqual(response.status_code, 200)
            self.assertEqual(response.data, b'{\n  "message": "Report deleted successfully"\n}\n')
            mock_collection.delete_one.assert_called_once_with({"_id": ObjectId("5f5e9c6c2b6e6b4d7b3f3b9e")})


if __name__ == "__main__":
    unittest.main()
