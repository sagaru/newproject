#!/bin/bash

# Define MongoDB database and collection names
DB_NAME="Dashboard"
COLLECTION_NAME="users"

# Function to check if MongoDB is running
check_mongo_running() {
    if ! pgrep -q mongod; then
        echo "MongoDB is not running. Please start MongoDB and try again."
        exit 1
    fi
}

# Function to create a new database and collection
create_db_and_collection() {
    mongo --eval "db = db.getSiblingDB('$DB_NAME'); db.createCollection('$COLLECTION_NAME');"
}

# Main script execution
check_mongo_running
create_db_and_collection

echo "Database and collection setup completed."

