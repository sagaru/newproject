Pre-requisites: { nodeJS > 18.0 , docker, Ubuntu}
Assuming we clone git repo to the directory ~/

installing packages:
-------------------------
git clone https://gitlab.scss.tcd.ie/sagaru/newproject.git
cd newproject
pip install -r ./flask_web/requirements.txt
pip install -r ./STT/requirements.txt
pip install -r ./map-api/requirements.txt
pip install -r ./clustering/requirements.txt
docker pull mongo
sudo apt-get update
sudo sudo apt-get ffmpeg && apt-get install -y flac 
pip install openai
--------------------------



---------------------------
3 options to run: 
	1. run locally
	2. run all on one server with nginx reverse proxy
	3. Kubernetes
----------------------------





1. run locally
-----------------------------
cd ~/newproject
bash run_local.sh

// all services will now start. This may take a while the first time because
// it must pull several docker images (for kafka strimzi)

// http://localhost:80
----------------------------



2. nginx reverse proxy
----------------------------
follow all steps in (1.) 
sudo apt install nginx
cp ~/newproject/nginx.conf /etc/nginx/nginx.conf
sudo service nginx start

// now access the computer's IP address at port 80

// Note: Requires you to have an api key env variable `$OPENAI_API_KEY`
----------------------------


3. kuberntes
----------------------------
// requires connection to a cluster or minikube
cd ~/newproject/k8s
bash ~/newproject/k8s/kafka/kafka-setup.sh
bash apply_all.sh

kubectl get svc --all-namespaces
// find the public IP address of the ingress controller (the last line of the output from above)
-----------------------------






