const themeSwitch = document.getElementById("theme-switch");
const themeIndicator = document.getElementById("theme-indicator");
const page = document.body;

const themeStates = ["light", "dark"]
const indicators = ["fa-moon", "fa-sun"]
const pageClass = ["bg-gray-100", "dark-page"]

// let currentTheme = localStorage.getItem("theme");
let currentTheme = themeStates[0];


if (currentTheme === null) {
    localStorage.setItem("theme", themeStates[0])
    setIndicator(0)
    setPage(0)
    themeSwitch.checked = true;
}
if (currentTheme === themeStates[0]) {
    setIndicator(0)
    setPage(0)
    themeSwitch.checked = true;

}
if (currentTheme === themeStates[1]) {
    setIndicator(1)
    setPage(1)
    themeSwitch.checked = false;
}

themeSwitch.addEventListener('change', async function () {
    if (this.checked) {
        setTheme(0)
        setIndicator(0)
        setPage(0)
    } else {
        setTheme(1)
        setIndicator(1)
        setPage(1)
    }
    await updateMapTheme();
});

setTheme(currentTheme) ;


function setTheme(theme) {
    localStorage.setItem("theme", themeStates[theme])
    currentTheme = localStorage.getItem("theme")
}

function setIndicator(theme) {
    themeIndicator.classList.remove(indicators[!theme])
    themeIndicator.classList.add(indicators[theme])
}

function setPage(theme) {
    page.classList.remove(pageClass[0])
    page.classList.remove(pageClass[1])
    page.classList.add(pageClass[theme])
}


function getTileLayerUrl() {
    return currentTheme === themeStates[1] ?
        'https://{s}.basemaps.cartocdn.com/dark_all/{z}/{x}/{y}{r}.png' : 
        'https://{s}.basemaps.cartocdn.com/rastertiles/voyager/{z}/{x}/{y}{r}.png'
}
