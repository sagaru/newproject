import unittest
from mongomock import MongoClient
import os
import sys
import json
project_root_path = os.path.abspath(os.path.join(os.getcwd(), '../'))
sys.path.append(project_root_path)
from apps.api.messaging import init_app as messaging_api_init
from bson import ObjectId
from unittest.mock import patch, MagicMock
from flask import Flask, jsonify, template_rendered
from flask_login import login_user, LoginManager, current_user
from contextlib import contextmanager
from apps.models import DashboardUser


app = Flask(__name__, template_folder='../apps/templates')

app.config["SECRET_KEY"] = "your_secret_key_for_testing"

login_manager = LoginManager()  
login_manager.init_app(app)

messaging_api_init(app)


@login_manager.user_loader
def load_user(user_id):
    return DashboardUser({"_id": user_id, "email": "test@example.com", "phone": "+123456789", "password": "password"})



@contextmanager
def captured_templates(app):
    recorded = []

    def record(sender, template, context, **extra):
        recorded.append((template, context))
    template_rendered.connect(record, app)
    try:
        yield recorded
    finally:
        template_rendered.disconnect(record, app)
        


class TestReportAPI(unittest.TestCase):
    def setUp(self):
        self.app = app
        app.config["TESTING"] = True
        self.client = app.test_client()
        self.app_context = self.app.app_context()
        self.app_context.push()
        self.client = self.app.test_client()
            
    def tearDown(self):
        self.app_context.pop()
        
        
    def test_kafka_get_messages(self):
        

    


if __name__ == "__main__":
    unittest.main()



### from messagin.py
# @app.route('/kafka-consumer/get-messages', methods=['POST'])
# @login_required
# def get_messages_kafka():
#     recipient = format_phone(request.get_json()['recipient'])
#     user = DashboardUser.find_by_email(current_user.email)
#     user.subscribe_consumer("DM_"+recipient)
#     messagesKafka = user.get_messages(decode=False, forUser=recipient)
#     app.logger.info("Found {} messages".format(len(messagesKafka)))
#     messages = []
#     for message in messagesKafka:
#         if message.topic().split('_')[1] == recipient:
#             value = message.value().decode('utf-8').split('::')
#             direction = 'inbound' if value[0] == 'user' else 'outbound'
#             messages.append({'value': value[-1], 'time': message.timestamp(), 'direction': direction})
    
#     # message.value, message.direction, message.time
#     return jsonify(messages), 200