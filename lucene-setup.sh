#! /usr/bin/bash

apt-get update
apt-get install default-jdk
apt-get install maven

cd /opt
git clone https://github.com/ColinDaly75/cs7is3-lucene-tutorial-examples.git

chmod 777 -R /opt/cs7is3-lucene-tutorial-examples/cs7is3-lucene-tutorial-examples/index/
