from flask import Flask, request, jsonify
from clustering import cluster_reports, cleanup_clusters, initialize_clustering
from threading import Thread
import os 
app = Flask(__name__)

DEBUG = os.environ.get('DEBUG', False)

def run_cleanup():
    ret, message = cleanup_clusters()
    if ret == -1:
        print(f"Cleanup failed: {message}")
    else:
        print("Cleanup completed successfully")
        
def run_initialization():
    ret, message = initialize_clustering()
    if ret == -1:
        print(f"Initialization failed: {message}")
    else:
        print("Initialization completed successfully")
        
def print_to_console(message):
    app.logger.info(message)


@app.route('/cluster', methods=['POST'])
def cluster_endpoint():
    data = request.get_json()
    new_report_id = data.get('new_report_id')
    if not new_report_id:
        return jsonify({'error': 'Missing new_report_id parameter'}), 400
    try:
        ret, cluster_name = cluster_reports(new_report_id)
        if ret == -1:
            return jsonify({'error': cluster_name}), 500
        
        # Send the HTTP response
        response = jsonify({'cluster_name': cluster_name}), 200
        
        # Start a new thread to run the cleanup function
        cleanup_thread = Thread(target=run_cleanup)
        cleanup_thread.start()
        
        return response
    except Exception as e:
        return jsonify({'error': str(e)}), 500


if __name__ == '__main__':
    if not DEBUG:
        # thread the initialization function
        initialization_thread = Thread(target=run_initialization)
        initialization_thread.start()
    
    app.run()
    
    
    
    