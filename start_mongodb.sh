#!/bin/bash

# MongoDB binary path
MONGO_BIN="/usr/local/mongodb-linux-x86_64-ubuntu2204-7.0.3/bin/mongod"

# MongoDB data and log paths
DBPATH="/data/db"
LOGPATH="/var/log/mongodb/mongod.log"

if [ "$1" == "--fork" ]; then
  sudo "$MONGO_BIN" --dbpath "$DBPATH" --logpath "$LOGPATH" --fork
  echo "MongoDB started in the background."
else
  sudo "$MONGO_BIN" --dbpath "$DBPATH" --logpath "$LOGPATH"
  echo "MongoDB started. To run in the background, use: $0 --fork"
fi

