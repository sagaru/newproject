from flask_login import LoginManager
from apps.models import DashboardUser
from apps.authentication.util import verify_pass

login_manager = LoginManager()


@login_manager.user_loader
def user_loader(user_id):
    if not user_id:
        return None

    return DashboardUser.find_by_id(user_id)


@login_manager.request_loader
def request_loader(request):
    username = request.form.get("username")
    password = request.form.get("password")

    user = DashboardUser.find_by_email(username)
    if not user:
        user = DashboardUser.find_by_phone(username)

    if user and verify_pass(password, user.password):
        return user

    return None
