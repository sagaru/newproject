import speech_api
from pymongo import MongoClient
import unittest
import os
from io import BytesIO
from bson import Binary, ObjectId
from unittest.mock import patch
import base64

class TestSpeechAPI(unittest.TestCase):
    def setUp(self):
        self.client = MongoClient(speech_api.MONGO_URI)
        self.db = self.client['MobileApp']
        self.collection = self.db['AudioMessages']
        self.report_collection = self.client['Dashboard']['Reports']
        self.appClient = speech_api.app.test_client()
    
    # @patch('speech_api.speech_client.recognize')
    def test_transcribe_voice_message(self):
            # read gettysburg.flac and send it as file

            with open('gettysburg.flac', 'rb') as f:
                audio_data = f.read()
                audio_data_base64 = base64.b64encode(audio_data).decode('utf-8')
                
            
            # send the file as post to tets client 
            response = self.appClient.post('/stt-transcribe', json={'file': audio_data_base64})
            self.assertEqual(response.status_code, 200)
            transcription = "four score and seven years"
            self.assertEqual(response.json['transcription'][:len(transcription)], transcription)
    
            
            
    def tearDown(self):
        self.collection.drop()
        
if __name__ == '__main__':
    unittest.main()