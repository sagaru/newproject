import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_app/report_form_page.dart';
import 'package:flutter_app/user.dart';
import 'package:latlong2/latlong.dart';

class UserPreferences {
  static late SharedPreferences _preferences;

  static User user = User();

  static Future init() async =>
      _preferences = await SharedPreferences.getInstance();

  static Future setUser(User user) async {
    await _preferences.setString('phone', user.phone);
    await _preferences.setDouble('latitude', user.location.latitude);
    await _preferences.setDouble('longitude', user.location.longitude);
    await _preferences.setString('dmTopic', user.dmTopic);
    await _preferences.setString('smsVerificationCode', user.smsVerificationCode);
    await _preferences.setString('consumerName', user.consumerName);
    await _preferences.setStringList('dmHistory', user.dmHistory.toList());
    await _preferences.setBool('isLoggedIn', user.isLoggedIn);
  }

  static User getUser() {
    user.phone = _preferences.getString('phone') ?? '';
    user.location = LatLng(
      _preferences.getDouble('latitude') ?? 0.0,
      _preferences.getDouble('longitude') ?? 0.0,
    );
    user.dmTopic = _preferences.getString('dmTopic') ?? '';
    user.smsVerificationCode = _preferences.getString('smsVerificationCode') ?? '';
    user.consumerName = _preferences.getString('consumerName') ?? '';
    user.dmHistory = _preferences.getStringList('dmHistory')?.toSet() ?? {};
    user.isLoggedIn = _preferences.getBool('isLoggedIn') ?? false;
    return user;
  }

  static Future clear() async => await _preferences.clear();

  static Future<bool> get isLoggedIn => Future.value(_preferences.getBool('isLoggedIn') ?? false);

}

