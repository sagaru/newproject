from flask import Flask, render_template


class appObj:
    def __init__(self, app):
        self.app = app

    def info(self, message):
        self.app.logger.info(message)

    def debug(self, message):
        self.app.logger.debug(message)

    def error(self, message):
        self.app.logger.error(message)
        
        
        
app = Flask(__name__)

logger = appObj(app)




# Error Handlers
@app.errorhandler(403)
def access_forbidden(error):
    return render_template('other/page-403.html'), 403

@app.errorhandler(404)
def not_found_error(error):
    return render_template('other/page-404.html'), 404

@app.errorhandler(500)
def internal_error(error):
    # Log the error, notify system administrators, etc.
    return render_template('other/page-500.html'), 500