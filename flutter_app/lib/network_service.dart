import 'package:http/http.dart' as http;

class NetworkService {
  Future<http.Response> sendVoiceMessage(String url, {required String filePath, required Map<String, String> fields}) async {
    var request = http.MultipartRequest('POST', Uri.parse(url))
      ..fields.addAll(fields)
      ..files.add(await http.MultipartFile.fromPath('voice_message', filePath));
    var streamedResponse = await request.send();
    return http.Response.fromStream(streamedResponse);
  }
}
