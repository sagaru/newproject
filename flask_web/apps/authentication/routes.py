
from flask import render_template, redirect, request
from flask_login import current_user, login_user, logout_user
from apps import login_manager
from apps.authentication.forms import LoginForm, CreateAccountForm
from apps.models import Report, DashboardUser
from apps.authentication.util import verify_pass,format_phone



def init_app(app):
    @app.route('/login', methods=['GET', 'POST'])
    def login():
        login_form = LoginForm()

        if login_form.validate_on_submit():
            email_or_phone = login_form.email_or_phone.data
            password = login_form.password.data

            user = DashboardUser.find_by_email(email_or_phone)

            if not user:
                user = DashboardUser.find_by_phone(format_phone(email_or_phone))

            if user and verify_pass(password, user.password):
                login_user(user)
                return redirect('/index')

            return render_template('accounts/login.html', msg='Wrong user or password', form=login_form)

        if current_user.is_authenticated:
            return redirect('/index')
        else:
            return render_template('accounts/login.html', form=login_form)


    @app.route('/register', methods=['GET', 'POST'])
    def register():
        # if method is GET, just return the form template
        if request.method == 'GET':
            return render_template('accounts/register.html', form=CreateAccountForm(), success=False)
        
        
        create_account_form = CreateAccountForm(request.form)
        
        # print the form data to the console
        app.logger.warning(create_account_form.email.data)
        app.logger.warning(create_account_form.phone.data)
        app.logger.warning(create_account_form.password.data)
        app.logger.warning(format_phone(create_account_form.phone.data))
        

        if create_account_form.validate_on_submit():

            email = create_account_form.email.data
            phone = create_account_form.phone.data
            password = create_account_form.password.data  # will be hashed in the model

            # Format phone number
            phone = format_phone(phone) 
            app.logger.warning(phone)

            # Check if email or phone already exists
            if DashboardUser.find_by_email(email):
                return render_template('accounts/register.html', msg='Email already registered', success=False, form=create_account_form)

            if DashboardUser.find_by_phone(phone):
                return render_template('accounts/register.html', msg='Phone already registered', success=False, form=create_account_form)

            # Create the user
            user = DashboardUser.create_user(email=email, phone=phone, password=password)

            # Optionally log the user in after registering
            login_user(user)

            return redirect('/index')

        else:
            # if fields are all empty, just return the form with no message
            if not create_account_form.email.data and not create_account_form.phone.data and not create_account_form.password.data:
                return render_template('accounts/register.html', form=create_account_form, success=False)
            
            return render_template('accounts/register.html', form=create_account_form, success=False, msg='Invalid information provided.')

        
        
    @app.route('/logout/<demo>')
    def logout():
        logout_user()
        return render_template('other/page-404.html', demo="demo"), 404


    @app.route('/logout')
    def logout2():
        logout_user()
        return redirect('/login')

    @app.errorhandler(404)
    def not_found_error(error):
        return render_template('other/page-404.html'), 404



@login_manager.unauthorized_handler
def unauthorized():
    # Redirect to a login page, show an error message, etc.
    return render_template('login_required.html'), 401


