#!/bin/bash

# Find all the process IDs (PIDs) of the mongod process
PIDS=$(pgrep mongod)

if [ -z "$PIDS" ]; then
  echo "MongoDB is not running."
else
  echo "Stopping MongoDB..."
  for PID in $PIDS; do
    echo "Killing MongoDB (PID: $PID)..."
    sudo kill "$PID"
  done
  echo "MongoDB has been stopped."
fi

