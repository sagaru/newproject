# from flask import Flask
from dotenv import load_dotenv
from .authentication import login_manager
from flask_wtf.csrf import CSRFProtect
from .models import MongoDBClient
from .app_init import app

from apps.home.routes import init_app as home_routes_init
from apps.authentication.routes import init_app as auth_routes_init
from apps.api.lucene import init_app as lucene_api_init
from apps.api.twilio import init_app as twilio_api_init
from apps.api.actions import init_app as actions_api_init
from apps.api.monitoring import init_app as monitoring_api_init
from apps.api.reports import init_app as reports_api_init
from apps.api.messaging import init_app as messaging_api_init
from apps.api.cluster import init_app as cluster_api_init


load_dotenv()

# app = Flask(__name__)

app.config["SECRET_KEY"] = "secret!"  # demo only


def register_extensions(app):
    login_manager.init_app(app)


def create_app(config):
    app.config.from_object(config)
    register_extensions(app)

    home_routes_init(app)
    auth_routes_init(app)
    reports_api_init(app)
    lucene_api_init(app)
    twilio_api_init(app)
    actions_api_init(app)
    messaging_api_init(app)
    monitoring_api_init(app)
    cluster_api_init(app)

    return app
