import networkx as nx
from haversine import haversine
import json
import numpy as np
import os
import re

DEBUG = os.environ.get('DEBUG', True)

def is_valid_coordinate(coord):
    """Check if the coordinate is valid (a tuple of two numeric values)."""
    return isinstance(coord, tuple) and len(coord) == 2 and all(isinstance(c, (int, float)) for c in coord)

def create_distance_matrix(G, app=None):
    nodes = list(G.nodes)
    n = len(nodes)
    distance_matrix = np.full((n, n), np.nan)  # Initialize with np.nan for invalid data handling

    for i, node1 in enumerate(nodes):
        for j, node2 in enumerate(nodes):
            if i != j:
                # Only calculate distance if both nodes have valid coordinates
                if is_valid_coordinate(node1) and is_valid_coordinate(node2):
                    distance_matrix[i][j] = haversine(node1, node2)
                elif DEBUG and app is not None:
                    # Log a warning for invalid coordinate data
                    app.logger.warning(f"Invalid coordinate data for nodes: {node1}, {node2}")

    return distance_matrix, nodes



def find_road_endpoints(G, road_name, app=None):
    road_name = road_name.lower()
    H = nx.Graph()
    for u, v, data in G.edges(data=True):
        if data.get('name') == road_name:
            H.add_node(u, **G.nodes[u])
            H.add_node(v, **G.nodes[v])
            H.add_edge(u, v, **data)
    
    
    distance_matrix, nodes = create_distance_matrix(H, app=app)
    
    # Sorting pairs by haversine distance
    n = len(nodes)
    pairs = [(i, j) for i in range(n) for j in range(i+1, n)]
    pairs.sort(key=lambda x: distance_matrix[x[0]][x[1]], reverse=True)

    max_dijkstra_length = 0
    longest_path = None

    for i, j in pairs:
        try:
            dijkstra_length = nx.dijkstra_path_length(H, nodes[i], nodes[j])
            if dijkstra_length > max_dijkstra_length:
                max_dijkstra_length = dijkstra_length
                longest_path = (nodes[i], nodes[j])
        except:
            pass
        
        # Optimization condition
        if distance_matrix[i][j] < max_dijkstra_length:
            break

    return longest_path, max_dijkstra_length




def parse_coordinates_string(string):
    pattern = r"\((-?\d+\.\d+), (\d+\.\d+)\) \((-?\d+\.\d+), (\d+\.\d+)\) (\d+\.\d+)"
    match = re.match(pattern, string)

    if match:
        return [float(x) for x in match.groups()]
    else:
        return None



def add_edges_from_file(G, file_path='./component_edges.txt', app=None):
    if (app):
        app.logger.info(f"Adding edges from file: {file_path}")
    previous_edges = set(G.edges)
    with open(file_path, 'r') as file:
        for line in file:
            parts = parse_coordinates_string(line)
            start_node = (float(parts[0]), float(parts[1]))
            end_node = (float(parts[2]), float(parts[3]))
            distance = float(parts[4])
            G.add_edge(start_node, end_node, weight=distance, name='Unknown', available=True)
            
    if (app):
        app.logger.info(f"Added {len(set(G.edges) - previous_edges)} edges from file")
        app.logger.info(f"Number of connected components: {nx.number_connected_components(G)}")
    
    return G
    


def create_graph(geojson_directory, G, app=None):
    for filename in os.listdir(geojson_directory):
        if filename.endswith('.geojson'):
            file_path = os.path.join(geojson_directory, filename)
            
            with open(file_path, 'r') as file:
                geojson_data = json.load(file)
            
            for feature in geojson_data['features']:
                if feature['geometry']['type'] == 'LineString':
                    road_segments = feature['geometry']['coordinates']
                    road_name = feature['properties'].get('tags', {}).get('name', 'Unknown')
                    if road_name == 'Unknown':
                        if 'name' in feature['properties'].keys():
                            road_name = feature['properties']['name']
                    for start, end in zip(road_segments[:-1], road_segments[1:]):
                        start_node = tuple(start)
                        end_node = tuple(end)
                        G.add_edge(start_node, end_node, weight=haversine(start_node, end_node), name=road_name.lower(), available=True)
    
    if (app):
        app.logger.info(f"graph: {G}")
        app.logger.info(f"Number of connected components: {nx.number_connected_components(G)}")
    G = add_edges_from_file(G, app=app)
    
    
    if (app):
        app.logger.info(f"graph: {G}")
    
    return G



def reverse_lat_lon(coordinates):
    """
    Reverses the order of latitude and longitude in a list of coordinates.

    :param coordinates: A list of coordinates (latitude, longitude)
    :return: A list of coordinates (longitude, latitude)
    """
    if len(coordinates) == 0:
        return []
    elif len(coordinates) == 2:
        return [coordinates[1], coordinates[0]]
    return [[coord[1], coord[0]] for coord in coordinates]


def a_star_search(graph, start, goal):
    """
    Find the shortest path between start and goal nodes using A* algorithm.

    :param graph: NetworkX graph (already filtered for available edges)
    :param start: Start node
    :param goal: Goal node
    :return: Shortest path as a list of nodes, or None if no path is found
    """
    try:
        return nx.astar_path(graph, start, goal)
    except nx.NetworkXNoPath:
        return None



def haversine_heuristic(u, v):
    """
    Heuristic function for A* algorithm, calculating the haversine distance
    between two nodes.

    :param u: The current node
    :param v: The goal node
    :return: Haversine distance between u and v
    """
    return haversine(u, v)



def apply_constraints(graph, constraints, remove=False):
    """
    Modifies the graph based on constraints (like avoiding certain roads).
    It can either remove the constrained edges or mark them as unavailable.

    :param graph: The NetworkX graph representing the road network
    :param constraints: A list of edges (each edge is a tuple of nodes) to be constrained
    :param remove: Boolean flag to indicate if edges should be removed or just marked
    """
    
    for edge in constraints:
        if remove:
            if graph.has_edge(*edge):
                graph.remove_edge(*edge)
        else:
            if graph.has_edge(*edge):
                graph.edges[edge[0], edge[1]]['available'] = False
                
    return  graph.edge_subgraph((u, v) for u, v, d in graph.edges(data=True) if d['available'])

def restore_constraints(graph, constraints):
    """
    Restores the constrained edges that were previously marked as unavailable.

    :param graph: The NetworkX graph representing the road network
    :param constraints: A list of edges to be restored
    """
    for edge in constraints:
        if graph.has_edge(*edge):
            graph.edges[edge[0], edge[1]]['available'] = True
            
    return graph.edge_subgraph((u, v) for u, v, d in graph.edges(data=True) if d['available'])


def manhattan_distance(point1, point2):
    return abs(point1[0] - point2[0]) + abs(point1[1] - point2[1])


def nearest_node(graph, coord):
    """
    Finds the nearest node in the graph to a given set of coordinates.

    :param graph: The NetworkX graph representing the road network
    :param coord: A tuple representing the coordinates (latitude, longitude)
    :return: The nearest node to the given coordinates
    """
    nearest = None
    min_distance = float('inf')
    
    g = graph.edge_subgraph((u, v) for u, v, d in graph.edges(data=True) if d['available'])

    for node in g.nodes():
        distance = haversine(coord, node)
        if distance < min_distance and distance != 0:
            nearest = node
            min_distance = distance

    # (latitude, longitude)
    return nearest             





#### testing ###

# if (__name__ == "__main__"):
#     G = nx.Graph()
#     G = create_graph('./routes', G)
#     for u, v, d in G.edges(data=True):
#         if 'sutton' in d['name']:
#             print('success')
            
    
#     print('Done')



#### testing ###

# from shapely.geometry import LineString, Point
# import geopandas as gpd    
# import matplotlib.pyplot as plt
# import matplotlib
# matplotlib.use('TkAgg')

# def main():
#     geojson_file_path = './routes'
#     road_name = "Stillorgan Road"
#     G = nx.Graph()
#     G = create_graph(geojson_file_path, G)

#     # Find the longest path in the graph
#     longest_path, max_length = find_road_endpoints(G, road_name)
#     print("Longest path:", longest_path)
#     print("Length of the longest path:", max_length)
#     path = a_star_search(G, longest_path[0], longest_path[1])
#     #plot with gpd
#     print(path)
#     gdf = gpd.GeoDataFrame(geometry=[LineString(path)])
#     gdf.plot()
#     plt.show()

# if __name__ == "__main__":
#     main()
