:: you may need to run `.\ipmap.bat` as administrator

set WSL_IP=172.30.218.3

:: for minikube 
netsh interface portproxy add v4tov4 listenaddress=0.0.0.0 listenport=8080 connectaddress=%WSL_IP% connectport=80

:: for node-server.js
netsh interface portproxy add v4tov4 listenaddress=0.0.0.0 listenport=3000 connectaddress=%WSL_IP% connectport=3000
netsh interface portproxy add v4tov4 listenaddress=127.0.0.1 listenport=3000 connectaddress=%WSL_IP% connectport=3000

:: for kafka REST proxy
netsh interface portproxy add v4tov4 listenaddress=0.0.0.0 listenport=8082 connectaddress=%WSL_IP% connectport=8082

netsh interface portproxy show all



