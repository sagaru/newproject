var activePolylines = new Map();
var mapMarkers = new Map();
var polyLineCounter = 0;
var markerCounter = 0;

// var startPointSelected = false;
// var endPointSelected = false;
var startCoordinates = null ; 
var endCoordinates = null ; 
let isSelectingStartPoint = false;
let isSelectingEndPoint = false;

var road_coordinates = null ;
var roadName = null ;




const target = document.getElementById('spinner');
console.log(target);
var spinner = new Spinner({ color: '#3d3d3d' });


function updateDivider() {
    const dividers = document.querySelectorAll('.section-divider');
    // Check if there are any dividers present
    if (dividers.length > 0) {
        console.log('updateDivider');
        dividers.forEach(divider => {
            if (currentTheme === themeStates[1]) { // Dark theme
                divider.classList.add('divider-dark');
                divider.classList.remove('divider-light');
            } else { // Light theme
                divider.classList.remove('divider-dark');
                divider.classList.add('divider-light');
            }
        });
    }
}


function addPolyline(id, path, options) {
    var polyline = L.polyline(path, options);
    polyline.on('click', function() {
        if (confirm("Are you sure you want to remove this route?")) {
            removePolyline(id);
        }
    });
    polyline.addTo(map);
    polyline.snakeIn();
    activePolylines.set(id, polyline);
    // console.log('AcivePolylines:', activePolylines);
}

function removePolyline(id) {
    if (activePolylines.has(id)) {
        var polyline = activePolylines.get(id);
        map.removeLayer(polyline);
        activePolylines.delete(id);
    }
    polyLineCounter--;
}


function loadPolylines() {
    // console.log('Polylines:', activePolylines) ;
    activePolylines.forEach((polyline, id) => {
        addPolyline(id, polyline.getLatLngs(), polyline.options, polyline.metadata);
        polyLineCounter++; 
    }
    );
}

function addMapMarker(coordinates, label) {

    var greenIcon = new L.Icon({
        iconUrl: 'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-green.png',
        iconSize: [25, 41],
        iconAnchor: [12, 41],
        popupAnchor: [1, -34],
        shadowSize: [41, 41]
    });

    var marker = L.marker(coordinates, {icon: greenIcon}).addTo(map)
    // onClick remove function ?
    markerCounter++;
    const id = 'marker' + String(markerCounter) ; 
    mapMarkers.set(id, marker); // Store the marker with a unique ID
    // add lable to marker
    marker.bindPopup(label);
}

function removeMapMarker() {
    const id = 'marker' + String(markerCounter) ; 
    var marker = mapMarkers.get(id);
    if (marker) {
        map.removeLayer(marker); // Remove the marker from the map
        mapMarkers.delete(id);   // Remove the marker reference from the Map

    } else {
        console.log('Marker with ID ' + id + ' does not exist.');
    }
    markerCounter--;
}

function clearMapMarkers() {
    mapMarkers.forEach((marker, id) => {
        map.removeLayer(marker);
        mapMarkers.delete(id);
    });
    markerCounter = 0;
}
    


function addPolylineBlock(path,id, options) {
    const counter = 'polyline' + String(polyLineCounter) ; 
    //if id already exists in activePolylines, return
    for (const [key, value] of activePolylines.entries()) {
        if (value.metadata === id) {
            console.log('Polyline with id:', id, 'already exists');
            return;
        }
    }
    
    console.log('Adding polyline with id:', counter);
    var polyline = L.polyline(path, options);
    polyline.metadata = id;   // for database retrieval
    polyline.on('click', function() {
        if (confirm("Are you sure you want to unblock this route?")) {
            removePolylineBlock(counter);
            spinner.spin(target);
            // fetch post to '/unblock-route' with 'path' = path
            fetch('/unblock-route', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({ id: id}),
            })
            .then(response => {
                spinner.stop();
                if (response.status !== 200) {
                    console.error('Error: Response status is not 200');
                    throw new Error('Non-200 response status');
                }
                return response.json();
            })
            .catch(error => {
                console.error('Fetch error:', error)
                spinner.stop();
            });
    }
    });
    polyline.addTo(map);
    polyline.snakeIn();
    activePolylines.set(counter, polyline);
    polyLineCounter++;
}

function removePolylineBlock(counter) {
        console.log('Removing polyline with id:', counter);
        if (activePolylines.has(counter)) {
            var polyline = activePolylines.get(counter);
            map.removeLayer(polyline);
            activePolylines.delete(counter);
        }
    polyLineCounter--;
}




function initializeActionsTab() {
    let selectedCoordinates;
    let isSelectingLocation = false;

    const selectLocationButton = document.getElementById('selectLocation');
    const serviceSelect = document.getElementById('serviceSelect');
    const confirmButton = document.getElementById('confirmButton');
    const mapContainer = document.getElementById('map'); 
    const findRoad = document.getElementById('findRoad') ; 
    const selectStartPoint = document.getElementById('selectStartPoint');
    const selectEndPoint = document.getElementById('selectEndPoint');
    const cancel = document.getElementById('cancel') ; 
    const cancelSvc = document.getElementById('cancel-deploy-svc') ;
    const blockRoute = document.getElementById('blockRoute') ;

    if (selectLocationButton) {
        selectLocationButton.addEventListener('click', function() {
            isSelectingLocation = !isSelectingLocation;
            if (isSelectingLocation) {
                this.textContent = 'Cancel Selection';
                mapContainer.classList.add('pointer-cursor');
            } else {
                this.textContent = 'Select Location on Map';
                mapContainer.classList.remove('pointer-cursor'); 
            }
        });
    }

    function onMapClick(e) {
        if (isSelectingLocation) {
            selectedCoordinates = e.latlng;
            document.getElementById('destination').value = `${e.latlng.lat.toFixed(6)},${e.latlng.lng.toFixed(6)}`;
            updateConfirmButtonVisibility();
            isSelectingLocation = false;
            selectLocationButton.textContent = 'Select Location on Map';
            addMapMarker(selectedCoordinates, 'Destination for: ' + serviceSelect.value);
        }
        isSelectingLocation = false;
        mapContainer.classList.remove('pointer-cursor');

        if (isSelectingStartPoint){
            startCoordinates = e.latlng;
            addMapMarker(startCoordinates, 'start');
            isSelectingStartPoint = false;
        }

        if (isSelectingEndPoint){
            endCoordinates = e.latlng;
            addMapMarker(endCoordinates, 'end');
            isSelectingEndPoint = false;
        }

    }

    if (blockRoute) {
        blockRoute.addEventListener('click', function () {
            block_route();
            selectEndPoint.style.display = 'none';
            blockRoute.style.display = 'none';
            console.log('blockRoute clicked');
        });
    }


    if (map) {
        map.on('click', onMapClick);
    } else {
        console.error('Map object not found');
    }

    function updateConfirmButtonVisibility() {
        const serviceSelected = serviceSelect.value;
        const destinationSelected = document.getElementById('destination').value;
        confirmButton.style.display = serviceSelected && destinationSelected ? 'block' : 'none';
    }

    if (serviceSelect) {
        serviceSelect.addEventListener('change', updateConfirmButtonVisibility);
    }

    if (confirmButton) {
        confirmButton.addEventListener('click', function() {
            spinner.spin(target);
            const service = serviceSelect.value;
            fetch('/get-route', {    // returns a list of coordinates as value for key 'path'
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({ service: service, destination: selectedCoordinates }),
            })
            .then(response => response.json())
            .then(data => {
                spinner.stop();
                // console.log('Success:', data);
                const polylineOptions = {
                    color:  'rgb(255, 0, 234)',
                    weight: 5, 
                    opacity: 0.8,
                    lineJoin: 'round'
                };
                addPolyline('polyline' + String(polyLineCounter), data.path, polylineOptions);
                polyLineCounter++;
            })
            .catch((error) => {
                spinner.stop();
                console.error('Error:', error);
            });
        });
    }


    if (findRoad) {
        findRoad.addEventListener('click', function() {
            blockRoute.style.display = 'block';
            roadName = document.getElementById('roadName').value;
            // Send roadName to an API and get back a list of coordinates
            // Highlight the route on the map with the returned coordinates
            spinner.spin(target);
            // post request to '/get-road' with 'road_name' = roadName
            fetch('/find-road', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({ road_name: roadName }),
            })
            .then(response => {
                spinner.stop();
                if (response.status !== 200) {
                    console.error('Error: Response status is not 200');
                    throw new Error('Non-200 response status');
                }
                return response.json();
            })
            .then(data => {
                console.log('Success:', data);
                const polylineOptions = {
                    color:  'rgb(255, 0, 234)',
                    weight: 5, 
                    opacity: 0.8,
                    lineJoin: 'round'
                };
                console.log(data.path)  ; 
                addPolyline('polyline' + String(polyLineCounter), data.path, polylineOptions);
                polyLineCounter++;
                road_coordinates = data.path ;
                startCoordinates = data.start
                endCoordinates = data.end
            })
            .catch(error => {
                console.error('Fetch error:', error) ; 
                spinner.stop();
            });
        });
        
    }


    if (selectStartPoint) { // selectStartPoint button clicked
        selectStartPoint.addEventListener('click', function() {
            // Implement logic to allow user to select a point on the map
            // Save the selected point's coordinates to startCoordinates
            // Set startPointSelected to true once selected
            clearMapMarkers() ; // remove all markers
            isSelectingStartPoint = true;
            isSelectingEndPoint = false; // reset
            // now show endpoint button by setting display to block
            selectEndPoint.style.display = 'inline-block';
        });
    }

    if (selectEndPoint) { // selectEndPoint button clicked
        selectEndPoint.addEventListener('click', function() {
            // Implement logic to allow user to select a point on the map
            // Save the selected point's coordinates to endCoordinates
            // Set endPointSelected to true once selected
            // If both start and end points are selected, show the blockRoute button

            

            isSelectingEndPoint = true;
            isSelectingStartPoint = false;  // ensure that only one point is selected at a time
            blockRoute.style.display = 'block';
            cancel.style.display = 'block';
        });
    }

    if (cancel) {
        cancel.addEventListener('click', function() {
            // Implement logic to allow user to select a point on the map
            // Save the selected point's coordinates to startCoordinates
            // Set startPointSelected to true once selected
            isSelectingStartPoint = false;
            isSelectingEndPoint = false;
            startCoordinates = null ; 
            endCoordinates = null ; 
            selectEndPoint.style.display = 'none';
            blockRoute.style.display = 'none';
            road_coordinates = null ;
            roadName = null ;
            clearMapMarkers() ; // remove all markers

        });
    }
    if (cancelSvc) {
        isSelectingStartPoint = false;
        isSelectingEndPoint = false;
        startCoordinates = null ; 
        endCoordinates = null ; 
        selectEndPoint.style.display = 'none';
        blockRoute.style.display = 'none';
        road_coordinates = null ;
        roadName = null ;
        clearMapMarkers() ; // remove all markers
    }

}


function block_route() {
    if (confirm("Are you sure you want to block this route?")) {
        console.log('blocking route from ' + startCoordinates + ' to ' + endCoordinates);
        if (startCoordinates && endCoordinates) {
            spinner.spin(target);
            console.log(startCoordinates, endCoordinates) ; 
            fetch('/block-route', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({ start: startCoordinates, end: endCoordinates }),
            })
            .then(response => {
                spinner.stop();
                console.log(response)
                if (response.status !== 200) {
                    console.error('Error: Response status is not 200');
                    throw new Error('Non-200 response status');
                }
                return response.json();
            })
            .then(data => {
                console.log(data) ; 
                const polylineOptions = {
                    color:  'red',
                    weight: 5, 
                    opacity: 0.8,
                    lineJoin: 'round'
                };
                addPolylineBlock(data.path, data.id,  polylineOptions);
            })
            .catch(error => {
                console.error('Fetch error:', error) ; 
                spinner.stop();
            });
        }

        clearMapMarkers() ; // remove all markers
        isSelectingStartPoint = false;
        isSelectingEndPoint = false;
        startCoordinates = null ; 
        endCoordinates = null ; 
        selectEndPoint.style.display = 'none';
        road_coordinates = null ;
        roadName = null ;
    }
}

