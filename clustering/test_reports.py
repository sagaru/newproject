# Test reports

reports = [

### Flood at Booterstown 10am 
# time ranges from 10:09-11:50 am (~2hrs) on 01/02.2024
# threes residents report flooding in booterstown and are requesting emergency services
# all reports are within ~600 m of each other
## booters_town_flood_1  
{
    'timestamp': '2024-02-01 10:09:26.851542Z',
    'description': 'B Heavy rain has caused severe flooding in the Booterstown area. The water levels are rising rapidly, and streets are becoming impassable. Emergency services are needed immediately to assist residents and prevent further damage. Please send help!',
    'location': { 'lat': 53.30968676936922, 'lng': -6.198008918699237 },
    'phone': None
},


## booters_town_flood_2  
{
    'timestamp': '2024-02-01 11:50:26.153542Z',
    'description': 'B We\'re facing a critical situation in Booterstown as floodwaters continue to rise. Several homes are inundated, and residents are stranded. Firefighters and rescue teams are urgently required to ensure the safety of our community. Please respond promptly!',
    'location': { 'lat': 53.309356620048675, 'lng': -6.20405270814696 },
    'phone': '+353 123456333'
},

## booters_town_flood_3   
{
    'timestamp': '2024-02-01 10:50:26.153542Z',
    'description': 'B The ongoing heavy rainfall has led to widespread flooding in Booterstown. Our streets are submerged, and many homes are at risk. Immediate action is needed to evacuate residents and provide assistance. Emergency services, please mobilize quickly!',
    'location': { 'lat': 53.30655662607413, 'lng': -6.19862211763689 },
    'phone': '+353 123477789'
},


### Car crash at Dun Laoghaire 6pm
# time ranges from 18:00-19:45 (~2hrs) on 01/02.2024
# threes residents report a car crash  in booterstown and are requesting emergency services
# all reports are within 1km m of each other
## dl_car_crash_1  
{
    'timestamp': '2024-02-01 18:00:26.851542Z',
    'description': 'DL 1 There\'s been a serious car crash near the Dun Laoghaire town center. Multiple vehicles are involved, and it appears to be a major accident. Emergency services are needed urgently to provide assistance and clear the scene. Please respond promptly!',
    'location': { 'lat': 53.291061008346176, 'lng': -6.14306005278669 },
    'phone': None
},

## dl_car_crash_2 
{
    'timestamp': '2024-02-01 18:55:26.153542Z',
    'description': 'DL 2 A significant car crash has occurred on the main road in Dun Laoghaire. The situation is critical, and immediate intervention is required. We urge the emergency services to attend to the injured and manage traffic disruption in the area.',
    'location': { 'lat': 53.29006259980741, 'lng': -6.130406586241863 },
    'phone': '+353 423456289'
},

## dl_car_crash_3 
{
    'timestamp': '2024-02-01 19:45:26.153542Z',
    'description': 'DL 3 We\'ve just witnessed a car crash in Dun Laoghaire, and the scene is chaotic. Emergency services are urgently needed to assist the injured and address the traffic buildup. Please dispatch help immediately to ensure the safety of everyone involved.',
    'location': { 'lat': 53.29441912037876, 'lng': -6.136733319514277 },
    'phone': '+353 123356749'
},

## dl_car_crash_4 
{
    'timestamp': '2024-02-01 18:00:26.153542Z',
    'description': 'DL 4 Urgent: Major Car Crash in Dun Laoghaire Town Center! A serious accident involving multiple vehicles has occurred near the heart of Dun Laoghaire. The situation is grave, and immediate assistance from emergency services is crucial. We plea for a prompt response to aid the injured and clear the scene for the safety of everyone involved.',
    'location': { 'lat': 53.29557614347157, 'lng': -6.144686336012579 },
    'phone': '+353 123356749'
},


### House Fire at Dalkey 7pm
# time ranges from 19:02-20:00 (~1hrs) on 01/02.2024
# four residents report a car crash  in booterstown and are requesting emergency services
# all reports are within 1km m of each other
## dalkey_fire_1 
{
    'timestamp': '2024-02-01 19:12:26.851542Z',
    'description': 'DK 1 A serious house fire has broken out in the Dalkey neighborhood. Flames are visible, and the situation is escalating quickly. We need immediate assistance from the fire department to control the blaze and ensure the safety of residents. Please respond urgently!',
    'location': { 'lat': 53.283840088854, 'lng': -6.105662929839503 },
    'phone': None
},

## dalkey_fire_2 
{
    'timestamp': '2024-02-01 19:22:26.153542Z',
    'description': 'DK 2 Our community is in distress as a house fire intensifies in Dalkey. The flames are spreading rapidly, and it\'s imperative that the fire department intervenes promptly. We urgently request assistance to extinguish the fire and protect our homes.',
    'location': { 'lat': 53.28278665567613, 'lng': -6.11013272354363 },
    'phone': '+353 123454489'
},

## dalkey_fire_3 
{
    'timestamp': '2024-02-01 19:55:26.153542Z',
    'description': 'DK 3 A devastating house fire is underway in Dalkey, posing a threat to neighboring homes. We require immediate action from emergency services to contain the fire and ensure the safety of residents. Your prompt response is crucial in this emergency situation.',
    'location': { 'lat': 53.28316955186543, 'lng': -6.110613054293732 },
    'phone': '+353 123116789'
},

## dalkey_fire_4 
{
    'timestamp': '2024-02-01 20:00:26.153542Z',
    'description': 'DK 4 Emergency: Dalkey House Fire Escalating! The situation in Dalkey is critical as a house fire intensifies. Flames are spreading rapidly, posing a serious threat to neighboring homes. Immediate intervention from the fire department is essential to control the blaze and safeguard residents. We urgently seek assistance; please respond promptly to this escalating emergency!',
    'location': { 'lat': 53.277697117080564, 'lng': -6.100729983124778 },
    'phone': None
},

### House Fire at Drumconda 7pm
# time ranges from 09:12-10:55 (~1hrs) on 01/02.2024
# threes residents report a car crash  in booterstown and are requesting emergency services
# all reports are within 1km m of each other
## drumconda_flood_1 
{
    'timestamp': '2024-02-01 09:12:26.851542Z',
    'description': 'DC Emergency! Flooding crisis in Drumcondra! The water levels are rapidly rising, and houses and shops are already being affected. Residents are stranded, and immediate action is needed to rescue people and prevent further damage. Urgent assistance required!',
    'location': { 'lat': 53.36793720304651, 'lng': -6.257798683343619 },
    'phone': None
},

## drumconda_flood_2 
{
    'timestamp': '2024-02-01 09:22:26.153542Z',
    'description': 'DC Urgent Flood Alert in Drumcondra! The ongoing heavy rainfall has caused severe flooding, putting homes and businesses at risk. Streets are submerged, and the situation is becoming critical. Emergency services, please respond quickly to help evacuate residents and address the crisis!',
    'location': { 'lat': 53.36756409391939, 'lng': -6.253108842727741 },
    'phone': '+353 123454489'
},

## drumconda_flood_3 
{
    'timestamp': '2024-02-01 10:20:26.153542Z',
    'description': 'DC Flooding Emergency in Drumcondra! The neighborhood is in distress as floodwaters continue to rise, threatening homes and shops. The situation requires immediate attention from emergency services to ensure the safety of residents and prevent further devastation. Please act promptly!',
    'location': { 'lat': 53.36707617705331, 'lng': -6.258255642172858 },
    'phone': '+353 123116789'
},

## drumconda_flood_4 
{
    'timestamp': '2024-02-01 10:55:26.153542Z',
    'description': 'DC Drumcondra Flood Disaster! Unprecedented flooding is wreaking havoc on our community. Houses and shops are submerged, and the situation is dire. We urgently appeal to emergency services to coordinate a swift response for rescue and relief efforts. The need is urgent; please act now!',
    'location': { 'lat': 53.36632994044944, 'lng': -6.256980967543927 },
    'phone': '+353 123116789'
},

### Bomb threat at Croke Park
# time ranges from 17:12-19:25 (~1hrs) on 01/02.2024
# six attending a game at Croke Park  in booterstown and are requesting emergency services
# all reports are within 1km m of each other
## croker_bomb_1 
{
    'timestamp': '2024-02-01 17:12:26.851542Z',
    'description': 'CR I\'ve noticed suspicious activity around Croke Park stadium. Unattended bags and unusual behavior. Please investigate immediately.',
    'location': { 'lat': 53.36164312366696, 'lng': -6.25225657065297 },
    'phone': None
},

## croker_bomb_2 
{
    'timestamp': '2024-02-01 17:22:26.153542Z',
    'description': 'CR Witnessed an individual acting suspiciously near the stadium entrance. Reporting for safety concerns. Requesting thorough inspection.',
    'location': { 'lat': 53.361941811467666, 'lng': -6.249389980772068 },
    'phone': '+353 123454489'
},

## croker_bomb_3 
{
    'timestamp': '2024-02-01 17:28:26.153542Z',
    'description': 'CR There\'s a bag left unattended near the stadium gates. Concerned about potential security risk. Please check and ensure public safety.',
    'location': { 'lat': 53.35968802454165, 'lng': -6.2485254536651285 },
    'phone': '+353 123116789'
},

## croker_bomb_4 
{
    'timestamp': '2024-02-01 17:32:26.851542Z',
    'description': 'CR Observing a suspicious person moving around Croke Park. Acting nervously and constantly checking their surroundings. Authorities should investigate.',
    'location': { 'lat': 53.36289216781349, 'lng': -6.254304134853615 },
    'phone': None
},

## croker_bomb_5 
{
    'timestamp': '2024-02-01 18:22:26.153542Z',
    'description': 'CR I\'ve come across a package near Croke Park that appears suspicious. No one seems to claim it. Please take immediate action to assess the situation.',
    'location': { 'lat': 53.36308223653909, 'lng': -6.2567157104677085 },
    'phone': '+353 123454489'
},

## croker_bomb_6 
{
    'timestamp': '2024-02-01 19:25:26.153542Z',
    'description': 'CR Noticed a group of people behaving strangely near the stadium. They seem to be acting in an unusual manner. Reporting for the safety of the public.',
    'location': { 'lat': 53.36302793127544, 'lng': -6.258672271814991 },
    'phone': '+353 123116789'
},

### Lost dog at 
# time ranges from 17:12 on 01/02.2024
# six attending a game at Croke Park  in booterstown and are requesting emergency services
# all reports are within 1km m of each other
## dog_annepark_1 
{
    'timestamp': '2024-02-01 17:12:26.851542Z',
    'description': 'I have lost my dog. I am at St. Anne\'s Park at the moment with my grandchild. Her name is Ellie. She is very worried about Ron (he is my dog). Could you please send the police to help search for my poor dog. Thank you',
    'location': { 'lat': 53.37296961351961, 'lng': -6.189202343177532 },
    'phone': None
}

]