import unittest
from astar import apply_constraints
import networkx as nx

class TestApplyConstraints(unittest.TestCase):

    def setUp(self):
        self.graph = nx.Graph()
        self.graph.add_edge((0, 0), (1, 1), available=True)
        self.graph.add_edge((1, 1), (2, 2), available=True)
        self.graph.add_edge((2, 2), (3, 3), available=True)
        self.graph.add_edge((12, 12), (13, 13), available=True)
        self.graph.add_edge((13, 13), (14, 14), available=True)


    def test_apply_constraints_without_removal(self):
        constraints = [((0, 0), (1, 1))]
        apply_constraints(self.graph, constraints) # remove is False by default
        original_edges = list(self.graph.edges)
        # graph should remain unchanged since edge not removed
        self.assertEqual(list(self.graph.edges), original_edges, "Graph should remain unchanged.")
        self.assertFalse(self.graph.edges[(0, 0), (1, 1)]['available'], "Edge should be marked as unavailable.")

    def test_apply_constraints_with_removal(self):
        constraints = [((0, 0), (1, 1)), ((1,1), (2,2))]
        apply_constraints(self.graph, constraints, remove=True) # remove the edge specified above
        self.assertFalse(self.graph.has_edge((0, 0), (1, 1)), "Edge should be removed.")
        self.assertFalse(self.graph.has_edge((1, 1), (2, 2)), "Edge should be removed.")


        constraints = [((2,2), (13,13))]
        apply_constraints(self.graph, constraints, remove=True) # remove the edge specified above
        self.assertTrue(self.graph.has_edge((13, 13), (14, 14)), "Edge should be removed.")

    def test_apply_constraints_non_existing_edge(self):
        constraints = [((0, 0), (4, 4))] # This edge does not exist in graph
        original_edges = list(self.graph.edges)
        apply_constraints(self.graph, constraints, remove=True)
        self.assertEqual(list(self.graph.edges), original_edges, "Graph should remain unchanged.")

    def test_apply_empty_constraints(self):
        constraints = []
        original_edges = list(self.graph.edges)
        apply_constraints(self.graph, constraints)
        self.assertEqual(list(self.graph.edges), original_edges, "Graph should remain unchanged.")

if __name__ == '__main__':
    unittest.main()
