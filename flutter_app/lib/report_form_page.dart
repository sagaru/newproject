import 'package:flutter/material.dart';
import 'package:flutter_app/user.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:permission_handler/permission_handler.dart';
import 'main.dart' ;

class ReportFormPage extends StatefulWidget {
  const ReportFormPage({super.key});

  @override
  _ReportFormPageState createState() => _ReportFormPageState();
}

class _ReportFormPageState extends State<ReportFormPage> {
  final _formKey = GlobalKey<FormState>();
  final _descriptionController = TextEditingController();
  final FocusNode _descriptionFocusNode = FocusNode();

  @override
  void initState() {
    super.initState();
    _requestPermission();
  }

  static Future<void> _requestPermission() async {
    var status = await Permission.location.request();
    if (status.isGranted) {
      print("Location permission granted.");
    } else if (status.isDenied) {
      throw Exception('Location permission denied');
    } else if (status.isPermanentlyDenied) {
      openAppSettings();
    }
  }


  Future<void> _submitData() async {
    final enteredDescription = _descriptionController.text;
    final timestamp = DateTime.now().toUtc().toString();
    // final url = Uri.parse('$serverAddr:$expressJsPort/flutter-report');
    final url = Uri.parse('$serverAddr/flutter-report');


    try {
      final response = await http.post(
        url,
        headers: {'Content-Type': 'application/json'},
        body: json.encode({
          'timestamp': timestamp,
          'location': {
            'lat': user.location.latitude,
            'lng': user.location.longitude,
          },
          'description': enteredDescription,
        }),
      );

      if (response.statusCode == 200) {
        // show message to user
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(
            content: Text('Data submitted successfully'),
          ),
        );
      } else {
        // show error message to user
        ScaffoldMessenger.of(context).showSnackBar(
           SnackBar(
            // include response code for debugging purposes
            content: Text('Error submitting data : ${response.statusCode}')
          ),
        );
      }
    } catch (error) {
      // show error message to user
      final msg = 'Error: $error' ;
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(msg),
        ),
      );
    }
  }

  @override
  void dispose() {
    // Dispose the controllers and focus nodes
    _descriptionController.dispose();
    _descriptionFocusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Report an Incident'),
      ),
      body: Form(
        key: _formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            if (user.location.latitude != 0.0 && user.location.longitude != 0.0)
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text('Coordinates: Lat: ${user.location.latitude}, Lng: ${user.location.longitude}'),
              ),
            TextFormField(
              focusNode: _descriptionFocusNode,
              controller: _descriptionController,
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Please enter a description of the incident';
                }
                return null;
              },
              maxLines: null,
              keyboardType: TextInputType.multiline,
              maxLength: 200,
            ),
            ElevatedButton(
              onPressed: () {
                if (_formKey.currentState!.validate()) {
                  _submitData();
                }
                // void the focus to dismiss the keyboard
                _descriptionFocusNode.unfocus();
                // clear the text field
                _descriptionController.clear();
              },
              child: const Text('Submit'),
            ),
          ],
        ),
      ),
    );
  }
}