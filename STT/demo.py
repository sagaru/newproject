import os
from pydub import AudioSegment
from google.cloud import speech
from google.cloud.speech import RecognitionConfig, RecognitionAudio
from google.oauth2 import service_account

# Local file paths
LOCAL_WAV_FILE = 'gettysburg.wav'
GOOGLE_CREDENTIALS_FILE = 'sa-key.json'

# Set up Google Speech-to-Text client
credentials = service_account.Credentials.from_service_account_file(GOOGLE_CREDENTIALS_FILE)
speech_client = speech.SpeechClient(credentials=credentials)

def transcribe_voice_message():
    try:
        # Load WAV file and get sample rate
        wav_audio = AudioSegment.from_file(LOCAL_WAV_FILE, format='wav')
        sample_rate = wav_audio.frame_rate

        # Transcribe WAV file using the Speech-to-Text API
        with open(LOCAL_WAV_FILE, 'rb') as audio_file:
            content = audio_file.read()

        audio = RecognitionAudio(content=content)
        config = RecognitionConfig(
            encoding=RecognitionConfig.AudioEncoding.LINEAR16,
            sample_rate_hertz=sample_rate,
            language_code="en-US"
        )

        response = speech_client.recognize(config=config, audio=audio)
        transcription = ' '.join([result.alternatives[0].transcript for result in response.results])

        return transcription
    except Exception as e:
        print(e)
        return None

if __name__ == '__main__':
    transcription = transcribe_voice_message()
    print(transcription)
