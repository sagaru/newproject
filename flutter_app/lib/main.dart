import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_app/mapPage.dart';
import 'package:flutter_app/report_form_page.dart';
import 'package:flutter_app/messaging.dart';
import 'package:flutter_app/signUpPage.dart';
import 'package:flutter_app/loginPage.dart';
import 'package:flutter_app/user.dart';
import 'package:flutter_app/background_services.dart' ;
import 'dart:isolate';
import 'package:flutter_app/utils/user_preferences.dart';
import 'package:latlong2/latlong.dart';
import 'package:flutter_app/voiceMessagePage.dart';
import 'package:overlay_support/overlay_support.dart';


// const String serverAddr = 'http://10.0.2.2';
const String serverAddr = 'http://172.206.108.155' ;
// const String serverAddr = 'http://4.153.208.166' ;

// const String expressJsPort = '3000';
//const String ingressPort = '80';
// const String kafkaPort = '8082';

void startBackgroundTask() {
  print("Starting background task");
  ReceivePort mainReceivePort = ReceivePort();

  Isolate.spawn(backgroundTaskEntryPoint, mainReceivePort.sendPort)
      .then((_) => print('Isolate started'));

  mainReceivePort.listen((dynamic receivedData) {
    if (receivedData is SendPort) {
      Timer.periodic(const Duration(seconds: 15), (_) {
        var userData = user.toMap();
        receivedData.send(userData);
      });
    } else if (receivedData is Map) {
      if (receivedData.containsKey('newMessages') && receivedData.containsKey('newWaypoints')) {
        List<String> newMessages = receivedData['newMessages'];
        List<LatLng> newWaypoints = receivedData['newWaypoints'];
        for (var message in newMessages) {
          print(message);
          print("\n") ;

          List<String> messageParts = message.split('::');

          if (messageParts[0] == 'alert') {
            // convert messageParts from str to Map object
            Map<String, dynamic> messageMap = json.decode(messageParts[1]) as Map<String, dynamic>;
            print('messageMap: ' + messageMap.toString());
            // convert user.toMap()['location'] to List<double>
            List<double> userLocation = user.toMap()['location'];
            if (!isCoordinateInsidePolygon(userLocation, messageMap['coords'])) {
              print('isCoordinateInsidePolygon: false') ;
              continue;
            }
            else {
              print('isCoordinateInsidePolygon: true') ;
            }

            showOverlayNotification((context) {
              return Card(
                margin: const EdgeInsets.symmetric(horizontal: 4),
                color: Colors.red,
                child: SafeArea(
                  child: ListTile(
                    title: Text("ALERT: ${messageMap['text']}", style: const TextStyle(color: Colors.white)),
                    trailing: IconButton(
                        icon: const Icon(Icons.close, color: Colors.white),
                        onPressed: () {
                          OverlaySupportEntry.of(context)!.dismiss();
                        }),
                  ),
                ), // Custom background color
              );
            }, duration: const Duration(hours: 1)); // Adjust duration as needed
            // do not add to dmhistory. This is a broadcast message
          }
          else {
            user.addToDmHistory(message);
          }
        }
        print('New waypoints: $newWaypoints') ;
        print('newWaypoints length: ${newWaypoints.length}');
        if (newWaypoints.length == 1) {
          // keep the waypoints the same if only one is received
          print('keeping waypoints the same') ;
        }
        else {
          print('Setting waypoints') ;
          user.setWaypoints = newWaypoints;
          mapPageKey.currentState?.updatePolylines();
        }
      }
      else {
        user.setWaypoints = []; // Clear waypoints if none received
      }
    }
  });
}

// convert String to List<List<List<double>>>, e.g:  List<List<double>> listOfLists = (json.decode(str) as List)
//       .map((list) => (list as List).map((item) => item.toDouble()).toList())
//       .toList();
bool isCoordinateInsidePolygon(List<double> point, dynamic polygon) {
  // cvt point to double
  var polygonPoints = polygon;
  // while polygonPoints length != 5, keep getting the first element
  while (polygonPoints.length != 5) {
    polygonPoints = polygonPoints[0];
  }
  // reverse the order of the points for each array in polygonPoints
  for (var i = 0; i < polygonPoints.length; i++) {
    polygonPoints[i] = polygonPoints[i].reversed.toList();
  }
  int i = 0 ;
  bool isInside = false;
  int nvert = polygonPoints.length;
  int j = nvert - 1;
  for (; i < nvert; j = i++) {
    if (((polygonPoints[i][1] > point[1]) != (polygonPoints[j][1] > point[1])) &&
        (point[0] < (polygonPoints[j][0] - polygonPoints[i][0]) * (point[1] - polygonPoints[i][1]) / (polygonPoints[j][1] - polygonPoints[i][1]) + polygonPoints[i][0])) {
      isInside = !isInside;
    }
  }

  return isInside;
}



void backgroundTaskEntryPoint(SendPort mainSendPort) async {
  ReceivePort receivePort = ReceivePort();
  mainSendPort.send(receivePort.sendPort);

  receivePort.listen((dynamic data) async {
    if (data is Map) {
      await sendLocation(data); // Assuming this just sends location data
      List<String> messages = await pollMessages(data); // Fetch messages
      List<LatLng> waypoints = await getWaypoints(data); // Fetch waypoints
      mainSendPort.send({'newMessages': messages, 'newWaypoints': waypoints});
    }
  });
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  UserPreferences.init();

  print("Starting background task");
  startBackgroundTask();

  runApp(const OverlaySupport.global(child: MyApp()));
}


class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Dublin Incident Report',
      home: BaseWidget(body: MapPage()), // Set MapPage as default view
    );
  }
}

class BaseWidget extends StatelessWidget {
  final Widget body;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  BaseWidget({super.key, required this.body});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: const Text('Dublin Incident Report'),
        leading: IconButton(
          icon: const Icon(Icons.menu),
          onPressed: () => _scaffoldKey.currentState?.openDrawer(),
        ),
      ),
      drawer: const AppDrawer(),
      body: body,
    );
  }
}


class AppDrawer extends StatelessWidget {
  const AppDrawer({super.key});
  @override
  Widget build(BuildContext context) {
    return Drawer(
        child: ListView(
          children: <Widget>[
            const UserAccountsDrawerHeader(
              accountName: Text(""),
              accountEmail: Text(""),     // void as we don't want to display email
            ),
            ListTile(
              leading: const Icon(Icons.record_voice_over), // Note: this icon is more suitable, but we are not using voice over here
              title: const Text('Create Incident Report'),
              onTap: () {
                Navigator.of(context).pop(); // Close the drawer
                Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => BaseWidget(body: const ReportFormPage())));
              },
            ),
            if (!user.isLoggedIn)
            ListTile(
              leading: const Icon(Icons.login),
              title: const Text('Login'),
              onTap: () async {
                user = UserPreferences.getUser();
                if (user.isLoggedIn) {
                  await createKafkaConsumer() ;
                  await subscribeTopics([user.getDmTopic, User.alertTopic]) ;
                  ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(
                        content: Text('Logged in as ${user.getPhone}'),
                      ));
                  Navigator.of(context).pop(); // Close the drawer(context) => MapPage(key: mapPageKey)
                  Navigator.of(context).pushReplacement(MaterialPageRoute(    builder: (context) => BaseWidget(body: MapPage(key: mapPageKey))));
                  return;
                }
                Navigator.of(context).pop(); // Close the drawer
                Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => BaseWidget(body: const LoginPage())));
              },
            ),
            if (!user.isLoggedIn)
            ListTile(
              leading: const Icon(Icons.app_registration),
              title: const Text('Sign Up'),
              onTap: () {
                Navigator.of(context).pop(); // Close the drawer
                Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => BaseWidget(body: SignUpPage())));
              },
            ),
            ListTile(
              leading: const Icon(Icons.map),
              title: const Text('Map'),
              onTap: () {
                Navigator.of(context).pop(); // Close the drawer
                Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => BaseWidget(body: MapPage())));
              },
            ),
            // if (user.isLoggedIn)
            ListTile(
              leading: const Icon(Icons.mic),
              title: const Text('Send Voice Note'),
              onTap: () {
                Navigator.of(context).pop(); // Close the drawer
                Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => BaseWidget(body: VoiceMessagePage())));
              },
            ),
            ListTile(
              leading: const Icon(Icons.messenger),
              title: const Text('Messaging'),
              onTap: () {
                Navigator.of(context).pop(); // Close the drawer
                Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => BaseWidget(body: const KafkaChatPage())));
              },
            ),
            if (user.isLoggedIn)
            ListTile(
              leading: const Icon(Icons.logout),
              title: const Text('Log Out'),
              onTap: ()
              {
                Navigator.of(context).pop(); // Close the drawer
                user.dispose() ;
                // Handle Logout - to do
              },
            ),
          ],
        ),
      );
  }
}




