from apps.config import Config
from flask import render_template, jsonify, request
import requests
from pymongo import MongoClient
from flask_login import current_user, login_required
from apps.models import MongoDBClient
from bson import ObjectId

def init_app(app):
    @app.route('/get-actions', methods=['GET'])
    def get_actions():
        html = render_template('actions/actions_tab.html')
        return jsonify({'html': html})
    
    
    # this method will fetch from the map-api backend the nearest amenity to the given coordinates AND the route to it
    @app.route('/get-route', methods=['POST'])
    def get_route():
        try:
            url = Config.MAP_API + '/get_nearest_amenity'
            destination = request.get_json()['destination']
            lng = destination['lng']
            lat = destination['lat']  
            amenity_name = request.get_json()['service']

            response = requests.post(url, json={'amenity_name': amenity_name, 'lng': lng, 'lat': lat})
            app.logger.info(f"response: {response.json()}")
            if not (response.status_code == 200):
                app.logger.error('Invalid request (1))')
                return jsonify({'error': 'Invalid request (1)'})
                
            url = Config.MAP_API + '/find_path'
            start_lat = response.json()['coordinates'][0]
            start_lng = response.json()['coordinates'][1]
            end_lat = lat
            end_lng = lng
            response = requests.post(url, json={'start_lat': start_lat, 'start_lon': start_lng, 'end_lat': end_lat, 'end_lon': end_lng})
            app.logger.info(f"response: {response.json()}")
            if not (response.status_code == 200):
                app.logger.error('Invalid request (2)')
                return jsonify({'error': 'Invalid request (2)'})
            else:
                return jsonify(response.json())
            
            
        except Exception as e:
            app.logger.error(f'Invalid request (3) : {e}')
            return jsonify({'error': 'Invalid request (3)'})
    

    
    @app.route('/unblock-route', methods=['POST'])
    def unblock_route():
        id = request.get_json()['id']
        client = MongoClient(Config.MONGO_URI)
        db = client.Dashboard
        collection = db.BlockedRoutes
        route = collection.find_one({'_id': ObjectId(id)})['path']
        for i in range(len(route)):
            route[i] = (route[i]['lng'], route[i]['lat'])
        if route:
            response = requests.post(Config.MAP_API + '/remove_constraint', json={'id': id, 'path': route})
        if response.status_code == 200:
            if remove_blocked_route_from_DB(id=id, app=app):
                return jsonify({f'message': 'Route unblocked successfully'}), 200
            else:
                return jsonify({f'message': 'Warning: Error OR Route already unblocked'})
        else:
            return jsonify({f'message': 'Warning: Error OR Route already blocked'})

       
       
    @app.route('/find-road', methods=['POST'])
    def find_road():
        road_name = request.get_json()['road_name'].lower()
        reponse = requests.post(Config.MAP_API + '/find-road', json={'road_name': road_name})
        if reponse.status_code == 200:
            return jsonify(reponse.json())
        else:
            app.logger.error('Road not found')
            return jsonify({'error': 'Road not found'}), 404
    
    @app.route('/block-route', methods=['POST'])
    def block_route():

        app.logger.warning('path block request: {}'.format(request.get_json()))
        start = request.get_json()['start']
        start = [float(start['lat']), float(start['lng'])]
        end = request.get_json()['end']
        end = [float(end['lat']), float(end['lng'])]
        response = requests.post(Config.MAP_API + '/start-end-path', json={'start': start, 'end': end})   # coordinate order swicthing handled at map-api
        path = response.json()['path']
        if path == []:
            return jsonify({'error': 'Road not found'}), 404
        
        response = requests.post(Config.MAP_API + '/road-name-by-path', json={'path': path})
        road_name = response.json()['road_name']
        app.logger.info(f"road name: {road_name}")
        # for Linestring in javascript
        for i in range(len(path)):
            path[i] = {'lat': path[i][0], 'lng': path[i][1]}
        
        app.logger.info(f"len coords: {len(path)}")
        if response.status_code == 200:
            id = add_blocked_route_to_DB(path, road_name , app=app)
            requests.get(Config.MAP_API + '/add_constraint')   # signal map-api to update constraints
            return jsonify({'path': path, 'road_name': road_name, 'id': id}), 200
        else:
            return jsonify({'error': 'Road not found'}), 404
        
        
        
        
        
def add_blocked_route_to_DB(route,road_name, app=None):
    
    ### get road name from map-api and add it to the entry
    
    client = MongoDBClient.get_client()
    db = client.Dashboard
    collection = db.BlockedRoutes
    
    # check if route already exists
    if collection.find_one({'path': route}):
        return 1

    ret = collection.insert_one({'path': route, 'road_name': road_name})
    
    # close connection
    client.close()
    
    if id:
        return str(ret.inserted_id)
    else:
        return 0
    



def remove_blocked_route_from_DB(route=None, id=None, app=None):
    client = MongoClient(Config.MONGO_URI)
    db = client.Dashboard
    collection = db.BlockedRoutes

    if route:
        update_result = collection.delete_one({'path': route})
    
    if id:
        update_result = collection.delete_one({'_id': ObjectId(id)})
    
    ret = 0 
    if update_result.deleted_count == 1:
        ret = 1
    else:
        app.logger.error(f'Error: {update_result.deleted_count} routes deleted')
    
    return ret



# @app.route('/block-route', methods=['POST'])
# def block_route():
#     route = request.get_json()['route']
#     if add_blocked_route_to_DB(route, app=app):
#         ### to map-api
#         response = requests.post(Config.MAP_API + '/add_constraint', json={'path': route})
#         if response.status_code == 200:
#             client = MongoClient(Config.MONGO_URI)
#             db = client.Dashboard
#             collection = db.BlockedRoutes
#             collection.insert_one({'route_id': id}) ################
            
#             return jsonify({f'message': 'Route {route} blocked successfully'})
#         else:
#             return jsonify({f'message': 'Warning: Error OR Route {route} already blocked'})
#     else:
#         return jsonify({f'message': 'Warning: Error OR Route {route} already blocked'})