import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'main.dart' ;
import 'package:flutter_app/user.dart' ;
import 'package:latlong2/latlong.dart' ;



Future<void> sendLocation(Map data) async {
  if (!data['isLoggedIn']) {
    print('sendLocation: User not logged in');
    print('sendLocation: $data');
    return;
  }

  try {
    var coordinates = data['location'];
    double lat = coordinates[0];
    double lon = coordinates[1];

    var response = await http.post(
      // Uri.parse('$serverAddr:$expressJsPort/update-location'),
      Uri.parse('$serverAddr/update-location'),
      headers: {"Content-Type": "application/json"},
      body: json.encode({
        'location': {
          'coordinates': [lat, lon],
          'timestamp': DateTime.now().toIso8601String()
        },
        'phone': data['phone']
      }),
    );
    print('Location sent: ${response.statusCode}');
  } catch (e) {
    print('Error sending location: $e');
  }
}



Future<List<LatLng>> getWaypoints(Map data) async {
  if (!data['isLoggedIn']) {
    print('getWaypoints: User not logged in');
    return [];
  }
  List<double> wp ;
  if (data['waypoint'] == null) {
    print('getWaypoints: No waypoint');
    wp = [] ;
  }
  else {
    wp = data['waypoint'] ;
  }

  print('sending waypoint: $wp') ;

  try {
    var response = await http.post(
      // Uri.parse('$serverAddr:$expressJsPort/mobile-directions'),
      Uri.parse('$serverAddr/mobile-directions'),
      headers: {"Content-Type": "application/json"},
      body: json.encode({'phone': data['phone'], 'waypoint': wp}),
    );

    print('getWaypoints: Response - ${response.statusCode} - ${response.body}');

    if (response.statusCode == 200) {
      final jsonData = jsonDecode(response.body);
      if (jsonData.containsKey('path') && jsonData['path'] is List) {
        List<dynamic> paths = jsonData['path'];
        return paths.map<LatLng>((e) {
          if (e is List && e.length == 2) {
            return LatLng(e[0], e[1]);
          }
          return const LatLng(0.0, 0.0);
        }).toList();
      }
    }
    else if (response.statusCode == 201) {
      print("waypoint is still valid") ;
      return const [LatLng(0.0, 0.0)] ;
    }
    else if (response.statusCode == 404) {
      return const [] ;
    }
    else {
      print('Error getting waypoints: ${response.statusCode} - ${response.body}');
    }
  } catch (e) {
    print('Error getting waypoints: $e');
  }
  return const [LatLng(0.0, 0.0)] ; // on error we keep the last waypoint
}



Future<List<String>> pollMessages(Map data) async {
  print('pollMessages: Starting...');

  if (!data['isLoggedIn']) {
    print('pollMessages: User not logged in');
    print("pollMessages: Data: $data");
    return [];
  }

  print('pollMessages: User is logged in');
  print('dmHistory: ${data['dmHistory']}');
  print('dmTopic: ${data['dmTopic']}');

  // String uriString = '$serverAddr:$kafkaPort/consumers/${data['consumerGroup']}/instances/${data['consumerName']}/records';
  String uriString = '$serverAddr/consumers/${data['consumerGroup']}/instances/${data['consumerName']}/records';
  print('pollMessages: URI - $uriString');

  final uri = Uri.parse(uriString);
  final headers = {
    'Accept': 'application/vnd.kafka.json.v2+json'
  };
  print('pollMessages: Making HTTP GET request');
  final response ;
  try {
    response = await http.get(uri, headers: headers);
  }
  catch (e) {
    print('pollMessages: Error making HTTP GET request: $e');
    return [];
  }

  print('pollMessages: Response status code - ${response.statusCode}');
  print("response: ${response.body}") ;
  if (response.statusCode >= 200 && response.statusCode < 300) {
    final jsonData = jsonDecode(response.body);

    if (jsonData is List) {
      return jsonData.map<String>((e) {
        if (e is Map) {
          if (e.containsKey('value')) {
            if (e['value'] is String) {
              return e['value'];
            } else if (e['value'] is Map && e['value'].containsKey('message')) {
              return e['value']['message'].toString();
            }
          }
        }
        return 'Invalid format';  // Return a default string for invalid formats
      }).toList();
    } else {
      return [];  // Return an empty list if jsonData is not a List
    }
  } else {
    print('Error polling messages: ${response.statusCode} - ${response.body}');
    return [];
  }
}



// new





